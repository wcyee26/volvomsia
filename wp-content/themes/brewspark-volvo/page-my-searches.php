<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Volvo Malaysia
 * @since 1.0
 * @version 1.0
 */

get_header(); 

?>

<div class="site-content-container">
	<div class="container">
		<section class="section-placeholder section-placeholder-primary">
			<div class="section-heading">
				<h2>My Searches</h2>
			</div>			
			<!-- MY SEARCHES -->
			<div class="section-content">
				<div class="row">
					<div class="col-xs-offset-1 col-xs-10 col-sm-offset-1 col-sm-10">
						<div class="result-placeholder">
							<div class="result-content">
								<div class="result-list result-list-full">
									<div id="searchListPlaceholder" class="result-list-content no-padding no-border">
										<!-- SEARCH LIST -->
										<div id="searchList" class="result-list-content-row my-searches" 
											 style="display: none; padding: 10px;">
											<div class="row">
												<div class="col-xs-10 col-sm-11 result-list-content-row-content">
													<a id="btnSearchListDetails" href="javascript:void(0)">
														<div class="row">
															<div class="col-xs-12 col-sm-9">
																<span id="searchListDetails">(None)</span>
															</div>
															<div class="col-xs-12 col-sm-3">
																<span id="searchListResult">
																	<span id="searchListResultCount">0</span>
																	matched results
																</span>																
															</div>
														</div>
													</a>
												</div>
												<div class="col-xs-2 col-sm-1 text-right">
													<a id="btnSearchListRemove" href="javascript:void(0)" data-index="-1" style="color: #800">
														<strong>X</strong>
													</a>
												</div>
											</div>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

<script type="text/javascript">
	jQuery("document").ready(function(){
		pageMySearches.init();
	});
</script>

<?php get_footer();