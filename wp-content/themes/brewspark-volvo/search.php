<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Volvo Malaysia
 * @since 1.0
 * @version 1.0
 */
?>

<?php get_header(); ?>

<?php

	$_page_url_vehicle_details = get_page_link( get_page_by_path('vehicle-details')->ID );

?>

<div class="site-content-container">
	<div class="container">

		<!-- Search Result -->
		<section class="section-placeholder section-placeholder-primary">
			<div class="section-heading">
				<h2>Search Result</h2>
			</div>			
			<div class="section-content">
				<div class="row">
					<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1">
						<div class="row">
							<div class="col-md-12">
								<!-- FORM GLOBAL SEARCH FILTER -->
								<form id="formGlobalSearchFilter" method="GET" action="<?php echo home_url( '/' ); ?>"
									  data-page-url-vehicle-details="<?php echo $_page_url_vehicle_details; ?>">	
									<!-- FIELD GLOBAL SEARCH STRING -->
									<input type="hidden" id="fieldSearchString" name="s" />	
									<!-- SECTION FORM -->
									<section class="section-placeholder">
										<div class="section-content" style="padding-top:15px; padding-bottom: 15px;">
											<div class="row">
												<div class="hidden-xs col-sm-6">
													<section class="section-article">
														<div class="section-content">
															<?php get_template_part( 'template-parts/search-filter-vehicle-models' ); ?>
														</div>
													</section>
												</div>
												<div class="col-xs-12 col-sm-3">
													<section class="section-article custom-filter">
														<div class="section-content">
															<div class="row">
																<div class="col-md-12">
																	<?php get_template_part( 'template-parts/search-filter-vehicle-properties' ); ?>				
																</div>
															</div>
														</div>
													</section>
												</div>
												<div class="col-xs-12 col-sm-3">
													<section class="section-article custom-filter">
														<div class="section-content">
															<div class="row">
																<div class="col-md-12">
																	<label>Features</label>
																	<?php get_template_part( 'template-parts/search-filter-vehicle-features' ); ?>
																</div>
															</div>
														</div>
													</section>
												</div>
											</div>
										</div>
										<hr class="hr-primary hr-sm" />
										<!-- ACTIONS -->
										<div class="section-footer text-center">
											<div class="row no-gutters">
												<div class="col-xs-12 col-sm-2">
													<button id="btnQuickSearch" type="button" class="btn btn-sharp btn-sharp-primary btn-width-max" style="display: none; text-align: center;">
														Quick search
													</button>
													<button id="btnAdvancedSearch" type="button" class="btn btn-sharp btn-sharp-primary btn-width-max" style="text-align: center;">
														Advanced search
													</button>
												</div>
												<div class="col-xs-12 col-sm-3 pull-right">
													<button id="btnSearch" type="submit" style="text-align: center;"
															class="btn btn-sharp btn-sharp-secondary btn-width-max ladda-button" data-style="zoom-in">
														<span id="btnSearchText">
															View <span id="resultCount">0</span> results	
														</span>
													</button>
												</div>
												<div class="col-xs-12 col-sm-2 pull-right" style="padding-left: 5px; padding-right: 5px;">
													<button id="btnResetSearch" type="button" class="btn btn-sharp btn-sharp-primary btn-width-max" style="text-align: center;">
														Reset search
													</button>
												</div>
												<div class="col-xs-12 col-sm-2 pull-right" style="padding-left: 5px; padding-right: 5px;">
													<button id="btnSaveSearch" type="button" class="btn btn-sharp btn-sharp-primary btn-width-max" style="text-align: center;">
														Save search
													</button>
												</div>
												<!-- <div class="col-xs-12 col-sm-2 pull-right" style="padding-left: 5px; padding-right: 5px;">
													<button id="btnAddSearchAlert" type="submit" class="btn btn-sharp btn-sharp-primary btn-width-max" 
															style="text-align: center; padding-left: 10px; padding-right: 10px;">
														Add search alert
													</button>
												</div> -->
											</div>																																
										</div>
									</section>
								
									<!-- SEARCH RESULTS -->									
									<section id="searchResults" class="section-placeholder">
										<div class="section-heading">
											<h2><span id="searchResultCount">0</span> search results</h2>
										</div>
										<div class="section-content" style="padding-top: 10px; padding-bottom: 10px;">
											<div class="result-placeholder">
												<div class="result-toolbar">	
													<div class="row">
														<div class="col-xs-12 col-md-9">
															<!-- <div class="result-toolbar-main">
																<h4>Filter by trim: </h4>
																<ul>
																	<li>-</li>
																	<li class="active"><a href="#">View all</a></li>
																</ul>
															</div> -->
														</div>
														<div class="col-xs-12 col-md-3">
															<div class="result-toolbar-actions">											
																<select id="fieldOrderby" name="orderby">
																	<option value="price" data-order="ASC"
																			<?php echo isset($_GET['orderby'], $_GET['order']) 
																				&& $_GET['orderby'] == 'price' && $_GET['order'] == 'ASC' 
																					? 'selected' : ''; ?>>
																		Sort by: Price ASC
																	</option>
																	<option value="price" data-order="DESC"
																			<?php echo isset($_GET['orderby'], $_GET['order']) 
																				&& $_GET['orderby'] == 'price' && $_GET['order'] == 'DESC' 
																					? 'selected' : ''; ?>>
																		Sort by: Price DESC
																	</option>
																	<option value="mileage" data-order="ASC"
																			<?php echo isset($_GET['orderby'], $_GET['order']) 
																				&& $_GET['orderby'] == 'mileage' && $_GET['order'] == 'ASC' 
																					? 'selected' : ''; ?>>
																		Sort by: Mileage ASC
																	</option>
																	<option value="mileage" data-order="DESC"
																			<?php echo isset($_GET['orderby'], $_GET['order']) 
																				&& $_GET['orderby'] == 'mileage' && $_GET['order'] == 'DESC' 
																					? 'selected' : ''; ?>>
																		Sort by: Mileage DESC
																	</option>
																	<option value="regDate" data-order="ASC"
																			<?php echo isset($_GET['orderby'], $_GET['order']) 
																				&& $_GET['orderby'] == 'regDate' && $_GET['order'] == 'ASC' 
																					? 'selected' : ''; ?>>
																		Sort by: Reg date ASC
																	</option>
																	<option value="regDate" data-order="DESC"
																			<?php echo isset($_GET['orderby'], $_GET['order']) 
																				&& $_GET['orderby'] == 'regDate' && $_GET['order'] == 'DESC' 
																					? 'selected' : ''; ?>>
																		Sort by: Reg date DESC
																	</option>
																	<option value="enginePower" data-order="ASC"
																			<?php echo isset($_GET['orderby'], $_GET['order']) 
																				&& $_GET['orderby'] == 'enginePower' && $_GET['order'] == 'ASC' 
																					? 'selected' : ''; ?>>
																		Sort by: Engine power ASC
																	</option>
																	<option value="enginePower" data-order="DESC"
																			<?php echo isset($_GET['orderby'], $_GET['order']) 
																				&& $_GET['orderby'] == 'enginePower' && $_GET['order'] == 'DESC' 
																					? 'selected' : ''; ?>>
																		Sort by: Engine power DESC
																	</option>
																</select>	
																<input type="hidden" id="fieldOrder" name="order" 
																	   value="<?php echo isset($_GET['order']) ? $_GET['order'] : 'ASC'; ?>" />
															</div>	
														</div>
													</div>
												</div>
												<div class="result-content">
													<!-- Web -->
													<div class="row">
														<div id="vehiclesPlaceholder" class="hidden-xs col-md-12">														
															<div id="vehicle0" class="result-list result-list-full" style="display: none;">
																<div class="result-list-heading">
																	<!-- <a id="vehicle0TitlePanel" href="#vehicle0SpecificDetail" class="result-list-tab" data-toggle="collapse" aria-expanded="false">															 -->
																	<a id="vehicle0TitlePanel" href="javascript:void(0)" class="result-list-tab">															
																		<div class="row">
																			<div class="col-sm-12 col-md-6">
																				<span id="vehicle0Title">Vehicle 0</span>
																			</div>
																			<div class="col-sm-8 col-md-3 text-right">
																				<div id="vehicle0FeatureVolvoSelekt" style="display: none;">
																					<img width="170px" 
																						 class="volvo-selekt" 
																						 src="<?php echo get_template_directory_uri() ?>/assets/images/icon-volvo-selekt.png">
																				</div>
																			</div>
																			<div class="col-sm-4 col-md-3 text-right">
																				<span id="vehicle0Price">RM 0</span>
																			</div>
																		</div>												
																	</a>
																</div>															
																<div class="result-list-content">
																	<div class="row">
																		<div class="col-sm-4" style="width: 30%;">	
																			<a id="vehicle0FeaturedImagePlaceholder" href="">
																				<img id="vehicle0FeaturedImage" 
																					 src="<?php echo get_template_directory_uri() ?>/assets/images/nopic.png">
																			</a>															
																		</div>
																		<div class="col-sm-5" style="width: 50%;">
																			<ul class="ul-custom ul-custom-bullet ul-col-ct-3">
																				<li id="vehicle0RegDate">0</li>
																				<li id="vehicle0Mileage">0 km</li>
																				<li id="vehicle0EngineSize">0 L</li>
																				<li id="vehicle0EnginePower">0 bhp</li>
																				<li id="vehicle0FuelType">No fuel</li>
																				<li id="vehicle0TransmissionType">No transmission</li>
																				<li id="vehicle0Co2Emission">No co2 emissions</li>
																				<li id="vehicle0Feature" style="display: none;">No feature</li>
																			</ul>
																			</br>
																			<div>
																				<strong>
																					<span id="vehicle0DealerLocationAddress">Somewhere in Malaysia.</span>
																				</strong>&nbsp;
																				<span id="vehicle0DealerLocationDistancePlaceholder" style="display: none;">
																					(<span id="vehicle0DealerLocationDistance">0 km</span>)
																				</span>
																			</div>
																			<div id="vehicle0DealerLocationPhonePlaceholder">
																				Please contact us at: <strong><span id="vehicle0DealerLocationPhone">03-1234567</span></strong>
																			</div>
																		</div>
																		<div class="col-sm-3" style="width: 20%;">														
																			<a id="btnVehicle0FullDetails" type="button" class="btn btn-sharp btn-sharp-primary btn-width-max"
																					href="">
																				Full Details
																			</a>
																			<button id="btnVehicle0Compare" type="button" class="btn btn-sharp btn-sharp-primary btn-width-max my-comparison">
																				Compare
																			</button>
																			<!-- <a id="btnVehicle0ReadMore" type="button" class="btn btn-sharp btn-sharp-secondary btn-width-max"
																			   href="#vehicle0SpecificDetail" data-toggle="collapse" aria-expanded="false">
																				Read More
																			</a> -->
																			<a id="btnVehicle0ReadMore" type="button" 
																			   class="btn btn-sharp btn-sharp-secondary btn-width-max ladda-button" data-style="zoom-in">
																				Read More
																			</a>
																		</div>
																	</div>
																</div>
																<!-- VEHICLE SPECIFIC DETAIL PLACEHOLDER -->
															</div>
														</div>
													</div>
													<!-- Mobile -->
													<div class="row">
														<div id="mobileVehiclesPlaceholder" class="visible-xs col-xs-12">														
															<div id="mobileVehicle0" class="result-list result-list-mobile" style="display: none;">
																<div id="mobileVehicle0FeaturedImagePlaceholder" class="result-list-thumbnail">
																	<img id="mobileVehicle0FeaturedImage" 
																		 src="<?php echo get_template_directory_uri() ?>/assets/images/nopic.png">
																</div>
																<div class="result-list-heading">
																	<span id="mobileVehicle0Title">Vehicle 0</span>
																</div>
																<div class="result-list-content">
																	<ul class="ul-custom ul-custom-bullet ul-col-ct-2">
																		<li id="mobileVehicle0RegDate">0</li>
																		<li id="mobileVehicle0Mileage">0 km</li>
																		<li id="mobileVehicle0EngineSize">0 L</li>
																		<li id="mobileVehicle0EnginePower">0 bhp</li>
																		<li id="mobileVehicle0FuelType">No fuel</li>
																		<li id="mobileVehicle0TransmissionType">No transmission</li>
																		<li id="mobileVehicle0Co2Emission">No co2 emissions</li>
																		<div id="mobileVehicle0FeatureVolvoSelekt" style="display: none;">
																			<img width="170px" 
																				 class="volvo-selekt"
																				 src="<?php echo get_template_directory_uri() ?>/assets/images/icon-volvo-selekt.png">
																		</div>
																		<li id="mobileVehicle0Feature" style="display: none;">No feature</li>
																	</ul>
																	<div>
																		<strong>
																			<span id="mobileVehicle0DealerLocationAddress">Somewhere in Malaysia.</span>
																		</strong>&nbsp;
																		<span id="mobileVehicle0DealerLocationDistancePlaceholder" style="display: none;">
																			(<span id="mobileVehicle0DealerLocationDistance">0 km</span>)
																		</span>
																	</div>
																	<div id="mobileVehicle0DealerLocationPhonePlaceholder">
																		Please contact us at: <strong><span id="mobileVehicle0DealerLocationPhone">03-1234567</span></strong>
																	</div>
																</div>
																<div class="result-list-total">
																	<span id="mobileVehicle0Price">RM 0</span>
																</div>
																<div class="result-list-footer">
																	<button id="mobileBtnVehicle0Compare" type="button" class="btn btn-sharp btn-sharp-primary my-comparison">
																		Compare
																	</button>
																	<a id="mobileBtnVehicle0FullDetails" type="button" class="btn btn-sharp btn-sharp-secondary"
																		href="">
																		Full Details
																	</a>
																</div>
															</div>
														</div>
													</div>
													<div id="spinnerResultContent" class="text-center" style="display: none">
														<br/>
														<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
														<span class="sr-only">Loading...</span>
													</div>
												</div>
											</div>
										</div>
										<div class="section-footer">
											<button id="btnShowMore" type="button" style="display: none;"
													class="btn btn-sharp btn-sharp-primary ladda-button" 
													data-style="zoom-in" data-spinner-color="#003057">
												Show <span id="btnShowMorePageSize">0</span> More
											</button>
											&nbsp;
											<button id="btnShowAll" type="button" style="display: none;"
													class="btn btn-sharp btn-sharp-primary ladda-button" 
													data-style="zoom-in" data-spinner-color="#003057">
												Show All
											</button>
										</div>
									</section>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

<script type="text/javascript">													
	jQuery(document).ready(function(){
		pageSearch.init();
	});
</script>

<?php get_footer();
