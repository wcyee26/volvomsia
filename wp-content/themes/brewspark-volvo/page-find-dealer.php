<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Volvo Malaysia
 * @since 1.0
 * @version 1.0
 */

get_header(); 

// Default WP Search String Field
$fieldSearchString = isset($_GET['q']) ? $_GET['q'] : null;
$fieldPostcode = isset($_GET['fieldPostcode']) ? $_GET['fieldPostcode'] : null;
$fieldState = isset($_GET['fieldState']) ? $_GET['fieldState'] : null;

$order = 'ASC';
$orderby = 'title';

$args = array();

// orders
$args['order'] = $order; 
$args['orderby'] = $orderby;  

// search post title or post meta (not using 's' anymore)
$args['_meta_or_title'] = '';
// post meta
$args['meta_query'] = array();

if(!empty($fieldSearchString)){

	$args['_meta_or_title'] = $fieldSearchString;

	$args['meta_query']['relation'] = 'OR';
	$args['meta_query'][] = array(
                    'key' => 'wpsl_address'
                	, 'value' => $fieldSearchString
                    , 'compare' => 'LIKE'
                );
	$args['meta_query'][] = array(
                    'key' => 'wpsl_address2'
                	, 'value' => $fieldSearchString
                    , 'compare' => 'LIKE'
                );
}
if(!empty($fieldPostcode)){
	$args['meta_query']['relation'] = 'AND';
	$args['meta_query'][] = array(
                    'key' => 'wpsl_zip'
                	, 'value' => $fieldPostcode
                    , 'compare' => 'LIKE'
                );
}
if(!empty($fieldState) && $fieldState != -1){
	$args['meta_query'][] = array(
                    'key' => 'wpsl_state'
                	, 'value' => $fieldState
                    , 'compare' => 'LIKE'
                );
}

$dealer_locations = BWooVehicle::get_dealer_locations( $args );

$states = array(
	'johor' => 'Johor'
	, 'kedah' => 'Kedah'
	, 'kelantan' => 'Kelantan'
	, 'melaka' => 'Melaka'
	, 'negeri_sembilan' => 'Negeri Sembilan'
	, 'pahang' => 'Pahang'
	, 'perak' => 'Perak'
	, 'perlis' => 'Perlis'
	, 'penang' => 'Penang'
	, 'sabah' => 'Sabah'
	, 'sarawak' => 'Sarawak'
	, 'selangor' => 'Selangor'
	, 'terengganu' => 'Terengganu'
	// , 'kuala_lumpur' => 'Kuala Lumpur'
	// , 'labuan' => 'Labuan'
	// , 'putrajaya' => 'Putrajaya'
	);

$current_page_url = get_permalink();

?>

<div class="site-content-container site-content-container-no-gutters" style="background: #eee;">
	<div class="container-fluid no-padding">
		<div class="row">
			<div class="col-xs-12">
				<div class="row no-gutters">
					<div class="col-xs-12 col-sm-9 pull-right">
						<!-- MAP -->
						<div class="embed-responsive embed-responsive-4by3" style="padding-bottom: 90%">
							<div id="map" class="embed-responsive-item"></div>
						</div>	
					</div>
					<div class="col-xs-12 col-sm-3">
						<!-- FIND DEALER FORM -->
						<div class="padder-box">
							<form id="formSearchFilter" method="GET" action="<?= $current_page_url; ?>">
								<h2>Find a Dealer</h2>
								<br/>
								<div class="form-group">
									<input id="fieldSearchString" name="q" type="text" class="form-control form-control-sharp" 
											value="<?= isset($_GET['q']) ? $_GET['q'] : '' ?>"
											placeholder="Dealer name or Location" />
								</div>								
								<div class="form-group">OR</div>
								<div class="row">
									<div class="col-xs-5" style="padding-right: 5px;">
										<div class="form-group">
											<input id="fieldPostcode" name="fieldPostcode" 
													type="text" class="form-control form-control-sharp" 
													value="<?= isset($_GET['fieldPostcode']) ? $_GET['fieldPostcode'] : ''; ?>"
													placeholder="Postcode" />
										</div>
									</div>
									<div class="col-xs-7" style="padding-left: 5px;">
										<div class="form-group">
											<select id="fieldState" name="fieldState" class="form-control form-control-sharp">
												<option value="-1">View all states</option>
												<?php foreach($states as $key => $state) : ?>
													<option value="<?= $state; ?>" 
															<?= $_GET['fieldState'] == $state ? 'selected' : ''; ?>>
														<?= $state; ?>
													</option>
												<?php endforeach; ?>
											</select>
										</div>
									</div>
								</div>
								<button type="submit" class="btn btn-sharp btn-sharp-secondary btn-width-max" style="text-align:center;">
									Show Dealers
								</button>								
							</form>							
						</div>
						<!-- LOCATION CARD LIST -->
						<div class="card-list-placeholder" style="overflow: auto; height: 400px;">
						<?php if(count($dealer_locations) > 0) : ?>
							<?php foreach( $dealer_locations as $index => $dealer_location ) : ?>
								<a id="dealerLocation<?= $dealer_location->ID; ?>" href="javascript:void(0);" class="card-list">
									<div class="row">
										<div class="col-xs-2">
											<span data-count="<?= intval($index)+1; ?>" class="icon-badge icon-badge-pin"></span>
										</div>
										<div class="col-xs-10 text-left">
											<p class="no-margin">
												<!-- DEALER NAME -->
												<strong><?= $dealer_location->title; ?></strong>
												<br/>
												<!-- DEALER ADDRESS -->
												<?= $dealer_location->full_address; ?>
											</p>
										</div>
									</div>								
								</a>
								<div id="dealerLocation<?= $dealer_location->ID; ?>InfoWindowContent" style="display: none;">
									<div class="text-left">
										<!-- TITLE -->
										<?php if($dealer_location->url) : ?>
											<a href="<?= $dealer_location->url; ?>" target="_blank">
												<h5><?= $dealer_location->title; ?></h5>
											</a>
										<?php else : ?>
											<h5><?= $dealer_location->title; ?></h5>
										<?php endif; ?>
										<!-- FULL ADDRESS -->
										<?= $dealer_location->full_address; ?>
										<br/>
										<!-- TELEPHONE -->
										<?php if(!empty($dealer_location->phone)) : ?>
											Tel: <strong><?= $dealer_location->phone ?></strong>
											<br/>
										<?php endif; ?>
										<br/>
										<a style="font-size: 14px;" target="_blank" 
											href="<?= $dealer_location->maps['google']->daddr; ?>">Get directions</a>
									</div>
								</div>
							<?php endforeach; ?>
						<?php else : ?>
							<br/>
							No result(s) found.
						<?php endif; ?>
						</div>						
					</div>
				</div>	
			</div>
		</div>		
	</div>
</div>

<script type="text/javascript">
	
	jQuery("document").ready(function(){

		var dealer_locations = <?= json_encode( $dealer_locations ); ?>;

		pageFindDealer.init( { locations: dealer_locations } );

	});

</script>

<?php get_footer();
