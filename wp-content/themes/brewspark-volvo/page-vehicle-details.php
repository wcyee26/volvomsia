<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Volvo Malaysia
 * @since 1.0
 * @version 1.0
 */

get_header();

$post_id = isset( $_GET['post_id'] ) ? $_GET['post_id'] : 0;
$vehicle = BWooVehicle::get_vehicle( $post_id, array('has_specific_detail' => true, 'has_adjacent_post' => true) );
$dealer_location = $vehicle->dealer_location;
$specific_detail = $vehicle->specific_detail;

$previous_post = $vehicle->previous_post;
$next_post = $vehicle->next_post;

$_page_url_vehicle_details = get_page_link( get_page_by_path('vehicle-details')->ID );

// get contact dealer form, to retrieve post_id and post_title for contact-form-7 shortcodes
$contact_forms = get_posts( 
	array(
		'post_type' => 'wpcf7_contact_form'
		, 'post_status' => 'publish'
		) 
	);
$contact_dealer_form = null;
foreach ($contact_forms as $index => $contact_form) {
	if($contact_form->post_title == 'Contact Dealer'){
		$contact_dealer_form = $contact_form;
		break;
	}
}

?>

<div class="site-content-container site-content-container-bg-top-detail">
	<div class="container">
		
		<!-- VEHICLE TITLE, ACTIONS -->
		<div class="heading-broad heading-broad-1">
			<?= $vehicle->title; ?>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-sm-offset-4">
				<div style="margin-bottom: 20px;">
					<div class="row">
						<div class="col-xs-6" style="padding-right: 5px;">
							<a type="button" class="btn btn-sharp btn-sharp-light btn-width-max" style="text-align: center;"
								<?php if( !empty($previous_post) ) : ?>
									href="<?= $_page_url_vehicle_details . '?post_id=' . $previous_post->ID; ?>"
								<?php else : ?>
									disabled
								<?php endif; ?>
								>
								PREV								
							</a>
						</div>								
						<div class="col-xs-6" style="padding-left: 5px;">
							<a type="button" class="btn btn-sharp btn-sharp-light btn-width-max" style="text-align: center;"
								<?php if( !empty($next_post) ) : ?>
									href="<?= $_page_url_vehicle_details . '?post_id=' . $next_post->ID; ?>"
								<?php else : ?>
									disabled
								<?php endif; ?>
								>
								NEXT								
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>		

		<!-- VEHICLE DETAILS -->
		<section class="section-placeholder section-placeholder-primary">
			<div class="section-heading">
				<h2>Vehicle Details</h2>
			</div>			
			<div class="section-content">
				<div class="row">
					<div class="col-xs-offset-1 col-xs-10 col-sm-offset-1 col-sm-10">
						<!-- VEHICLE DETAILS -->
						<div class="text-left" style="margin-bottom: 40px;">							
							<div class="row">
								<!-- IMAGE PLACEHOLDER -->
								<div class="col-xs-12 col-sm-6 pull-right">
									<div style="margin-bottom: 20px;">
										<img src="<?= $vehicle->featured_image->srcs['large']; ?>" 
											 title="<?= $vehicle->featured_image->title; ?>">
									</div>								
								</div>
								<!-- VEHICLE DESCRIPTION -->
								<div class="col-xs-12 col-sm-6">	
									<!-- COLUMN LIST -->							
									<div class="column-list">
										<!-- VOLVO SELEKT -->
										<?php $features = $vehicle->features; ?>
										<?php foreach($features as $key => $feature) : ?>
											<?php if($feature->slug == 'volvo-selekt') : ?>												
												<div class="row">
													<div class="col-xs-5">
														<div class="column-list-heading <?= $feature->slug; ?>">
															<img width="100%" 
																 src="<?= get_template_directory_uri() ?>/assets/images/icon-volvo-selekt.png">
														</div>
													</div>
												</div>									
											<?php endif; ?>
										<?php endforeach; ?>
										<!-- PRICE -->
										<div class="row">
											<div class="col-xs-6">
												<div class="column-list-heading">Price</div>
											</div>
											<div class="col-xs-6">
												<div class="column-list-content" style="font-size: 24px;">
													<?= $vehicle->price_unit . " " . $vehicle->price; ?>
												</div>
											</div>
										</div>
										<!-- MILEAGE -->
										<div class="row">
											<div class="col-xs-6">
												<div class="column-list-heading">Mileage</div>
											</div>
											<div class="col-xs-6">
												<div class="column-list-content">
													<?= $vehicle->mileage . " " . $vehicle->mileage_unit; ?>
												</div>
											</div>
										</div>
										<!-- MODEL YEAR -->
										<div class="row">
											<div class="col-xs-6">
												<div class="column-list-heading">Model year</div>
											</div>
											<div class="col-xs-6">
												<div class="column-list-content">
													<?= $vehicle->vehicle_model->created_year; ?>
												</div>
											</div>
										</div>
										<!-- ENGINE SIZE -->
										<div class="row">
											<div class="col-xs-6">
												<div class="column-list-heading">Engine size</div>
											</div>
											<div class="col-xs-6">
												<div class="column-list-content">
													<?= $vehicle->engine_size . ' ' . $vehicle->engine_size_unit; ?>
												</div>
											</div>
										</div>
										<!-- ENGINE POWER -->
										<div class="row">
											<div class="col-xs-6">
												<div class="column-list-heading">Engine power</div>
											</div>
											<div class="col-xs-6">
												<div class="column-list-content">
													<?= $vehicle->engine_power . ' ' . $vehicle->engine_power_unit; ?>
												</div>
											</div>
										</div>
										<!-- FUEL TYPE -->
										<div class="row">
											<div class="col-xs-6">
												<div class="column-list-heading">Fuel type</div>
											</div>
											<div class="col-xs-6">
												<div class="column-list-content">
													<?= $vehicle->fuel_type->name; ?>
												</div>
											</div>
										</div>
										<!-- TRANSMISSION TYPE -->
										<div class="row">
											<div class="col-xs-6">
												<div class="column-list-heading">Transmission type</div>
											</div>
											<div class="col-xs-6">
												<div class="column-list-content">
													<?= $vehicle->transmission_type->name; ?>
												</div>
											</div>
										</div>
										<!-- EXTERIOR COLOUR -->
										<div class="row">
											<div class="col-xs-6">
												<div class="column-list-heading">Exterior colour</div>
											</div>
											<div class="col-xs-6">
												<div class="column-list-content">
													<?= $vehicle->ext_colour->name; ?>
												</div>
											</div>
										</div>
										<!-- INTERIOR COLOUR -->
										<div class="row">
											<div class="col-xs-6">
												<div class="column-list-heading">Interior colour</div>
											</div>
											<div class="col-xs-6">
												<div class="column-list-content">
													<?= $vehicle->int_colour->name; ?>
												</div>
											</div>
										</div>
										<!-- REGISTRATION NUMBER -->
										<div class="row">
											<div class="col-xs-6">
												<div class="column-list-heading">Registration</div>
											</div>
											<div class="col-xs-6">
												<div class="column-list-content">
													<?= $vehicle->reg_no; ?>
												</div>
											</div>
										</div>
										<!-- REGISTRATION DATE -->
										<div class="row">
											<div class="col-xs-6">
												<div class="column-list-heading">Registration date</div>
											</div>
											<div class="col-xs-6">
												<div class="column-list-content">
													<?php $reg_date = date_create( $vehicle->reg_date ); ?>
													<?php $reg_date = date_format( $reg_date, "Y M" ); ?>
													<?= $reg_date; ?>
												</div>
											</div>
										</div>
										<!-- CO2 EMISSION -->
										<div class="row">
											<div class="col-xs-6">
												<div class="column-list-heading">CO2 emission</div>
											</div>
											<div class="col-xs-6">
												<div class="column-list-content">
													<?= $vehicle->co2_emission . ' ' . $vehicle->co2_emission_unit; ?>
												</div>
											</div>
										</div>
									</div>
									<!-- HPI ENVIRONMENT SHEET -->
									<!-- <div class="row">
										<div class="col-xs-8 col-sm-6">
											<img src="<?= get_template_directory_uri() ?>/assets/images/co2-colour-chart.png"
												 style="margin-bottom: 5px;">										
										</div>
										<div class="col-xs-12">
											<a href="<?= get_template_directory_uri() . '/assets/attachments/hpi_environmental_sheet.pdf'; ?>" target="_blank">
												View HPI Environmental Sheet
											</a>	
											</br>									
										</div>										
									</div> -->
									<!-- VEHICLE WARNING -->
									<div class="row">
										<div class="col-xs-12">											
											<p>
												Whilst every effort has been made to ensure the accuracy of this information and images, 
												some inaccuracies may occur. It is important that you do not rely solely on this information 
												or images but check with your supplying dealer any items that may affect your decision to 
												buy this vehicle. Be sure you can afford the repayments before entering into a credit 
												agreement.
											</p>
											</br>
										</div>
									</div>
								</div>		
								<!-- ACTIONS, LOCATION, CONTACT INFO -->
								<div class="col-xs-12 col-sm-6 pull-right">
									<button id="btnVehicle<?= $vehicle->ID; ?>Compare" type="button" 
											class="btn btn-sharp btn-sharp-primary my-comparison">
										Compare
									</button>
									&nbsp;						
									<a id="btnVehicle<?= $vehicle->ID; ?>ContactDealer" type="button" class="btn btn-sharp btn-sharp-primary" 
									   data-toggle="collapse" href="#contactDealer">
										Contact Dealer
									</a>
									<div id="contactDealer" class="collapse">
										<!-- CONTACT DEALER FORM -->
										<?= do_shortcode( '[contact-form-7 id="'.$contact_dealer_form->ID.'" title="'.$contact_dealer_form->post_title.'"]' ); ?>
									</div>
									<!-- DEALER INFO -->
									<div>
										</br>
										<p>
											<!-- DEALER NAME -->
											<strong><?= $dealer_location->title; ?></strong>
											<br/>
											<!-- DEALER ADDRESS -->
											<?= $dealer_location->full_address; ?>
											</br>
											</br>
											<!-- DEALER CONTACT INFO-->
											Please contact us at: <strong><?= $dealer_location->phone; ?></strong>
										</p>
									</div>									
									<div>
										<?php if( $dealer_location->url2 ) : ?>
										<a target="_blank" href="<?= $dealer_location->url2; ?>">
											Visit dealer stock
										</a>
										<?php endif; ?>
										<?php if( $dealer_location->url3 ) : ?>
										&nbsp;&nbsp;&nbsp;&nbsp;
										<a target="_blank" href="<?= $dealer_location->url3; ?>">
											Visit group stock
										</a>
										<?php endif; ?>
									</div>			
								</div>					
							</div>
						</div>
						<!-- TECHNICAL SPECS -->
						<div>
							<div class="row">
								<div class="col-xs-12">
									<section class="section-placeholder">
										<div class="section-heading">
											<h2>Technical Specifications</h2>
										</div>			
										<div class="section-content">
											<div class="result-placeholder">
												<div class="result-content">
													<div class="result-list result-list-full">
														<!-- OPTIONAL EQUIPMENT -->
														<?php $opt_eqps = $specific_detail->opt_eqps; ?>
														<?php if( count($opt_eqps) > 0 ) : ?>
														<div class="result-list-heading">
															<a href="#optEquip" class="result-list-tab accordion-toggle collapsed" data-toggle="collapse" aria-expanded="false">															
																Optional Equipment
															</a>
														</div>
														<div class="result-list-content collapse" id="optEquip">
															<div class="row">
																<?php foreach( $opt_eqps as $index => $opt_eqp ) : ?>
																	<?php if( $index == 0 || $index % 10 == 0 ) : ?>
																	<div class="col-xs-12 col-sm-6">																	
																		<ul class="ul-custom ul-custom-check">
																	<?php endif; ?>	
																		<li>
																			<?= $opt_eqp->name; ?><?= $opt_eqp->description ? ' (' . $opt_eqp->description . ')' : ''; ?>
																		</li>
																	<?php if( ($index+1) % 10 == 0 || $index == count($opt_eqps)-1 ) : ?>
																		</ul>
																	</div>
																	<?php endif; ?>	
																<?php endforeach; ?>
															</div>
														</div>
														<?php endif; ?>
														<!-- STANDARD EQUIPMENT -->
														<?php $std_eqps = $specific_detail->std_eqps; ?>
														<?php if( count($std_eqps) > 0 ) : ?>
														<div class="result-list-heading">
															<a href="#stdEquip" class="result-list-tab accordion-toggle collapsed" data-toggle="collapse" aria-expanded="false">															
																Standard Equipment
															</a>
														</div>
														<div class="result-list-content collapse" id="stdEquip">
															<div class="row">																
																<?php foreach( $std_eqps as $index => $std_eqp ) : ?>
																	<?php if( $index == 0 || $index % 10 == 0 ) : ?>
																	<div class="col-xs-12 col-sm-6">																	
																		<ul class="ul-custom ul-custom-check">
																	<?php endif; ?>	
																		<li>
																			<?= $std_eqp->name; ?><?= $std_eqp->description ? ' (' . $std_eqp->description . ')' : ''; ?>
																		</li>
																	<?php if( ($index+1) % 10 == 0 || $index == count($std_eqps)-1 ) : ?>
																		</ul>
																	</div>
																	<?php endif; ?>	
																<?php endforeach; ?>
															</div>
														</div>
														<?php endif; ?>
														<!-- IMAGE GALLERY -->
														<div class="result-list-heading">
															<a href="#imgGallery" class="result-list-tab accordion-toggle" data-toggle="collapse" aria-expanded="false">															
																Image Gallery
															</a>
														</div>
														<div class="result-list-content collapse in no-padding" id="imgGallery">
															<?php if( count($specific_detail->gallery_images) <= 0 ) : ?>
																<br/>
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																No image found.
																<br/>
																<br/>
															<?php else : ?>
																<div class="row no-gutters">
																	<?php foreach($specific_detail->gallery_images as $index => $gallery_image) : ?>
																	<div class="col-xs-12 col-sm-4">
																		<a href="<?= $gallery_image->srcs['full']; ?>" class="swipebox"
																		   title="<?= $gallery_image->title; ?>">
																			<img src="<?= $gallery_image->srcs['medium']; ?>">
																		</a>
																	</div>
																	<?php endforeach; ?>
																</div>	
															<?php endif; ?>												
														</div>
														<!-- VIDEO -->
														<?php $is_video_available = true ?>
														<?php if( $is_video_available ) : ?>
														<div class="result-list-heading">
															<a href="#vidGallery" class="result-list-tab accordion-toggle" data-toggle="collapse" aria-expanded="false">															
																Video
															</a>
														</div>
														<div class="result-list-content collapse in no-padding" id="vidGallery">
															<div class="row">
																<div class="col-xs-12">
																	<!-- 16:9 aspect ratio -->
																	<div class="embed-responsive embed-responsive-16by9">
																		<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/EjhUgQz6TRM"></iframe>
																	</div>
																</div>
															</div>															
														</div>
														<?php endif ?>
														<!-- DEALER LOCATION -->
														<div class="result-list-heading">
															<a href="#dealerLoc" class="result-list-tab accordion-toggle" data-toggle="collapse" aria-expanded="false">															
																Dealer Location
															</a>
														</div>
														<div class="result-list-content collapse in no-padding" id="dealerLoc">
															<div class="row no-gutters">
																<div class="col-xs-12 col-sm-8 pull-right">
																	<div id="map" class="google-map"></div>
																	<div id="dealerLocation<?= $dealer_location->ID; ?>InfoWindowContent" style="display: none;">
																		<!-- TITLE -->
																		<?php if($dealer_location->url) : ?>
																			<a href="<?= $dealer_location->url; ?>" target="_blank">
																				<h5><?= $dealer_location->title; ?></h5>
																			</a>
																		<?php else : ?>
																			<h5><?= $dealer_location->title; ?></h5>
																		<?php endif; ?>
																		<!-- FULL ADDRESS -->
																		<?= $dealer_location->full_address; ?>
																		<br/>
																		<!-- TELEPHONE -->
																		<?php if(!empty($dealer_location->phone)) : ?>
																			Tel: <strong><?= $dealer_location->phone ?></strong>
																			<br/>
																		<?php endif; ?>
																		<br/>
																		Get directions: 
																		<a style="font-size: 14px;" target="_blank" 
																			href="<?= $dealer_location->maps['google']->daddr; ?>">to here</a>
																		 - <a style="font-size: 14px;" target="_blank" 
																		 		href="<?= $dealer_location->maps['google']->saddr; ?>">from here</a>
																	</div>
																</div>
																<div class="col-xs-12 col-sm-4">
																	<div class="padder-box">
																		<!-- DEALER NAME -->
																		<strong><?= $dealer_location->title; ?></strong>
																		<br/>
																		<!-- DEALER ADDRESS -->
																		<?= $dealer_location->full_address; ?>
																		</br>
																		</br>
																		<!-- DEALER CONTACT INFO-->
																		Please contact us at: <strong><?= $dealer_location->phone; ?></strong>
																		<br/>
																		<br/>
																		<button id="btnDealerLocation<?= $dealer_location->ID; ?>ContactDealer" 
																				type="button" class="btn btn-sharp btn-sharp-primary btn-width-max">
																			Contact Dealer
																		</button>
																		<a type="button" target="_blank" class="btn btn-sharp btn-sharp-primary btn-width-max"
																			href="<?= $dealer_location->maps['google']->daddr; ?>">
																			Get Directions
																		</a>
																		<?php if( $dealer_location->url2 ) : ?>
																		<a type="button" class="btn btn-sharp btn-sharp-primary btn-width-max"
																		   href="<?= $dealer_location->url2; ?>" target="_blank">
																			Visit Dealer Stock
																		</a>
																		<?php endif; ?>
																		<?php if( $dealer_location->url3 ) : ?>
																		<a type="button" class="btn btn-sharp btn-sharp-primary btn-width-max"
																		   href="<?= $dealer_location->url3; ?>" target="_blank">
																			Visit Group Stock
																		</a>
																		<?php endif; ?>
																	</div>
																</div>
															</div>															
														</div>
													</div>	
												</div>
											</div>
										</div>
								</div>
							</div>
						</div>							
					</div>
				</div>
			</div>
		</section>

	</div>
	
</div>

<script type="text/javascript">
	jQuery("document").ready(function(){

		var post_id = <?= $vehicle->ID; ?>;

		var dealer_locations = [];
		var dealer_location = <?= json_encode($dealer_location); ?>;
		dealer_locations.push(dealer_location);
		
		pageVehicleDetails.init({ 
			post_id: post_id
			, locations: dealer_locations 
		});

	});
</script>

<?php get_footer();
