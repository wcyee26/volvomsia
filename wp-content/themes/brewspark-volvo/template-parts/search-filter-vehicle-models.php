<?php 

$_page_url_search = home_url('/') . '?s=';

?>

<?php $vehicle_serieses = BWooVehicle::get_vehicle_serieses(); ?>
<?php foreach( $vehicle_serieses as $vehicle_series_index => $vehicle_series ) : ?>
	<?php if( $vehicle_series_index == 0 || $vehicle_series_index%3 == 0 ) : ?>
	<div class="row">		
	<?php endif; ?>
		<div class="col-sm-4">
			<div class="checkbox-list-placeholder">
				<div class="checkbox-list-heading">
					<h3><?= $vehicle_series->letter_head; ?></h3>&nbsp;&nbsp;
					<?php $vehicle_model_ids = $vehicle_series->children;
						  $url_vehicle_series = $_page_url_search;
						  foreach( $vehicle_model_ids as $vehicle_model_id ){
						  		$url_vehicle_series .= "&fieldVehicleModels[]=" . (String) $vehicle_model_id;
						  }
					?>
					<a href="<?= $url_vehicle_series; ?>">View All</a>
				</div>	
				<div class="checkbox-list-content">
					<ul>
						<?php $vehicle_models = BWooVehicle::get_vehicle_models( array('include' => $vehicle_series->children) ) ?>
						<?php foreach($vehicle_models as $key => $vehicle_model) : ?>
						<li>														
							<div id="vehicleModel<?php echo $vehicle_model->term_id; ?>" 
								 class="checkbox checkbox-custom checkbox-custom-thumbnail">																																		
							    <label>
							    	<div class="checkbox-custom-image">																    		
					    		    	<img src="<?php echo $vehicle_model->featured_image->srcs['medium']; ?>">
							    	</div>														    	
							    	<input id="fieldVehicleModel<?php echo $vehicle_model->term_id; ?>" 
							    		   <?php if( isset($_GET['fieldVehicleModels']) ) : ?>
								    		   <?php foreach( $_GET['fieldVehicleModels'] as $index => $val ) : ?>
								    	 	   <?php echo $val == $vehicle_model->term_id ? 'checked' : ''; ?>
									    	   <?php endforeach; ?>
									       <?php endif; ?>
							    		   name="fieldVehicleModels[]" 
							    		   type="checkbox" 
							    		   data-vehicle-models-count="<?php echo $vehicle_model->count; ?>"
							    		   data-vehicle-models-slug="<?php echo $vehicle_model->slug; ?>"
							    		   value="<?php echo $vehicle_model->term_id; ?>"/>
							    	<span class="checkbox-custom-heading">
							    		<h4><?php echo $vehicle_model->name; ?></h4>
							    		<span id="vehicleModel<?php echo $vehicle_model->term_id; ?>Count" 
							    			  class="checkbox-custom-heading-subtitle pull-right">
							    			<?php echo $vehicle_model->count; ?>
							    		</span>
							    	</span>
							    	<br/>
							    	<sup class="checkbox-custom-content"><?php echo $vehicle_model->short_description; ?></sup>
							    </label>																    
							</div>																
						</li>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>										
		</div>
	<?php if( ($vehicle_series_index+1)%3 == 0 || $vehicle_series_index == count($vehicle_serieses)-1 ) : ?>
	</div>
	<br/>
	<?php endif; ?>
<?php endforeach; ?>