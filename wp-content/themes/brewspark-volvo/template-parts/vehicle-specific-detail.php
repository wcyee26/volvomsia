<?php 
	
	$vehicle_specific_detail = BWooVehicle::get_vehicle_specific_detail( $post_id );
	$specific_detail = $vehicle_specific_detail; 

	$_page_url_vehicle_details = get_page_link( get_page_by_path('vehicle-details')->ID );

?>
<div class="result-list-content" id="vehicle<?= $specific_detail->vehicle_ID; ?>SpecificDetail">
	<div class="row">
		<div class="col-sm-4" style="width: 30%;">
			<ul class="ul-custom ul-custom-tabs">
				<!-- <li> -->
				<li class="active">
					<a href="#vehicle<?= $specific_detail->vehicle_ID; ?>SpecificDetailStdEqps" data-toggle="tab">
						Standard Equipment
					</a>
				</li>
				<li>
					<a href="#vehicle<?= $specific_detail->vehicle_ID; ?>SpecificDetailOptEqps" data-toggle="tab">
						Optional Equipment
					</a>
				</li>
				<li>
					<a href="#vehicle<?= $specific_detail->vehicle_ID; ?>SpecificDetailPictures" data-toggle="tab">
						Pictures
					</a>
				</li>
				<!-- <li class="active"> -->
				<li>
					<a href="#vehicle<?= $specific_detail->vehicle_ID; ?>SpecificDetailContact" data-toggle="tab">
						Contact
					</a>
				</li>
			</ul>
		</div>
		<div class="col-sm-5" style="width: 50%;">
			<div class="tab-content">
				<!-- STANDARD EQUIPMENT -->
				<div class="tab-pane active" id="vehicle<?= $specific_detail->vehicle_ID; ?>SpecificDetailStdEqps">
				<!-- <div class="tab-pane" id="vehicle<?= $specific_detail->vehicle_ID; ?>SpecificDetailStdEqps"> -->
					<div class="tab-pane-heading">Standard Equipment</div>
					<div class="tab-pane-content">
						<ul class="ul-custom ul-custom-check">
							<?php foreach( $specific_detail->std_eqps as $index => $std_eqp ) : ?>
								<li><?= $std_eqp->name; ?>&nbsp;<?= ($std_eqp->description) ? '(' . $std_eqp->description . ')' : ''; ?></li>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>
				<!-- OPTIONAL EQUIPMENT -->
				<div class="tab-pane" id="vehicle<?= $specific_detail->vehicle_ID; ?>SpecificDetailOptEqps">
					<div class="tab-pane-heading">Optional Equipment</div>
					<div class="tab-pane-content">
						<ul class="ul-custom ul-custom-check">
							<?php foreach( $specific_detail->opt_eqps as $index => $opt_eqp ) : ?>
								<li><?= $opt_eqp->name; ?>&nbsp;<?= ($opt_eqp->description) ? '(' . $opt_eqp->description . ')' : ''; ?></li>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>
				<!-- PICTURES -->
				<div class="tab-pane" id="vehicle<?= $specific_detail->vehicle_ID; ?>SpecificDetailPictures">
					<div class="tab-pane-heading">Pictures</div>
					<div class="tab-pane-content">
						<div class="row no-gutters">
						<?php foreach( $specific_detail->gallery_images as $index => $gallery_image ) : ?>
							<div class="col-sm-4">
								<a href="<?= $gallery_image->srcs['full']; ?>" class="swipebox" 
								   title="<?= $gallery_thumbnail_image->title; ?>">
									<img src="<?= $gallery_image->srcs['thumbnail']; ?>">
								</a>
							</div>
						<?php endforeach; ?>
						</div>
					</div>
				</div>
				<!-- CONTACT -->
				<div class="tab-pane" id="vehicle<?= $specific_detail->vehicle_ID; ?>SpecificDetailContact">
				<!-- <div class="tab-pane active" id="vehicle<?= $specific_detail->vehicle_ID; ?>SpecificDetailContact"> -->
					<div class="tab-pane-heading">Contact</div>
					<div class="tab-pane-content">
						<p class="no-margin">
							<?php $dealer_location = $specific_detail->dealer_location; ?>
							<!-- DEALER NAME -->
							<strong><?= $dealer_location->title; ?></strong>
							<br/>
							<!-- DEALER ADDRESS -->
							<?= $dealer_location->full_address; ?>
							<br/>
							<br/>
							<!-- DEALER CONTACT INFO-->
							Please contact us at: <strong><?= $dealer_location->phone; ?></strong>
							<br/>
							<br/>
							<a type="button" class="btn btn-sharp btn-sharp-primary"
							   href="<?= $_page_url_vehicle_details ?>?post_id=<?= $specific_detail->vehicle_ID; ?>#contactDealer">
								Contact Dealer
							</a>																	
							<a type="button" class="btn btn-sharp btn-sharp-primary" target="_blank"
							   href="<?= $dealer_location->maps['google']->daddr; ?>">
								Get Directions
							</a>
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-3" style="width: 20%;">
			<button id="btnVehicle<?= $specific_detail->vehicle_ID; ?>ReadLess" type="button" class="btn btn-sharp btn-sharp-primary btn-width-max">Read Less</button>
		</div>
	</div>
</div>