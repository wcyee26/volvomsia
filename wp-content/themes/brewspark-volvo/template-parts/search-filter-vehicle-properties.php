<!-- PRICE -->
<div id="customFilterPrice" class="custom-filter-placeholder custom-filter-slider">
	<div class="custom-filter-slider-heading clearfix">														
		<label>Price</label>
		<div id="sliderPriceDescr" class="pull-right custom-filter-slider-descr">
		</div>
	</div>
	<div class="custom-filter-slider-content">														
		<div id="sliderPrice"></div>
		<input type="hidden" id="fieldSliderPriceStart" name="fieldSliderPrices[]" 
			   value="<?php echo isset( $_GET['fieldSliderPrices'][0] ) ? $_GET['fieldSliderPrices'][0] : '' ?>" />
		<input type="hidden" id="fieldSliderPriceEnd" name="fieldSliderPrices[]" 
			   value="<?php echo isset( $_GET['fieldSliderPrices'][1] ) ? $_GET['fieldSliderPrices'][1] : '' ?>" />
	</div>
</div>
<!-- MILEAGE -->
<div id="customFilterMileage" class="custom-filter-placeholder custom-filter-slider">
	<div class="custom-filter-slider-heading clearfix">														
		<label>Mileage</label>
		<div id="sliderMileageDescr" class="pull-right custom-filter-slider-descr">
		</div>
	</div>
	<div class="custom-filter-slider-content">														
		<div id="sliderMileage"></div>
		<input type="hidden" id="fieldSliderMileageStart" name="fieldSliderMileages[]" 
			   value="<?php echo isset( $_GET['fieldSliderMileages'][0] ) ? $_GET['fieldSliderMileages'][0] : '' ?>" />
		<input type="hidden" id="fieldSliderMileageEnd" name="fieldSliderMileages[]" 
			   value="<?php echo isset( $_GET['fieldSliderMileages'][1] ) ? $_GET['fieldSliderMileages'][1] : '' ?>" />
	</div>
</div>
<!-- AGE -->
<div id="customFilterAge" class="custom-filter-placeholder custom-filter-slider custom-filter-advanced-field">
	<div class="custom-filter-slider-heading clearfix">														
		<label>Age</label>
		<div id="sliderAgeDescr" class="pull-right custom-filter-slider-descr">
		</div>
	</div>
	<div class="custom-filter-slider-content">														
		<div id="sliderAge"></div>
		<input type="hidden" id="fieldSliderAgeStart" name="fieldSliderAges[]" 
			   value="<?php echo isset( $_GET['fieldSliderAges'][0] ) ? $_GET['fieldSliderAges'][0] : '' ?>" />
		<input type="hidden" id="fieldSliderAgeEnd" name="fieldSliderAges[]"
			   value="<?php echo isset( $_GET['fieldSliderAges'][1] ) ? $_GET['fieldSliderAges'][1] : '' ?>" />
	</div>
</div>
<!-- CO2EMISSION -->
<div id="customFilterCo2Emission" class="custom-filter-placeholder custom-filter-slider custom-filter-advanced-field">
	<div class="custom-filter-slider-heading clearfix">														
		<label>CO2 Emission</label>
		<div id="sliderCo2EmissionDescr" class="pull-right custom-filter-slider-descr">
		</div>
	</div>
	<div class="custom-filter-slider-content">														
		<div id="sliderCo2Emission"></div>
		<input type="hidden" id="fieldSliderCo2EmissionStart" name="fieldSliderCo2Emissions[]" 
			   value="<?php echo isset( $_GET['fieldSliderCo2Emissions'][0] ) ? $_GET['fieldSliderCo2Emissions'][0] : '' ?>" />
		<input type="hidden" id="fieldSliderCo2EmissionEnd" name="fieldSliderCo2Emissions[]" 
			   value="<?php echo isset( $_GET['fieldSliderCo2Emissions'][1] ) ? $_GET['fieldSliderCo2Emissions'][1] : '' ?>" />
	</div>
</div>
<!-- FUEL TYPES -->
<div id="customFilterFuelType" class="custom-filter-placeholder custom-filter-advanced-field">													
	<label>Fuel Type</label>
	<div>
		<select id="fieldFuelType" name="fieldFuelType" data-term-taxonomy="pa_fuel-type">
			<option value="-1">View all</option>
			<?php $fuel_types = BWooVehicle::get_fuel_types(); ?>
			<?php foreach ($fuel_types as $key => $fuel_type) : ?>
			<option value="<?php echo $fuel_type->term_id; ?>" 
				<?php echo $_GET['fieldFuelType'] == $fuel_type->term_id ? 'selected' : ''; ?>>
				<?php echo $fuel_type->name; ?>
			</option>
			<?php endforeach; ?>
		</select>
	</div>
</div>
<!-- TRANSMISSION TYPES -->
<div id="customFilterTransmissionType" class="custom-filter-placeholder custom-filter-advanced-field">													
	<label>Transmission</label>
	<div>
		<select id="fieldTransmissionType" name="fieldTransmissionType" data-term-taxonomy="pa_transmission_type">
			<option value="-1">View all</option>
			<?php $transmission_types = BWooVehicle::get_transmission_types(); ?>
			<?php foreach ($transmission_types as $key => $transmission_type) : ?>
			<option value="<?php echo $transmission_type->term_id; ?>"
				<?php echo $_GET['fieldTransmissionType'] == $transmission_type->term_id ? 'selected' : ''; ?>>
				<?php echo $transmission_type->name; ?>
			</option>
			<?php endforeach; ?>
		</select>
	</div>
</div>
<!-- EXTERIOR COLOURS -->
<div id="customFilterExteriorColour" class="custom-filter-placeholder custom-filter-advanced-field">													
	<label>Colour</label>
	<div>
		<select id="fieldExteriorColour" name="fieldExteriorColour" data-term-taxonomy="pa_exterior-colour">
			<option value="-1">View all</option>
			<?php $exterior_colours = BWooVehicle::get_exterior_colours(); ?>
			<?php foreach ($exterior_colours as $key => $exterior_colour) : ?>
			<option value="<?php echo $exterior_colour->term_id; ?>"
				<?php echo $_GET['fieldExteriorColour'] == $exterior_colour->term_id ? 'selected' : ''; ?>>
				<?php echo $exterior_colour->name; ?>
			</option>
			<?php endforeach; ?>
		</select>
	</div>
</div>