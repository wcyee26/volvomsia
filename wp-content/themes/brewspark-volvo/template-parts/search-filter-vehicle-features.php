<!-- FEATURES CHECKLIST -->
<?php $vehicle_features = BWooVehicle::get_vehicle_features(); ?>
<?php foreach($vehicle_features as $key => $vehicle_feature) : ?>
<div id="customFilter<?php echo BWooVehicle::ucslug($vehicle_feature->slug); ?>" 
	 class="custom-filter-placeholder 
	 		<?php if($key>2) echo 'custom-filter-advanced-field'; ?>">		
	<div id="vehicleFeature<?php echo $vehicle_feature->term_id; ?>" class="checkbox checkbox-custom">																																		
	    <label>										    	
	    	<!-- INPUT -->
	    	<input type="checkbox" 
	    		   id="fieldVehicleFeature<?php echo $vehicle_feature->term_id; ?>" 
	    		   name="fieldVehicleFeatures[]" 
	    			<?php if( isset($_GET['fieldVehicleFeatures']) ) : ?>
	    			<?php foreach($_GET['fieldVehicleFeatures'] as $index => $val) : ?>
	    			<?php echo $val == $vehicle_feature->term_id ? 'checked' : ''; ?>
		    		<?php endforeach; ?>
	    			<?php endif; ?>
	    		   value="<?php echo $vehicle_feature->term_id; ?>"/>
    		<!-- LABEL -->
	    	<?php if($vehicle_feature->slug == 'volvo-selekt') : ?>
	    	<div class="<?= $vehicle_feature->slug; ?>">
	    		<img src="<?php echo get_template_directory_uri() . '/assets/images/icon-volvo-selekt.png'; ?>"	    			 
	    			 width="70%">
			</div>	    	
		    <?php else : ?>
	    	<?php echo $vehicle_feature->name; ?>
		    <?php endif; ?>
	    </label>																    
	</div>
</div>
<?php endforeach; ?>