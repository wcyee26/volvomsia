<?php

global $wp;
$current_url = add_query_arg( null, null, home_url( $wp->request . '/' ) );

$main_menu_items = wp_get_nav_menu_items('Main Menu');

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<title>
		<?php wp_title('|', true, 'right'); ?><?php bloginfo('name'); ?>
	</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	<script type="text/javascript">
		var obj_units = {
			price: "<?= get_woocommerce_currency_symbol(get_woocommerce_currency()); ?>"
			, distance: "<?= get_option('bwoovehicle_distance_unit'); ?>"
			, engine_size: "<?= get_option('bwoovehicle_engine_size_unit'); ?>"
			, engine_power: "<?= get_option('bwoovehicle_engine_power_unit'); ?>"
			, co2_emission: "<?= get_option('bwoovehicle_co2_emission_unit'); ?>"
		};

		// init global units
		globalSettings.init({
			obj_units: obj_units
		});
	</script>
</head>

<body <?php body_class(); ?>>

	<div class="site">
		<header class="site-header">
			<!-- MAIN NAV BAR -->
			<nav class="navbar navbar-inverse navbar-fixed-top">
				<div class="container-fluid">
					<!-- HEADER -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#navbar" 
								aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="<?= home_url('/'); ?>">
							<img alt="Brand" src="<?= get_template_directory_uri() ?>/assets/images/logo.png">
						</a>
					</div>
					<!-- MENU -->
					<div id="navbar" class="collapse navbar-collapse">						
						<ul class="nav navbar-nav">
							<?php foreach ($main_menu_items as $index => $main_menu_item) : ?>
							<?php if( $index < 2 ) : ?>
								<li class="<?= ( $main_menu_item->url == $current_url ) ? 'active' : '' ?>"
									<?= ( $index == 0 ) ? 'style="margin-right: 55px;"' : '' ?>>								
									<a href="<?= $main_menu_item->url; ?>"><?= strtoupper($main_menu_item->title); ?></a>
								</li>
							<?php endif; ?>
							<?php endforeach; ?>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<?php foreach ($main_menu_items as $index => $main_menu_item) : ?>
							<?php if( $index >= 2 ) : ?>
								<li class="<?= ( $main_menu_item->url == $current_url ) ? 'active' : '' ?>"
									<?= ( $index != count($main_menu_items)-1 ) ? 'style="margin-right: 55px;"' : '' ?>>
									<a href="<?= $main_menu_item->url; ?>"><?= strtoupper($main_menu_item->title); ?></a>
								</li>
							<?php endif; ?>
							<?php endforeach; ?>
						</ul>
						<!-- ONLY SHOW IN MOBILE, ADDITIONAL NAV BAR (MY COMPARISON, MY SEARCHES) -->
						<ul class="nav navbar-nav navbar-right visible-xs-block">
							<li class="<?= strpos($current_url, 'my-comparison') !== false ? 'active' : '' ?>">
								<a id="mobileMyComparison" href="javascript:void(0);" class="disabled" 
								   style="margin-right: 55px">
									MY COMPARISON (<span id="mobileMyComparisonCount">0</span>)
								</a>
							</li>
							<li class="<?= strpos($current_url, 'my-searches') !== false ? 'active' : '' ?>">
								<a id="mobileMySearches" href="javascript:void(0);" class="disabled">
									MY SEARCHES (<span id="mobileMySearchesCount">0</span>)
								</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>	
			<!-- ADDITIONAL NAV BAR (MY COMPARISON, MY SEARCHES) -->
			<nav class="navbar navbar-light" style="position: absolute; right: 0; top: 90px; z-index: 2;">
				<div class="container-fluid">
					<div class="collapse navbar-collapse" style="padding-left: 0; padding-right: 0;">						
						<ul class="nav navbar-nav">
							<li>
								<a id="myComparison" href="javascript:void(0);" class="disabled">
									MY COMPARISON (<span id="myComparisonCount">0</span>)
								</a>
							</li>
							<li>
								<a id="mySearches" href="javascript:void(0);" class="disabled" style="margin-left: 55px">
									MY SEARCHES (<span id="mySearchesCount">0</span>)
								</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>	
		</header>

		<div id="content" class="site-content">

		