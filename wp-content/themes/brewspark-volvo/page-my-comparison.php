<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Volvo Malaysia
 * @since 1.0
 * @version 1.0
 */

get_header(); 

$fieldPostIds = isset($_GET['fieldPostIds']) ? $_GET['fieldPostIds'] : null;

if($fieldPostIds){
	$vehicle_ids = $fieldPostIds;
}

$alphabet_letters = 'ABCDE';

$args = array('post__in' => $vehicle_ids);

$vehicles = BWooVehicle::get_vehicles( $args );
$vehicles_count = count($vehicles);

$vehicle_attributes = array(
						'mileage' => 'Mileage'
						, 'created_year' => 'Model year'
						, 'engine_size' => 'Engine size'
						, 'engine_power' => 'Engine power'
						, 'fuel_type' => 'Fuel Type'
						, 'transmission_type' => 'Transmission type'
						, 'ext_colour' => 'Exterior colour'
						, 'reg_date' => 'Registration date'
						);

if($vehicles_count <= 1){
	$col_heading_class = 'col-sm-5';
	$col_content_class = 'col-sm-7';
}
elseif ($vehicles_count <= 2) {
	$col_heading_class = 'col-sm-2';
	$col_content_class = 'col-sm-5';
}
elseif ($vehicles_count <= 3) {
	$col_heading_class = 'col-sm-3';
	$col_content_class = 'col-sm-3';
}

$_page_url_vehicle_details = get_page_link( get_page_by_path('vehicle-details')->ID );

?>

<div class="site-content-container">
	<div class="container">
		<section class="section-placeholder section-placeholder-primary">
			<div class="section-heading">
				<h2>My Comparison</h2>
			</div>			
			<!-- MY COMPARISON -->
			<div class="section-content">
				<div class="row">
					<div class="col-xs-offset-1 col-xs-10 col-sm-offset-1 col-sm-10">
						<div class="row">
							<!-- WEB -->
							<div class="col-sm-12">
								<!-- VEHICLE CARDS -->
								<div class="row">
									<div class="<?= $col_heading_class ?>"></div>
									<?php foreach($vehicles as $index => $vehicle) : ?>
										<div class="<?= $col_content_class ?>">
											<div class="card-brochure" style="margin-top: 30px;">
												<div class="card-brochure-heading" style="min-height: 40px;">
													<span class="visible-xs-inline">
														<strong>
															(<?= $alphabet_letters[$index]; ?>)
														</strong>
													</span>
													<?= $vehicle->title; ?>
												</div>
												<div class="card-brochure-thumbnail no-margin" style="min-height: 170px;">
													<a href="<?= $_page_url_vehicle_details . '?post_id=' . $vehicle->ID; ?>">
														<img src="<?= $vehicle->featured_image->srcs['large']; ?>">
													</a>
												</div>
												<div class="card-brochure-total card-brochure-total-light" 
													 style="border-top-width: 0; padding-right: 0; padding-left: 0;">
													<!-- PRICE -->
													<?= $vehicle->price_unit . ' ' . $vehicle->price; ?>
													</br>
													<!-- VOLVO SELEKT -->
													<?php $features = $vehicle->features; ?>
													<?php if(count($features) == 0) : ?>
														&nbsp;
													<?php endif; ?>
													<?php foreach($features as $key => $feature) : ?>
														<?php if($feature->slug == 'volvo-selekt') : ?>
															<img src="<?= get_template_directory_uri() ?>/assets/images/icon-volvo-selekt.png" 
																 class="<?= $feature->slug; ?>"
																 style="width: 70%;">
														<?php elseif($key == 1) : ?>
															&nbsp;
														<?php endif; ?>
													<?php endforeach; ?>
												</div>
												
											</div>
										</div>
									<?php endforeach; ?>
								</div>
								<!-- VEHICLE DETAILS -->
								<section class="section-placeholder">
									<div class="section-content" style="padding-top: 10px; padding-bottom: 0;">
										<div class="result-placeholder">
											<div class="result-content">
												<div class="result-list result-list-full">
													<!-- DEALER LOCATION -->
													<div class="result-list-heading">
														<a href="#dealerLocation" class="result-list-tab accordion-toggle collapsed" data-toggle="collapse" aria-expanded="false">
															Dealer Location
														</a>
													</div>
													<div class="result-list-content collapse in" id="dealerLocation">
														<!-- CONTACT INFO -->
														<div class="row">
															<div class="<?= $col_heading_class ?>"></div>
															<?php foreach($vehicles as $index => $vehicle) : ?>
															<?php $dealer_location = $vehicle->dealer_location; ?>
															<div class="<?= $col_content_class ?>">
																<span class="visible-xs-inline">
																	<strong>(<?= $alphabet_letters[$index]; ?>)</strong>
																</span>
																<strong><?= $dealer_location->title; ?></strong>
																<br/>
																<?= $dealer_location->full_address; ?>
																<br/>
																<br/>
																<?php if(!empty($dealer_location->phone)) : ?>
																	Tel: <strong><?= $dealer_location->phone ?></strong>
																	<br/>
																	<br/>
																<?php endif; ?>
																<a type="button" class="btn btn-sharp btn-sharp-primary btn-width-max"
																   href="<?= $_page_url_vehicle_details ?>?post_id=<?= $vehicle->ID; ?>#contactDealer">
																	<span class="visible-xs-inline">
																		<strong>(<?= $alphabet_letters[$index]; ?>)</strong>
																	</span>
																	Contact Dealer
																</a>
															</div>
															<?php endforeach; ?>
														</div>
														<!-- ACTIONS -->
														<!-- <div class="row">
															<div class="<?= $col_heading_class ?>"></div>
															<?php foreach($vehicles as $index => $vehicle) : ?>
															<div class="<?= $col_content_class ?>">
																<button type="button" class="btn btn-sharp btn-sharp-primary btn-width-max">
																	Contact Dealer
																</button>
															</div>
															<?php endforeach; ?>
														</div> -->
													</div>
													<!-- VEHICLE DETAILS -->
													<div class="result-list-heading">
														<a href="#vehicleDetails" class="result-list-tab accordion-toggle collapsed" data-toggle="collapse" aria-expanded="false">
															Vehicle Details
														</a>
													</div>
													<div class="result-list-content no-padding collapse in" id="vehicleDetails">
														<?php $vehicle_attributes_counter = 1; ?>
														<?php foreach($vehicle_attributes as $key => $vehicle_attribute) : ?>
														<div class="result-list-content-row 
																	<?= ($vehicle_attributes_counter%2) == 1 
																		? 'result-list-content-row-deep-grey' : ''; 
																	?>"
																	>
															<div class="row">
																<div class="<?= $col_heading_class ?> result-list-content-row-heading">
																	<?= $vehicle_attribute ?>
																</div>
																<?php foreach($vehicles as $index => $vehicle) : ?>
																<div class="<?= $col_content_class ?> result-list-content-row-content">
																	<span class="visible-xs-inline">
																		(<?= $alphabet_letters[$index]; ?>)
																	</span>
																	<?php $key_unit = $key . '_unit'; ?>
																	<?php if($key == 'mileage') : ?>
																		<?= $vehicle->$key . ' ' . $vehicle->$key_unit; ?>
																	<?php elseif($key == 'created_year') : ?>
																		<?= $vehicle->vehicle_model->$key; ?>
																	<?php elseif($key == 'engine_size') : ?>
																		<?= $vehicle->$key . ' ' . $vehicle->$key_unit; ?>
																	<?php elseif($key == 'engine_power') : ?>
																		<?= $vehicle->$key . ' ' . $vehicle->$key_unit; ?>
																	<?php elseif($key == 'fuel_type') : ?>
																		<?= $vehicle->$key->name; ?>
																	<?php elseif($key == 'transmission_type') : ?>
																		<?= $vehicle->$key->name; ?>
																	<?php elseif($key == 'ext_colour') : ?>
																		<?= $vehicle->$key->name; ?>
																	<?php elseif($key == 'reg_date') : ?>
																		<?php $reg_date = date_create( $vehicle->key ); ?>
																		<?php $reg_date = date_format( $reg_date, "Y M" ); ?>
																		<?= $reg_date; ?>
																	<?php endif; ?>
																</div>
																<?php endforeach; ?>
															</div>
														</div>
														<?php $vehicle_attributes_counter += 1; ?>
														<?php endforeach; ?>
													</div>
													<!-- OPTIONAL EQUIPMENT -->
													<div class="result-list-heading">
														<a href="#optEqp" class="result-list-tab accordion-toggle collapsed" data-toggle="collapse" aria-expanded="false">
															Optional Equipment
														</a>
													</div>
													<div class="result-list-content no-padding collapse in" id="optEqp">
														<div class="result-list-content-row">
															<div class="row">
																<div class="<?= $col_heading_class ?> result-list-content-row-heading"></div>
																<?php foreach ($vehicles as $index => $vehicle) : ?>
																<div class="<?= $col_content_class ?> result-list-content-row-content">
																	<span class="visible-xs-inline">
																		(<?= $alphabet_letters[$index]; ?>)
																	</span>
																	<?php if(count($vehicle->features) > 0) : ?>
																	<ul class="ul-custom ul-custom-bullet">
																		<?php foreach ($vehicle->features as $feature) : ?>
																			<?php if($feature->slug != 'volvo-selekt') : ?>
																				<li><?= $feature->name; ?></li>
																			<?php endif; ?>
																		<?php endforeach; ?>
																	</ul>
																	<?php else : ?>
																		(None)
																		<br/><br/>
																	<?php endif; ?>
																</div>
																<?php endforeach; ?>
															</div>
														</div>
													</div>
													<!-- CO2 EMISSION -->
													<div class="result-list-heading">
														<a href="#co2Emission" class="result-list-tab accordion-toggle collapsed" data-toggle="collapse" aria-expanded="false">
															Environmental Info
														</a>
													</div>
													<div class="result-list-content no-padding collapse in" id="co2Emission">
														<div class="result-list-content-row">
															<div class="row">
																<div class="<?= $col_heading_class ?> result-list-content-row-heading">
																	CO2 Emission
																</div>
																<?php foreach ($vehicles as $index => $vehicle) : ?>
																<div class="<?= $col_content_class ?> result-list-content-row-content">
																	<span class="visible-xs-inline">
																		(<?= $alphabet_letters[$index]; ?>)
																	</span>
																	<?= $vehicle->co2_emission . ' ' . $vehicle->co2_emission_unit; ?>
																</div>
																<?php endforeach; ?>
															</div>
														</div>
													</div>
													<!-- ACTIONS -->
													<div class="result-list-footer" style="padding-left: 0; padding-right: 0;">
														<div class="row">
															<div class="<?= $col_heading_class ?>"></div>
															<?php foreach ($vehicles as $index => $vehicle) : ?>
															<div class="<?= $col_content_class ?>">
																<div class="row no-gutters">
																	<div class="col-xs-1 visible-xs-inline">
																		<span>
																			<strong>(<?= $alphabet_letters[$index]; ?>)</strong>
																		</span>
																	</div>
																	<div class="col-xs-5 col-sm-12">
																		<button id="btnVehicle<?= $vehicle->ID ?>FullDetails" 
																			data-vehicle-id=
																			type="button" 
																			class="btn btn-sharp btn-sharp-secondary btn-width-max">
																			Full Details
																		</button>
																	</div>
																	<div class="col-xs-6 col-sm-12">
																		<button id="btnVehicle<?= $vehicle->ID ?>RemoveVehicle" 
																			type="button" 
																			class="btn btn-sharp btn-sharp-primary btn-width-max">
																			Remove Vehicle
																		</button>
																	</div>
																</div>
															</div>
															<?php endforeach; ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</section>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

<script type="text/javascript">
	
	jQuery("document").ready(function(){

		var post_ids = [];

		<?php foreach( $vehicles as $index => $vehicle ) : ?>
			post_ids.push(<?= $vehicle->ID; ?>);
		<?php endforeach ?>

		pageMyComparison.init({ post_ids : post_ids });

	});

</script>

<?php get_footer();