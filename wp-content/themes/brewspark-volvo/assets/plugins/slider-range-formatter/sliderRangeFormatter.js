//This plugin requires wNumb library
var sliderRangeFormatter = function () {
	return {
		format: function(options){
			var min = parseFloat(options.min);
			var max = parseFloat(options.max);
			var start = parseFloat(options.start);
			var end = parseFloat(options.end);
			var formatOptions = options.formatOptions;

			var output = "";

			var wNumbFormatter = wNumb(formatOptions);

			if(start > min && end >= max){
				output = "from " + wNumbFormatter.to(start);
			}
			else if(start <= min && end < max){
				output = "to " + wNumbFormatter.to(end);
			}
			else if(start > min && end < max){
				output = wNumbFormatter.to(start) + " - " + wNumbFormatter.to(end);
			}

			return output;
		}
	};
}();