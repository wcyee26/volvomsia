// This plugin requires Bootstrap css for responsive utilities, eg. xs, sm, md, lg
function BrewsparkGallerySlideshow( gallerySlideshowId ){

	var _root = this;

	var gallerySlideshowClass = "brewspark-gallery-slideshow";
	var gallerySlideshowSlideClass = "brewspark-gallery-slideshow-slide";

	var dataGallerySlideshowSlideIndex = "brewspark-gallery-slideshow-slide-index";

	var gallerySlideshowSelector = "#"+gallerySlideshowId+'.'+gallerySlideshowClass;	
	var gallerySlideshowSlideSelector = "#"+gallerySlideshowId+'.'+gallerySlideshowClass+' .'+gallerySlideshowSlideClass;	

	jQuery(gallerySlideshowSlideSelector).css("display", "none");

	_root.display = function(page, pageLength, callbackFunc){

		if(page === undefined){
			page = 1;
		}
		if(pageLength === undefined){
			pageLength = 3;
		}

		jQuery(gallerySlideshowSlideSelector).each(function(index, val){
			var slideIndex = jQuery(this).data(dataGallerySlideshowSlideIndex);

			if(slideIndex >= page && slideIndex <= page*pageLength){
				callbackFunc(this);
			}
		});
	};

	_root.clear = function(){
		jQuery(gallerySlideshowSlideSelector).removeClass("visible-xs visible-sm visible-md visible-lg");
	};

	_root.length = function(){
		return jQuery(gallerySlideshowSlideSelector).length;
	};
}