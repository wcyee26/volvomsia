//This is used for setting up search filter, can be used globally
var globalSearchFilter = function () {

	var _root = this;
	_root.form_id = 'formGlobalSearchFilter';	

	var _ladda_btn_search = Ladda.create( document.querySelector( "#btnSearch" ) );

	var handleSettings = function(){

		// global units such as km, L, etc.
		var _obj_units = globalSettings.getUnits();
		var _obj_unit_ranges = globalSettings.getUnitRanges();

		var _price_unit = _obj_units.price;
		var _distance_unit = _obj_units.distance;
		var _engine_size_unit = _obj_units.engine_size;
		var _engine_power_unit = _obj_units.engine_power;
		var _co2_emission_unit = _obj_units.co2_emission;

		var _price_range = _obj_unit_ranges.price;
		var _mileage_range = _obj_unit_ranges.mileage;
		var _age_range = _obj_unit_ranges.age;
		var _co2_emission_range = _obj_unit_ranges.co2_emission;

		var handleSliderPrice = function(){

			var sliderPrice = document.getElementById('sliderPrice');
			var sliderPriceDescr = document.getElementById('sliderPriceDescr');

			noUiSlider.create(sliderPrice, {
				start: [_price_range.min, _price_range.max]
				, connect: true
				, range: {
					'min': _price_range.min
					, 'max': _price_range.max
				}
				, step: 100
			});

			sliderPrice.noUiSlider.on('update', function( values, handle ) {
				var min = this.options.range.min;
				var max = this.options.range.max;
				var output = "";

				output = sliderRangeFormatter.format({
					'min': min
					, 'max': max
					, 'start': values[0]
					, 'end': values[1]
					, 'formatOptions': { decimals: 0, thousand: ',', prefix: ' '+_price_unit+' '}
				});

				sliderPriceDescr.innerHTML = output;
			});

		};

		var handleSliderMileage = function(){

			var sliderMileage = document.getElementById('sliderMileage');
			var sliderMileageDescr = document.getElementById('sliderMileageDescr');

			noUiSlider.create(sliderMileage, {
				start: [_mileage_range.min, _mileage_range.max]
				, connect: true
				, range: {
					'min': _mileage_range.min
					, 'max': _mileage_range.max
				}
				, step: 100
			});

			sliderMileage.noUiSlider.on('update', function( values, handle ) {
				var min = this.options.range.min;
				var max = this.options.range.max;
				var output = "";

				output = sliderRangeFormatter.format({
					'min': min
					, 'max': max
					, 'start': values[0]
					, 'end': values[1]
					, 'formatOptions': { decimals: 0, thousand: ',', postfix: ' '+_distance_unit}
				});

				sliderMileageDescr.innerHTML = output;
			});

		};

		var handleSliderAge = function(){

			var sliderAge = document.getElementById('sliderAge');
			var sliderAgeDescr = document.getElementById('sliderAgeDescr');

			noUiSlider.create(sliderAge, {
				start: [_age_range.min, _age_range.max]
				, connect: true
				, range: {
					'min': _age_range.min
					, 'max': _age_range.max
				}
				, step: 1
			});

			sliderAge.noUiSlider.on('update', function( values, handle ) {
				var min = this.options.range.min;
				var max = this.options.range.max;
				var output = "";

				output = sliderRangeFormatter.format({
					'min': min
					, 'max': max
					, 'start': values[0]
					, 'end': values[1]
					, 'formatOptions': { decimals: 0, prefix: 'year ' }
				});

				sliderAgeDescr.innerHTML = output;
			});

		};

		var handleSliderCo2Emission = function(){

			var sliderCo2Emission = document.getElementById('sliderCo2Emission');
			var sliderCo2EmissionDescr = document.getElementById('sliderCo2EmissionDescr');

			noUiSlider.create(sliderCo2Emission, {
				start: [_co2_emission_range.min, _co2_emission_range.max]
				, connect: true
				, range: {
					'min': _co2_emission_range.min
					, 'max': _co2_emission_range.max
				}
				, step: 10
			});

			sliderCo2Emission.noUiSlider.on('update', function( values, handle ) {
				var min = this.options.range.min;
				var max = this.options.range.max;
				var output = "";

				output = sliderRangeFormatter.format({
					'min': min
					, 'max': max
					, 'start': values[0]
					, 'end': values[1]
					, 'formatOptions': { decimals: 0, thousand: ',', postfix: ' '+_co2_emission_unit}
				});

				sliderCo2EmissionDescr.innerHTML = output;
			});

		};

		handleSliderPrice();
		handleSliderMileage();
		handleSliderAge();
		handleSliderCo2Emission();

	};

	var handleEventExecution = function( options ){
		
		if(options === undefined){
			options = { dataCallback: '' };
		}

		var _obj_vehicle_models = {};
		// counts of vehicle models and vehicle attributes
		var _result_count = 0;

		// event bindings and element configuring
		var handleFilterCheckboxVehicleModels = function(){
			jQuery("input[name='fieldVehicleModels[]']").each(function(){				
				jQuery(this).change(function(){
					handleAjaxVehicles( filter_type = 'models', options );
				});
			});
		};
		var handleFilterSliders = function(){
			var sliderPrice = document.getElementById('sliderPrice');
			var sliderMileage = document.getElementById('sliderMileage');
			var sliderAge = document.getElementById('sliderAge');
			var sliderCo2Emission = document.getElementById('sliderCo2Emission');

			var fieldSliderPrices = [];
			var fieldSliderMileages = [];
			var fieldSliderAges = [];
			var fieldSliderCo2Emissions = [];

			// set sliders if value exists
			jQuery("input[name='fieldSliderPrices[]']").each(function(key){
				if( jQuery(this).val() != '' ){
					fieldSliderPrices.push( jQuery(this).val() );
				}
				if( key == fieldSliderPrices.length-1 )
					sliderPrice.noUiSlider.set(fieldSliderPrices);
			});
			// set input values
			sliderPrice.noUiSlider.on('update', function( values, handle ) {
				jQuery("input[name='fieldSliderPrices[]']").each(function(key){
					var sliderValue = values[key];
					jQuery(this).val(sliderValue);
				});
			});

			jQuery("input[name='fieldSliderMileages[]']").each(function(key){
				if( jQuery(this).val() != '' ){
					fieldSliderMileages.push( jQuery(this).val() );
				}
				if( key == fieldSliderMileages.length-1 )
					sliderMileage.noUiSlider.set(fieldSliderMileages);
			});
			sliderMileage.noUiSlider.on('update', function( values, handle ) {
				jQuery("input[name='fieldSliderMileages[]']").each(function(key){
					var sliderValue = values[key];
					jQuery(this).val(sliderValue);
				});
			});

			jQuery("input[name='fieldSliderAges[]']").each(function(key){
				if( jQuery(this).val() != '' ){
					fieldSliderAges.push( jQuery(this).val() );
				}
				if( key == fieldSliderAges.length-1 )
					sliderAge.noUiSlider.set(fieldSliderAges);
			});
			sliderAge.noUiSlider.on('update', function( values, handle ) {
				jQuery("input[name='fieldSliderAges[]']").each(function(key){
					var sliderValue = values[key];
					jQuery(this).val(sliderValue);
				});
			});

			jQuery("input[name='fieldSliderCo2Emissions[]']").each(function(key){
				if( jQuery(this).val() != '' ){
					fieldSliderCo2Emissions.push( jQuery(this).val() );
				}
				if( key == fieldSliderCo2Emissions.length-1 )
					sliderCo2Emission.noUiSlider.set(fieldSliderCo2Emissions);
			});
			sliderCo2Emission.noUiSlider.on('update', function( values, handle ) {
				jQuery("input[name='fieldSliderCo2Emissions[]']").each(function(key){
					var sliderValue = values[key];
					jQuery(this).val(sliderValue);
				});
			});
			
			// trigger ajax
			sliderPrice.noUiSlider.on('change', function( values, handle ) {
				handleAjaxVehicles( filter_type = 'properties', options );
			});

			sliderMileage.noUiSlider.on('change', function( values, handle ) {
				handleAjaxVehicles( filter_type = 'properties', options );
			});

			sliderAge.noUiSlider.on('change', function( values, handle ) {
				handleAjaxVehicles( filter_type = 'properties', options );
			});

			sliderCo2Emission.noUiSlider.on('change', function( values, handle ) {
				handleAjaxVehicles( filter_type = 'properties', options );
			});
		};
		var handleFilterSelections = function(){
			jQuery("#fieldFuelType").change(function(){
				handleAjaxVehicles( filter_type = 'properties', options );
			});

			jQuery("#fieldTransmissionType").change(function(){
				handleAjaxVehicles( filter_type = 'properties', options );
			});

			jQuery("#fieldExteriorColour").change(function(){
				handleAjaxVehicles( filter_type = 'properties', options );
			});
		};
		var handleFilterVehicleFeatures = function(){

			jQuery("input[name='fieldVehicleFeatures[]']").each(function(){				
				jQuery(this).change(function(){
					handleAjaxVehicles( filter_type = 'properties', options );
				});
			});

		};
		var handleFilterToggle = function(){

			jQuery("#btnAdvancedSearch").click(function(){
			    jQuery(".custom-filter-advanced-field").slideToggle();
			    jQuery(this).toggle();
			    jQuery("#btnQuickSearch").toggle();
			});
			
			jQuery("#btnQuickSearch").click(function(){
			    jQuery(".custom-filter-advanced-field").slideToggle();
			    jQuery(this).toggle();
			    jQuery("#btnAdvancedSearch").toggle();
			});

		};
		var handleFilterReset = function(){
			jQuery("#btnResetSearch").click(function(){				

				jQuery("input[name='fieldVehicleModels[]']").removeProp("checked");
				jQuery("#fieldFuelType").val("-1");
				jQuery("#fieldTransmissionType").val("-1");
				jQuery("#fieldExteriorColour").val("-1");				

				var sliderPrice = document.getElementById('sliderPrice');
				var sliderMileage = document.getElementById('sliderMileage');
				var sliderAge = document.getElementById('sliderAge');
				var sliderCo2Emission = document.getElementById('sliderCo2Emission');
				sliderPrice.noUiSlider.reset();
				sliderMileage.noUiSlider.reset();
				sliderAge.noUiSlider.reset();
				sliderCo2Emission.noUiSlider.reset();

				jQuery("input[name='fieldVehicleFeatures[]']").removeProp("checked");

				// call ajax to refresh
				handleAjaxVehicles( filter_type = 'properties', options );
			});
		};
		var handleFilterSave = function(){
			jQuery("#btnSaveSearch").click(function(){
				var query = jQuery("#"+_root.form_id).serialize();

				globalMySearches.add({ 
					post_query: query 
					, success: function(response){

						var data = response.data;
						var notify_type = 'danger';
						var message = data.detail;
						if(data.status == 1){
							notify_type = 'success';
						}

						jQuery.notify({ message: message }, { type: notify_type });

					}
				});
			});
		};
		
		// local init
		handleFilterCheckboxVehicleModels();
		handleFilterSliders();
		handleFilterSelections();
		handleFilterVehicleFeatures();
		handleFilterToggle();	
		handleFilterReset();	
		handleFilterSave();	

		// retrieve vehicle models count
		jQuery("input[name='fieldVehicleModels[]']").each(function(index){
			var term_id = jQuery(this).val();
			var vehicle_models_count = jQuery(this).attr("data-vehicle-models-count");
			var vehicle_models_slug = jQuery(this).attr("data-vehicle-models-slug");

			_obj_vehicle_models[term_id] = { 'term_id': term_id
														, 'slug': vehicle_models_slug 
														, 'count': vehicle_models_count 
														};

			// set result count for vehicle models
			_result_count += parseInt(vehicle_models_count);
		});

		// set UI result count
		jQuery("#resultCount").text(_result_count);	

	};

	// handling ajax
	var handleAjaxVehicles = function( filter_type, options ){	

		// btn search start loading
		_ladda_btn_search.start();		

		if(filter_type === undefined){
			filter_type = 'models';
		}
		if(options === undefined){
			options = { dataCallback: '' };
		}

		var serializedData = jQuery("#"+_root.form_id).serializeArray();
		serializedData.push({ name: 'action', value: _local_obj_global_search_filter.av_action });

		jQuery.ajax({
		    type: "GET"
		    , url: _local_obj_global_search_filter.ajax_url
		    , data: serializedData
		    , dataType: "json"
		    , success: function(data) {

		    	var is_models_and_properties = false;

		    	var vehicles = data.data;
		    	var meta = data.meta;

    	    	for(var i=0; i < serializedData.length; i++){
    	    		if( serializedData[i].name == 'fieldVehicleModels[]' && filter_type == 'properties' ){
    					is_models_and_properties = true;
    					break;
    	    		}
    	    	}

		    	if( filter_type == 'models' ){
		    		
		    		var obj_fuel_types = {};
		    		var obj_transmission_types = {};
		    		var obj_exterior_colours = {};
		    		var obj_vehicle_features = {};
		    		jQuery.each(vehicles, function(index, vehicle){
		    			obj_fuel_types[vehicle.fuel_type.term_id] = vehicle.fuel_type;
		    			obj_transmission_types[vehicle.transmission_type.term_id] = vehicle.transmission_type;
		    			obj_exterior_colours[vehicle.ext_colour.term_id] = vehicle.ext_colour;

		    			jQuery.each(vehicle.features, function(index, feature){
		    				obj_vehicle_features[feature.term_id] = feature;
		    			});
		    		});


		    		// Update UI of vehicle properties and features
		    		// update field fuel type
		    		jQuery("#fieldFuelType option[value!='-1']").hide();
		    		jQuery.each(obj_fuel_types, function(index, obj_fuel_type){
		    			var term_id = obj_fuel_type.term_id;
		    			jQuery("#fieldFuelType option[value='"+term_id+"']").show();
		    		});
		    		// update field transmission type
		    		jQuery("#fieldTransmissionType option[value!='-1']").hide();
		    		jQuery.each(obj_transmission_types, function(index, obj_transmission_type){
		    			var term_id = obj_transmission_type.term_id;
		    			jQuery("#fieldTransmissionType option[value='"+term_id+"']").show();
		    		});
		    		// update field exterior colour
		    		jQuery("#fieldExteriorColour option[value!='-1']").hide();
		    		jQuery.each(obj_exterior_colours, function(index, obj_exterior_colour){
		    			var term_id = obj_exterior_colour.term_id;
		    			jQuery("#fieldExteriorColour option[value='"+term_id+"']").show();
		    		});
		    		// update field vehicle features
		    		jQuery("div[id^='vehicleFeature']").addClass("disabled");
		    		jQuery("input[name='fieldVehicleFeatures[]']").prop("disabled", true);
		    		jQuery.each(obj_vehicle_features, function(index, obj_vehicle_feature){
		    			var term_id = obj_vehicle_feature.term_id;
		    			jQuery("#vehicleFeature"+term_id).removeClass("disabled");
		    			jQuery("#fieldVehicleFeature"+term_id).removeProp("disabled");
		    		});
		    	}			    	
		    	
		    	if( is_models_and_properties ){
	    			// remove fieldVehicleModels from searching
	    			var propertiesSerializedData = serializedData.filter(function(el) {
					    return el.name !== "fieldVehicleModels[]";
					});
					propertiesSerializedData.push({ name: 'action', value: _local_obj_global_search_filter.av_action });

	    			jQuery.ajax({
	    				type: "GET"
	    				, url: _local_obj_global_search_filter.ajax_url
	    				, data: propertiesSerializedData
	    				, dataType: "json"
	    				, success: function(data) {
	    					var vehicles = data.data;
	    					var meta = data.meta;

					    	// init vehicle models count to 0
					    	var obj_vehicle_models = {};
					    	jQuery("input[name='fieldVehicleModels[]']").each(function(index){
					    		var term_id = jQuery(this).val();
					    		var count = 0;
					    		obj_vehicle_models[term_id] = { count: 0 };
					    	});

					    	// get involved vehicle models and increment count
					    	jQuery.each(vehicles, function(index, vehicle){
					    		var vehicle_model = vehicle.vehicle_model;
					    		obj_vehicle_models[vehicle_model.term_id].count += 1;
					    	});

					    	// update UI of vehicle models 
					    	jQuery("input[name='fieldVehicleModels[]']").each(function(index){
					    		var term_id = jQuery(this).val();
					    		var count = obj_vehicle_models[term_id].count;

					    		jQuery("#vehicleModel"+term_id+"Count").text(count);

					    		if(count == 0){
					    			jQuery(this).prop("disabled", true);
						    		jQuery("#vehicleModel"+term_id).addClass("disabled");
					    		}
					    		else{
					    			jQuery(this).removeProp("disabled");
						    		jQuery("#vehicleModel"+term_id).removeClass("disabled");
					    		}
					    	});
	    				}
	    			});
		    	}
		    	else if( filter_type == 'properties' ){

    		    	// init vehicle models count to 0
    		    	var obj_vehicle_models = {};
    		    	jQuery("input[name='fieldVehicleModels[]']").each(function(index){
    		    		var term_id = jQuery(this).val();
    		    		var count = 0;
    		    		obj_vehicle_models[term_id] = { count: 0 };
    		    	});

    		    	// get involved vehicle models and increment count
    		    	jQuery.each(vehicles, function(index, vehicle){
    		    		var vehicle_model = vehicle.vehicle_model;
    		    		obj_vehicle_models[vehicle_model.term_id].count += 1;
    		    	});

    		    	// update UI of vehicle models 
    		    	jQuery("input[name='fieldVehicleModels[]']").each(function(index){
    		    		var term_id = jQuery(this).val();
    		    		var count = obj_vehicle_models[term_id].count;

    		    		jQuery("#vehicleModel"+term_id+"Count").text(count);

    		    		if(count == 0){
    		    			jQuery(this).prop("disabled", true);
    			    		jQuery("#vehicleModel"+term_id).addClass("disabled");
    		    		}
    		    		else{
    		    			jQuery(this).removeProp("disabled");
    			    		jQuery("#vehicleModel"+term_id).removeClass("disabled");
    		    		}
    		    	});
		    	}		    	

		    	//retrieve final data using callback func
		    	if( !jQuery.isEmptyObject(options) ){
		    		if(options.hasOwnProperty('dataCallback') && typeof options.dataCallback == 'function' ){
		    			options.dataCallback( data );
		    		}
		    	}

		    	//update UI result count
		    	jQuery("#resultCount").text(meta.count);
		    	// btn search stop loading
		    	_ladda_btn_search.stop();

		    }
		    , error: function(jqXHR, textStatus, errorThrown) {
		        console.log('Error api-vehicles.');
		    }
		});
	};

	return {
		init: function( options ){

			if(options === undefined){
				options = { dataCallback: '' };
			}

			handleSettings();
			handleEventExecution( options );

		}
		, ajax: function( options ){

			if(options === undefined){
				options = { dataCallback: '' };
			}

			handleAjaxVehicles( filter_type = 'properties', options );
		}
	};
}();