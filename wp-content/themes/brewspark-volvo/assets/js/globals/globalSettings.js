var globalSettings = function(){

	var _root = this;

	var _options = {};
	var currentDate = new Date();
	var currentYear = currentDate.getFullYear();

	_options.obj_unit_ranges = {
		price: { min: 0, max: 700000 }
		, mileage: { min: 0, max: 100000 }
		, age : { min: 2010, max: currentYear }
		, co2_emission : { min: 0, max: 500 }
	};

	var handleBasic = function(){
		_options.obj_units = _root.options.obj_units;		
	};

	return {
		init: function( options ){

			if(options === undefined){
				options = { obj_units: {} };
			}

			_root.options = options;	

			handleBasic();		
		}
		, getUnits: function(){
			return _options.obj_units;
		}
		, getUnitRanges: function(){
			return _options.obj_unit_ranges;
		}
	};
}();