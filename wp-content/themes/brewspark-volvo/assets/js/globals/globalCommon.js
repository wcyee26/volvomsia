// Global functions

String.prototype.ucfirst = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

String.prototype.serializeArray = function() {
    
    var query = this;
    var decoded_query = decodeURIComponent(query);

    var query_array = decoded_query.split("&");
    var new_serialize_array = [];

    for(var i=0; i<query_array.length; i++){
    	var key_val_pair = query_array[i].split("=");
    	var param_obj = { name: key_val_pair[0], value: key_val_pair[1] };
    	new_serialize_array.push(param_obj);
    }

    return new_serialize_array;
}

Date.prototype.getMonthName = function() {
    var months = ["January", "Feburary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    return months[this.getMonth()];
};
