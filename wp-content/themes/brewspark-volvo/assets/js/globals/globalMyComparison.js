//This is used for setting and getting My Comparison local storage
var globalMyComparison = function () {

	var _root = this;
	_root.options = {};
	var _storage_name = 'my_comparisons';
	var _storage_max_length = 3;

	_root.page_url_my_comparison = '';
	_root.page_url_my_comparison_with_params = '';

	_root.response = { 
		data: { 
			status: 0, title: 'My Comparison', detail: '' } 
		, links: { 
			self: _root.page_url_my_comparison
			, next: '' 
		}
	};

	var handleAdd = function(){

		if (typeof(Storage) !== "undefined") {

			var storage_name = _storage_name;
			var storages = [];
			var post_id = _root.options.post_id;			

			// first checking of storage
			if(localStorage.getItem(storage_name) == null || localStorage.getItem(storage_name) == ''){
				storages.push(post_id);
				var val = JSON.stringify(storages);
				localStorage.setItem(storage_name, val);

				_root.response.data.status = 1;				
				_root.response.data.detail = 'Successfully added vehicle to My Comparison.';
			}
			// if storage already exist
			else{		    	
				var my_comparisons = localStorage.getItem(storage_name);		    					
				my_comparisons = JSON.parse(my_comparisons);

				if(my_comparisons.length < _storage_max_length){
					// check if post id has already exist 
					var is_allowed_storage = true;
					jQuery.each(my_comparisons, function(index, my_comparison){
						if(my_comparison == post_id){
							is_allowed_storage = false;
							_root.response.data.status = 0;				
							_root.response.data.detail = 'This vehicle has already existed in My Comparison.';
							return false;
						}
					});

					// if post id not exist before
					if(is_allowed_storage){
						my_comparisons.push(post_id);
    					var val = JSON.stringify(my_comparisons);
	    				localStorage.setItem(storage_name, val);

	    				_root.response.data.status = 1;				
	    				_root.response.data.detail = 'Successfully added vehicle to My Comparison.';
					}
				}
				else{
					_root.response.data.status = 0;				
					_root.response.data.detail = 'You can only compare ' + _storage_max_length + ' vehicles at once.';
				}
			}		

			handleUpdateUI();

			if(typeof _root.options.success == 'function' ){
				_root.options.success(_root.response);
			}
		}
	};

	var handleRemove = function(){

		if (typeof(Storage) !== "undefined") {

			var storage_name = _storage_name;
			var my_comparisons = localStorage.getItem(storage_name);
			my_comparisons = JSON.parse(my_comparisons);

			var post_id = _root.options.post_id;

			if(my_comparisons.length > 0){
				var index = my_comparisons.indexOf(post_id);
				if(index > -1){
					my_comparisons.splice(index, 1);
					var val = JSON.stringify(my_comparisons);
    				localStorage.setItem(storage_name, val);    				

					_root.response.data.status = 1;
					_root.response.data.detail = 'Successfully removed vehicle from My Comparison.';
				}
				else{
					_root.response.data.status = 0;
					_root.response.data.detail = 'There is nothing to be removed.';
				}
			}

			handleUpdateUI();

			if(typeof _root.options.success == 'function' ){
				_root.options.success(_root.response);
			}

		}

	};

	var handleUpdateUI = function(){	

		var my_comparisons = localStorage.getItem(_storage_name); 					
		my_comparisons = JSON.parse(my_comparisons);

		if( my_comparisons != null ){
			if( my_comparisons.length > 0 ){
				_root.page_url_my_comparison_with_params = _root.page_url_my_comparison + "?";
				
				// remove active class (such as btn compare)
				jQuery(".my-comparison").removeClass("active");

				jQuery.each(my_comparisons, function(index, my_comparison){
					var post_id = my_comparison;

					if(index > 0)
						_root.page_url_my_comparison_with_params += '&';

					_root.page_url_my_comparison_with_params += 'fieldPostIds[]=' + post_id;

					// set active class to related element (such as btn compare)
					jQuery(".my-comparison[id*='"+post_id+"']").addClass("active");
				});
				
				// Web
				jQuery("#myComparison").prop("href", _root.page_url_my_comparison_with_params);
				jQuery("#myComparison").parent("li").addClass("active");
				jQuery("#myComparison").removeClass("disabled");
				jQuery("#myComparisonCount").text(my_comparisons.length);
				
				// Mobile
				jQuery("#mobileMyComparison").prop("href", _root.page_url_my_comparison_with_params);
				jQuery("#mobileMyComparison").removeClass("disabled");
				jQuery("#mobileMyComparisonCount").text(my_comparisons.length);

			}
			else{
				// Web
				jQuery("#myComparison").prop("href", "javascript:void(0);");
				jQuery("#myComparison").parent("li").removeClass("active");
				jQuery("#myComparison").addClass("disabled");
				jQuery("#myComparisonCount").text(0);

				// Mobile
				jQuery("#mobileMyComparison").prop("href", "javascript:void(0);");
				jQuery("#mobileMyComparison").addClass("disabled");
				jQuery("#mobileMyComparisonCount").text(0);

				// remove active class (such as btn compare)
				jQuery(".my-comparison").removeClass("active");
			}
		}
	};

	return {
		init: function( options ){

			if(options === undefined){
				options = { 
					page_url_my_comparison: ''
				};
			}

			_root.page_url_my_comparison = options.page_url_my_comparison;

			handleUpdateUI();
		}
		, refresh: function(){
			handleUpdateUI();
		}
		, add: function( options ){

			if(options === undefined){
				options = { post_id: '', success: '' };
			}

			_root.options = options;

			handleAdd();
		}
		, remove: function( options ){

			if(options === undefined){
				options = { post_id: '', success: '' };
			}

			_root.options = options;

			handleRemove();	
		}
		, length: function(){

			if(localStorage.getItem(storage_name) == null){
				return 0;
			}
			else{
				var my_comparisons = localStorage.getItem(storage_name);
				my_comparisons = JSON.parse(my_comparisons);

				return my_comparisons.length;
			}			
		}
		, get: function(){

			var my_comparisons = localStorage.getItem(storage_name);
			my_comparisons = JSON.parse(my_comparisons);

			return my_comparisons;
		}
	};

}();