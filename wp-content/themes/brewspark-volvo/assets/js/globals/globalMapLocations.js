var globalMapLocations = function(){

	var _root = this;

	var handleBasic = function(){

		var _local_obj_page_find_dealer = _root.options._local_obj_page_find_dealer;

		if( !jQuery.isEmptyObject(_root.options.locations) ){

			var locations = _root.options.locations;			
			var template_directory_uri = '';

			if(!jQuery.isEmptyObject(_local_obj_page_find_dealer)){
				if( _local_obj_page_find_dealer.hasOwnProperty('template_directory_uri') )
					template_directory_uri = _local_obj_page_find_dealer.template_directory_uri;
			}

			if( locations.length <= 1 ){

				var location = locations[0];
				var dealer_location_id = location.ID;

				var coords = { lat: parseFloat(location.lat), lng: parseFloat(location.lng) };

		        var map = new google.maps.Map(document.getElementById('map'), {
					zoom: 18
					, center: coords
		        });

		        var marker = new google.maps.Marker({
					position: coords
					, map: map
					, title: location.title
		        });

		        var info_window_content = jQuery("#dealerLocation"+dealer_location_id+"InfoWindowContent").html();

		        var info_window = new google.maps.InfoWindow({
					content: info_window_content
					, maxWidth: 300
				});

				info_window.open(map, marker);

				marker.addListener("click", function(){
					info_window.open(map, marker);
				});

			}
			else{

				var map = new google.maps.Map(document.getElementById('map'), {
							zoom: 10,
							center: {lat: -28.024, lng: 140.887}
						});
				var markers = [];
				var info_window = null;

				jQuery.each(locations, function(index, location){

					var dealer_location_id = location.ID;

					var image = {
						url: template_directory_uri+'/assets/images/icon-pin.png'
						, size: new google.maps.Size(31, 41)
						, origin: new google.maps.Point(0, 0)
						, anchor: new google.maps.Point(0, 0)
					};
					var coords = { lat: parseFloat(location.lat), lng: parseFloat(location.lng) };
					var marker = new google.maps.Marker({
									position: coords
									, label: { text: String(index+1), color: "white" }
									, icon: image
								});

			        var info_window_content = jQuery("#dealerLocation"+dealer_location_id+"InfoWindowContent").html();

			        // set marker click event
					marker.addListener("click", function(){
						if (info_window) {
					        info_window.close();
					    }

						info_window = new google.maps.InfoWindow({
												content: info_window_content
												, maxWidth: 300
											});

						info_window.open(map, marker);
					});

					// bind dealer location to marker click event
					jQuery("#dealerLocation"+dealer_location_id).click(function(){
						map.setZoom(18);
						map.panTo( marker.getPosition() );
						google.maps.event.trigger(marker, 'click');
					});

					// push marker into array of markers
					markers.push(marker);
				});

				var markerCluster = new MarkerClusterer(map, markers,
										{
											imagePath: template_directory_uri+'/assets/images/icon-state-markers/m'
											, averageCenter: true
										}
									);
				var styles = markerCluster.getStyles();
				jQuery.each(styles, function(index, style){
					styles[index].width = 85;
					styles[index].height = 85;
					styles[index].textColor = "#fff";
					styles[index].textSize = 16;
					styles[index].anchorText = [15, 0];
				});
				markerCluster.setStyles(styles);
				markerCluster.fitMapToMarkers();

			}

			
		}
		// if no location is given
		else{
	        var map = new google.maps.Map(document.getElementById('map'), {
				zoom: 10
				// random kuala lumpur location
				, center: { lat: 3.171571, lng: 101.690941 }
	        });
		}

	};

	return {
		init: function( options ){

			if(options === undefined){
				options = { 
					locations: ''
					, _local_obj_page_find_dealer: { template_directory_uri: '' } 
				};
			}

			_root.options = options;

			handleBasic();
		}
	}

}();