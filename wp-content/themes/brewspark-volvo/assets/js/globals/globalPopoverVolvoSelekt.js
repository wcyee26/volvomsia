var globalPopoverVolvoSelekt = function(){

	var _options = null;

	var handleBasic = function(){

		var content = _options.content;

		// init popover for volvo selekt
		jQuery(".volvo-selekt").popover({
			trigger: 'hover'
			, container: 'body'
			, placement: 'auto bottom'
			, html : true
			, title: 'VOLVO SELEKT'
			, content: content
		});
	};

	return {
		init: function( options ){

			if(options === undefined){
				options = { content: null }
			}

			_options = options;

			handleBasic();
		}
		, refresh: function(){
			handleBasic();
		}
	};

}();