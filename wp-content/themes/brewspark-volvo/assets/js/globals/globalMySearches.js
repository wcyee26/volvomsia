//This is used for setting and getting My Searches local storage
var globalMySearches = function () {

	var _root = this;
	_root.options = {};
	var _storage_name = 'my_searches';
	var _storage_max_length = 25;

	_root.page_url_my_searches = '';

	_root.response = { 
		data: { status: 0, title: 'My Searches', detail: '' }
	};

	var handleAdd = function(){

		if (typeof(Storage) !== "undefined") {

			var storage_name = _storage_name;
			var storages = [];
			var post_query = _root.options.post_query;			

			// first checking of storage
			if(localStorage.getItem(storage_name) == null || localStorage.getItem(storage_name) == ''){
				storages.push(post_query);
				var val = JSON.stringify(storages);
				localStorage.setItem(storage_name, val);

				_root.response.data.status = 1;				
				_root.response.data.detail = 'Successfully saved search condition to My Searches.';
			}
			// if storage already exist
			else{		    	
				var storages = localStorage.getItem(storage_name);		    					
				storages = JSON.parse(storages);

				if(storages.length >= _storage_max_length){
					_root.response.data.status = 0;				
					_root.response.data.detail = 'You can only save ' + _storage_max_length + ' searches at maximum.';
				}
				else{
					storages.push(post_query);
					var val = JSON.stringify(storages);
					localStorage.setItem(storage_name, val);

					_root.response.data.status = 1;				
					_root.response.data.detail = 'Successfully saved search condition to My Searches.';
				}
			}		

			handleUpdateUI();

			if(typeof _root.options.success == 'function' ){
				_root.options.success(_root.response);
			}
		}
	};

	var handleRemove = function(){

		if (typeof(Storage) !== "undefined") {

			var storage_name = _storage_name;
			var storages = localStorage.getItem(storage_name);
			storages = JSON.parse(storages);

			if(storages.length > 0){
				var post_query_index = _root.options.post_query_index;

				storages.splice(post_query_index, 1);

				var val = JSON.stringify(storages);
				localStorage.setItem(storage_name, val);

				_root.response.data.status = 1;				
				_root.response.data.detail = 'Successfully removed the saved search condition from My Searches.';
			}

			handleUpdateUI();

			if(typeof _root.options.success == 'function' ){
				_root.options.success(_root.response);
			}

		}

	};

	var handleUpdateUI = function(){	

		var storages = localStorage.getItem(_storage_name);		    					
		storages = JSON.parse(storages);

		if( storages != null ){
			if( storages.length > 0 ){
				jQuery("#mySearches").prop("href", _root.page_url_my_searches);
				jQuery("#mySearches").parent("li").addClass("active");
				jQuery("#mySearches").removeClass("disabled");
				jQuery("#mySearchesCount").text(storages.length);

				jQuery("#mobileMySearches").prop("href", _root.page_url_my_searches);
				jQuery("#mobileMySearches").removeClass("disabled");
				jQuery("#mobileMySearchesCount").text(storages.length);
			}
			else{
				jQuery("#mySearches").prop("href", "javascript:void(0);");
				jQuery("#mySearches").parent("li").removeClass("active");
				jQuery("#mySearches").addClass("disabled");
				jQuery("#mySearchesCount").text(0);

				jQuery("#mobileMySearches").prop("href", "javascript:void(0);");
				jQuery("#mobileMySearches").addClass("disabled");
				jQuery("#mobileMySearchesCount").text(0);
			}
		}

	};

	return {
		init: function( options ){

			if(options === undefined){
				options = { page_url_my_searches: '' };
			}

			_root.page_url_my_searches = options.page_url_my_searches;

			handleUpdateUI();
		}
		, refresh: function(){
			handleUpdateUI();
		}
		, add: function( options ){

			if(options === undefined){
				options = { post_query: '', success: '' };
			}

			_root.options = options;

			handleAdd();
		}
		, remove: function( options ){

			if(options === undefined){
				options = { post_query_index: '', success: '' };
			}

			_root.options = options;

			handleRemove();	
		}
		, length: function(){

			if(localStorage.getItem(storage_name) == null){
				return 0;
			}
			else{
				var storages = localStorage.getItem(storage_name);
				storages = JSON.parse(storages);

				return storages.length;
			}			
		}
		, get: function(){

			var storages = localStorage.getItem(storage_name);
			storages = JSON.parse(storages);

			return storages;
		}
	};

}();