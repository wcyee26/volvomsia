var pageFindDealer = function () {

	var _root = this;

	var handleBasic = function(){

		globalMapLocations.init({ 
			locations: _root.options.locations
			// variable '_local_obj_page_find_dealer' is from functions.php
			, _local_obj_page_find_dealer: _local_obj_page_find_dealer
		});		
	};

	return {
		init: function( options ){

			if(options === undefined){
				options = { locations: '' };
			}

			_root.options = options;

			handleBasic();
		}
	};

}();