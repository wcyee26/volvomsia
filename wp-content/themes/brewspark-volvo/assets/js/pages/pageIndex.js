var pageIndex = function () {

	var handleBasic = function(){

		globalSearchFilter.init();
		
	};

	var handleGallerySlideshow = function(){

		var gallerySlideshow = new BrewsparkGallerySlideshow('featuredVehicleGallerySlideshow');
		var pageXs = 1;
		var pageLengthXs = 1;
		var pageSm = 1;
		var pageLengthSm = 3;

		var handleXsDisplay = function(){
			gallerySlideshow.display(pageXs, pageLengthXs, function(slideElement){
				jQuery(slideElement).addClass("visible-xs");
			});

			if(pageXs == 1){
				jQuery("#btnPrevFeaturedVehiclesExtraSmall").css("cssText", "display: none !important;");
				jQuery("#btnNextFeaturedVehiclesExtraSmall").css("cssText", "");
			}
			else if(pageXs == gallerySlideshow.length()){
				jQuery("#btnNextFeaturedVehiclesExtraSmall").css("cssText", "display: none !important;");
				jQuery("#btnPrevFeaturedVehiclesExtraSmall").css("cssText", "");
			}
			else{
				jQuery("#btnPrevFeaturedVehiclesExtraSmall").css("cssText", "");
				jQuery("#btnNextFeaturedVehiclesExtraSmall").css("cssText", "");
			}

			if(gallerySlideshow.length () <= pageLengthXs){
				jQuery("#btnPrevFeaturedVehiclesExtraSmall").css("cssText", "display: none !important;");
				jQuery("#btnNextFeaturedVehiclesExtraSmall").css("cssText", "display: none !important;");
			}
		};

		var handleSmDisplay = function(){
			gallerySlideshow.display(pageSm, pageLengthSm, function(slideElement){
				jQuery(slideElement).addClass("visible-sm visible-md visible-lg");
			});

			if(pageSm == 1){
				jQuery("#btnPrevFeaturedVehiclesSmall").css("cssText", "display: none !important;");
				jQuery("#btnNextFeaturedVehiclesSmall").css("cssText", "");
			}
			else if(pageSm > gallerySlideshow.length() - pageLengthSm){
				jQuery("#btnNextFeaturedVehiclesSmall").css("cssText", "display: none !important;");
				jQuery("#btnPrevFeaturedVehiclesSmall").css("cssText", "");
			}
			else{
				jQuery("#btnPrevFeaturedVehiclesSmall").css("cssText", "");
				jQuery("#btnNextFeaturedVehiclesSmall").css("cssText", "");
			}

			if(gallerySlideshow.length () <= pageLengthSm){
				jQuery("#btnPrevFeaturedVehiclesSmall").css("cssText", "display: none !important;");
				jQuery("#btnNextFeaturedVehiclesSmall").css("cssText", "display: none !important;");
			}
		};				

		jQuery("#btnPrevFeaturedVehiclesExtraSmall").click(function(){
			gallerySlideshow.clear();
			pageXs = pageXs - pageLengthXs;
			handleXsDisplay();
			//when page xs switched to older page
			if(pageXs == pageSm-1){
				pageSm = pageSm - pageLengthSm;				
			}			
			handleSmDisplay();
		});
		jQuery("#btnNextFeaturedVehiclesExtraSmall").click(function(){
			gallerySlideshow.clear();
			pageXs = pageXs + pageLengthXs;
			handleXsDisplay();
			//when page xs switched to later page
			if(pageXs == pageLengthSm+1){
				pageSm = pageSm + pageLengthSm;				
			}				
			handleSmDisplay();
		});			

		jQuery("#btnPrevFeaturedVehiclesSmall").click(function(){
			gallerySlideshow.clear();
			pageSm = pageSm - pageLengthSm;
			handleSmDisplay();
			pageXs = pageSm;
			handleXsDisplay();
		});
		jQuery("#btnNextFeaturedVehiclesSmall").click(function(){
			gallerySlideshow.clear();
			pageSm = pageSm + pageLengthSm;
			handleSmDisplay();
			pageXs = pageSm;
			handleXsDisplay();
		});

		// init
		handleXsDisplay();
		handleSmDisplay();
	};

	return {
		init: function(){

			handleBasic();
			handleGallerySlideshow();
		}
	};
}();