var pageMyComparison = function () {

	var _root = this;

	var handleBasic = function(){

		var post_ids = _root.options.post_ids;
		jQuery.each(post_ids, function(index, post_id){

			jQuery("#btnVehicle"+post_id+"FullDetails").click(function(){
				window.location = _local_obj_page_my_comparison.page_url_vehicle_details + "?post_id=" + post_id;
			});
			jQuery("#btnVehicle"+post_id+"RemoveVehicle").click(function(){
				globalMyComparison.remove({ 
					post_id: post_id
					, success: function(response){
						
						if(response.data.status == 1){							
							var redirect_url = jQuery("#myComparison").prop("href");

							if( redirect_url.indexOf('void') >= 0 ){
								redirect_url = _local_obj_page_my_comparison.home_url;	
							}
							
							window.location = redirect_url;
						}
					} 
				});
			});
		});

	};

	return {
		init: function( options ){

			if(options === undefined){
				options = { post_ids: [] };
			}

			_root.options = options;

			handleBasic();
		}
	};

}();