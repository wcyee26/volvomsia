var pageSearch = function () {

	var _root = this;
	_root.page_number = 1;
	_root.page_size = 25;

	_root.result_count = 0;

	_root.form_id = 'formGlobalSearchFilter';
	_root.prefix_vehicle_id = "vehicle";
	_root.prefix_vehicle_id_ucfirst = "Vehicle";
	_root.sample_vehicle_id = _root.prefix_vehicle_id + '0';
	_root.sample_vehicle_id_ucfirst = _root.prefix_vehicle_id_ucfirst + '0';

	_root.prefix_mobile = "mobile";
	_root.prefix_mobile_vehicle_id = _root.prefix_mobile + _root.prefix_vehicle_id_ucfirst;
	_root.sample_mobile_vehicle_id =_root.prefix_mobile_vehicle_id + '0';

	_root.page_url_vehicle_details = jQuery("#"+_root.form_id).attr("data-page-url-vehicle-details");

	var _ladda_btn_show_more = Ladda.create( document.querySelector( "#btnShowMore" ) );
	var _ladda_btn_show_all = Ladda.create( document.querySelector( "#btnShowAll" ) );

	var handleBasic = function(){

		globalSearchFilter.init();

		// change orderby and order
		jQuery("#fieldOrderby").change(function(){
			var element_selected = jQuery(this).find("option:selected");

			var order = jQuery(element_selected).attr("data-order");
			jQuery("#fieldOrder").val(order);

			// remove everything in placeholder except sample vehicle
			jQuery("#vehiclesPlaceholder").children().not("#vehicle0").remove();
			jQuery("#mobileVehiclesPlaceholder").children().not("#mobileVehicle0").remove();
			// reset to page 1
			_root.page_number = 1;
			handleAjax();
		});

		// auto scroll to search result
		jQuery('html, body').animate({
            scrollTop: jQuery("#searchResults").offset().top - 100
        }, 500);        

        // event bindings of btns       
		// btns show more and show all		
		jQuery("#btnShowMore").click(function(){			
			jQuery(this).prop("disabled", true);
			jQuery("#btnShowAll").prop("disabled", true);
			_ladda_btn_show_more.start();

			_root.page_number += 1;
			handleAjax();
		});
		jQuery("#btnShowAll").click(function(){
			jQuery(this).prop("disabled", true);
			jQuery("#btnShowMore").prop("disabled", true);
			_ladda_btn_show_all.start();

			_root.page_number += 1;
			handleAjax({ showRestAll: true });
		});
	};

	var handleAjax = function( options ){

		if(options === undefined){
			options = { showRestAll: false };
		}

		// show spinner loading
		jQuery("#spinnerResultContent").show();

		var serializedData = jQuery("#"+_root.form_id).serializeArray();
		serializedData.push({ name: 'pageNumber', value: _root.page_number });
		serializedData.push({ name: 'pageSize', value: _root.page_size });
		if(options.showRestAll == true){
			serializedData.push({ name: 'resultCount', value: _root.result_count });
		}
		serializedData.push({ name: 'action', value: _local_obj_page_search.av_action });

		jQuery.ajax({
		    type: "GET"
		    , url: _local_obj_page_search.ajax_url
		    , data: serializedData
		    , dataType: "json"
		    , success: function(data) {

		    	var vehicles = data.data;
		    	var meta = data.meta;

		    	_root.result_count = meta.count;
		    	// set result count UI
		    	jQuery("#resultCount").text(_root.result_count);
		    	jQuery("#searchResultCount").text(_root.result_count);

		    	// set btn load more count
		    	var result_rest_count = parseInt(_root.result_count) - parseInt(_root.page_size);
		    	if( result_rest_count < _root.page_size ){
		    		jQuery("#btnShowMorePageSize").text( result_rest_count );
		    	}
		    	else{
		    		jQuery("#btnShowMorePageSize").text( _root.page_size );
		    	}

		    	// whether to show/hide buttons depends on the page availability
		    	if(_root.page_number < meta.max_num_pages ){
		    		jQuery("#btnShowMore").show();
		    		jQuery("#btnShowAll").show();
		    	}
		    	else{
		    		jQuery("#btnShowMore").hide();
		    		jQuery("#btnShowAll").hide();
		    	}

		    	// Web rendering
		    	jQuery.each(vehicles, function(index, vehicle){
		    		var sample_vehicle = jQuery("#"+_root.sample_vehicle_id).clone();
		    		var vehicle_id = vehicle['ID'];
		    		var element_vehicle_id = _root.prefix_vehicle_id + String(vehicle_id);
		    		var element_vehicle_id_ucfirst = element_vehicle_id.ucfirst();

		    		var page_url_vehicle_details = _root.page_url_vehicle_details + '?post_id=' + vehicle_id;

		    		// replace main vehicle id
		    		jQuery(sample_vehicle).prop("id", element_vehicle_id);
		    		// replace all related vehicle id in main vehicle
		    		jQuery(sample_vehicle).find("[id^='"+_root.sample_vehicle_id+"']").each(function(){
		    			var sample_id = jQuery(this).prop("id");
		    			var new_element_vehicle_id = sample_id.replace(_root.sample_vehicle_id, element_vehicle_id);
		    			jQuery(this).prop("id", new_element_vehicle_id);
		    		});
		    		// replace ids such as btnVehicle0, etc.
		    		jQuery(sample_vehicle).find("[id*='"+_root.sample_vehicle_id_ucfirst+"']").each(function(){
		    			var sample_id = jQuery(this).prop("id");
		    			var new_element_vehicle_id = sample_id.replace(_root.sample_vehicle_id_ucfirst, element_vehicle_id_ucfirst);
		    			jQuery(this).prop("id", new_element_vehicle_id);
		    		});
		    		
		    		//append element in the vehicle list placeholder
		    		jQuery("#vehiclesPlaceholder").append(sample_vehicle);

		    		//replace element with retrieved data
		    		var vehicle_title = vehicle.title;
		    		// html decode vehicle price unit 
		    		var vehicle_price = jQuery("<div/>").html(vehicle.price_unit).text() + ' ' + vehicle.price;
		    		var vehicle_featured_image = vehicle.featured_image;
		    		var vehicle_mileage = vehicle.mileage + ' ' + vehicle.mileage_unit;
		    		var vehicle_engine_size = vehicle.engine_size + ' ' + vehicle.engine_size_unit;
		    		var vehicle_engine_power = vehicle.engine_power + ' ' + vehicle.engine_power_unit;
		    		var vehicle_fuel_type = vehicle.fuel_type.name;
		    		var vehicle_transmission_type = vehicle.transmission_type.name;
		    		var vehicle_co2_emission = vehicle.co2_emission + ' ' + vehicle.co2_emission_unit;
		    		var vehicle_reg_date = new Date(vehicle.reg_date);
		    		vehicle_reg_date = String(vehicle_reg_date.getFullYear()) + ' ' + vehicle_reg_date.getMonthName();
		    		var vehicle_features = vehicle.features;
		    		var vehicle_dealer_location = vehicle.dealer_location;
		    		var vehicle_dealer_location_address = vehicle_dealer_location.title;
		    		var vehicle_dealer_location_phone = vehicle_dealer_location.phone;

		    		jQuery("#"+element_vehicle_id+"Title").text(vehicle_title);
		    		jQuery("#"+element_vehicle_id+"Price").text(vehicle_price);
		    		jQuery("#"+element_vehicle_id+"FeaturedImagePlaceholder").prop("href", page_url_vehicle_details);
		    		jQuery("#"+element_vehicle_id+"FeaturedImage").prop("title", vehicle_featured_image.title);
		    		jQuery("#"+element_vehicle_id+"FeaturedImage").prop("src", vehicle_featured_image.srcs['medium']);
		    		jQuery("#"+element_vehicle_id+"Mileage").text(vehicle_mileage);
		    		jQuery("#"+element_vehicle_id+"EngineSize").text(vehicle_engine_size);
		    		jQuery("#"+element_vehicle_id+"EnginePower").text(vehicle_engine_power);
		    		jQuery("#"+element_vehicle_id+"FuelType").text(vehicle_fuel_type);
		    		jQuery("#"+element_vehicle_id+"TransmissionType").text(vehicle_transmission_type);
		    		jQuery("#"+element_vehicle_id+"Co2Emission").text(vehicle_co2_emission);
		    		jQuery("#"+element_vehicle_id+"RegDate").text(vehicle_reg_date);
		    		if(vehicle_features.length > 0){
		    			jQuery.each(vehicle_features, function(index, vehicle_feature){
		    				var vehicle_feature_term_id = vehicle_feature.term_id;
		    				var vehicle_feature_name = vehicle_feature.name;
		    				var vehicle_feature_slug = vehicle_feature.slug;

		    				if(vehicle_feature_slug != 'volvo-selekt'){
			    				var element_feature = jQuery("#"+element_vehicle_id+"Feature").clone();
			    				jQuery(element_feature).prop("id", element_vehicle_id+"Feature"+vehicle_feature_term_id);
			    				jQuery(element_feature).text(vehicle_feature_name);
			    				jQuery(element_feature).insertAfter("#"+element_vehicle_id+"Feature");
			    				jQuery(element_feature).show();
		    				}
		    				else{
		    					jQuery("#"+element_vehicle_id+"FeatureVolvoSelekt").show();
		    				}
		    			});
		    		}
		    		jQuery("#"+element_vehicle_id+"DealerLocationAddress").text(vehicle_dealer_location_address);
		    		jQuery("#"+element_vehicle_id+"DealerLocationPhone").text(vehicle_dealer_location_phone);
		    		jQuery("#btn"+element_vehicle_id_ucfirst+"FullDetails").prop("href", page_url_vehicle_details);		    		

		    		// binding events
		    		jQuery("#"+element_vehicle_id+"TitlePanel,#btn"+element_vehicle_id_ucfirst+"ReadMore").click(function(){
		    			var btn_read_more_id = 'btn'+element_vehicle_id_ucfirst+'ReadMore';
		    			// remove click events 
		    			jQuery("#"+element_vehicle_id+"TitlePanel,#"+btn_read_more_id).off("click");
		    			// set btn loading effect
		    			var ladda_btn_read_more = Ladda.create( document.querySelector( "#"+btn_read_more_id ) );
		    			// start btn loading
		    			ladda_btn_read_more.start();

		    			// load ajax of vehicle specific detail
		    			handleAjaxVehicleSpecificDetail( vehicle_id, {
		    				dataCallback: function(response){
		    					jQuery("#"+btn_read_more_id).removeProp("disabled");
		    					// stop btn loading
		    					ladda_btn_read_more.stop();
		    					// hide btn read more
		    					jQuery("#btn"+element_vehicle_id_ucfirst+"ReadMore").hide();
		    					// set click event for toggling specific detail
		    					jQuery("#"+element_vehicle_id+"TitlePanel"+
		    							",#btn"+element_vehicle_id_ucfirst+"ReadMore"+
		    							",#btn"+element_vehicle_id_ucfirst+"ReadLess").click(function(){
					    						jQuery("#"+element_vehicle_id+"SpecificDetail").slideToggle();
					    						jQuery("#btn"+element_vehicle_id_ucfirst+"ReadMore").toggle();
					    					});
		    				}
		    			} );
		    			
		    		});
		    		jQuery("#btn"+element_vehicle_id_ucfirst+"Compare").click(function(){

		    			if( jQuery(this).hasClass("active") ){
		    				globalMyComparison.remove({ 
		    					post_id: vehicle_id
		    					, success: function(response){

		    						var data = response.data;
		    						var notify_type = 'danger';
		    						var message = data.detail;
		    						if(data.status == 1){
		    							notify_type = 'success';
		    						}

		    						jQuery.notify({ message: message }, { type: notify_type });
		    					} 
		    				});
		    			}
		    			else{
		    				globalMyComparison.add({ 
		    					post_id: vehicle_id
		    					, success: function(response){

		    						var data = response.data;
		    						var notify_type = 'danger';
		    						var message = data.detail;
		    						if(data.status == 1){
		    							notify_type = 'success';
		    						}

		    						jQuery.notify({ message: message }, { type: notify_type });
		    					} 
		    				});
		    			}

		    		});

		    		// show vehicle
		    		jQuery("#"+element_vehicle_id).show();
		    	});
				
				// Mobile rendering
				jQuery.each(vehicles, function(index, vehicle){
					var sample_vehicle = jQuery("#"+_root.sample_mobile_vehicle_id).clone();
					var vehicle_id = vehicle['ID'];
					var element_vehicle_id = _root.prefix_mobile_vehicle_id + String(vehicle_id);		
					var element_vehicle_id_ucfirst = _root.prefix_vehicle_id_ucfirst + vehicle_id;

					var page_url_vehicle_details = _root.page_url_vehicle_details + '?post_id=' + vehicle_id;			

					// replace main vehicle id
					jQuery(sample_vehicle).prop("id", element_vehicle_id);
					// replace all related vehicle id in main vehicle
					jQuery(sample_vehicle).find("[id^='"+_root.sample_mobile_vehicle_id+"']").each(function(){
						var sample_id = jQuery(this).prop("id");
						var new_element_vehicle_id = sample_id.replace(_root.sample_mobile_vehicle_id, element_vehicle_id);
						jQuery(this).prop("id", new_element_vehicle_id);
					});
					// replace ids such as mobileBtnVehicle0, etc.
					jQuery(sample_vehicle).find("[id*='"+_root.sample_vehicle_id_ucfirst+"']").each(function(){
						var sample_id = jQuery(this).prop("id");
						var new_element_vehicle_id = sample_id.replace(_root.sample_vehicle_id_ucfirst, element_vehicle_id_ucfirst);
						jQuery(this).prop("id", new_element_vehicle_id);
					});

					//append element in the mobile vehicle list placeholder
					jQuery("#mobileVehiclesPlaceholder").append(sample_vehicle);

					//replace element with retrieved data
					var vehicle_title = vehicle.title;
					// html decode vehicle price unit 
					var vehicle_price = jQuery("<div/>").html(vehicle.price_unit).text() + ' ' + vehicle.price;
					var vehicle_featured_image = vehicle.featured_image;
					var vehicle_mileage = vehicle.mileage + ' ' + vehicle.mileage_unit;
					var vehicle_engine_size = vehicle.engine_size + ' ' + vehicle.engine_size_unit;
					var vehicle_engine_power = vehicle.engine_power + ' ' + vehicle.engine_power_unit;
					var vehicle_fuel_type = vehicle.fuel_type.name;
					var vehicle_transmission_type = vehicle.transmission_type.name;
					var vehicle_co2_emission = vehicle.co2_emission + ' ' + vehicle.co2_emission_unit;
					var vehicle_reg_date = new Date(vehicle.reg_date);
					vehicle_reg_date = String(vehicle_reg_date.getFullYear()) + ' ' + vehicle_reg_date.getMonthName();
					var vehicle_features = vehicle.features;
					var vehicle_dealer_location = vehicle.dealer_location;
					var vehicle_dealer_location_address = vehicle_dealer_location.title;
					var vehicle_dealer_location_phone = vehicle_dealer_location.phone;

		    		jQuery("#"+element_vehicle_id+"Title").text(vehicle_title);
		    		jQuery("#"+element_vehicle_id+"Price").text(vehicle_price);
		    		jQuery("#"+element_vehicle_id+"FeaturedImage").prop("title", vehicle_featured_image.title);
		    		jQuery("#"+element_vehicle_id+"FeaturedImage").prop("src", vehicle_featured_image.srcs['large']);
		    		jQuery("#"+element_vehicle_id+"Mileage").text(vehicle_mileage);
		    		jQuery("#"+element_vehicle_id+"EngineSize").text(vehicle_engine_size);
		    		jQuery("#"+element_vehicle_id+"EnginePower").text(vehicle_engine_power);
		    		jQuery("#"+element_vehicle_id+"FuelType").text(vehicle_fuel_type);
		    		jQuery("#"+element_vehicle_id+"TransmissionType").text(vehicle_transmission_type);
		    		jQuery("#"+element_vehicle_id+"Co2Emission").text(vehicle_co2_emission);
		    		jQuery("#"+element_vehicle_id+"RegDate").text(vehicle_reg_date);
		    		if(vehicle_features.length > 0){
		    			jQuery.each(vehicle_features, function(index, vehicle_feature){
		    				var vehicle_feature_term_id = vehicle_feature.term_id;
		    				var vehicle_feature_name = vehicle_feature.name;
		    				var vehicle_feature_slug = vehicle_feature.slug;

		    				if(vehicle_feature_slug !== 'volvo-selekt'){
		    					var element_feature = jQuery("#"+element_vehicle_id+"Feature").clone();
		    					jQuery(element_feature).prop("id", element_vehicle_id+"Feature"+vehicle_feature_term_id);
		    					jQuery(element_feature).text(vehicle_feature_name);
		    					jQuery(element_feature).insertAfter("#"+element_vehicle_id+"Feature");
		    					jQuery(element_feature).show();
		    				}
		    				else{
		    					jQuery("#"+element_vehicle_id+"FeatureVolvoSelekt").show();
		    				}
		    				
		    			});
		    		}
		    		jQuery("#"+element_vehicle_id+"DealerLocationAddress").text(vehicle_dealer_location_address);
		    		jQuery("#"+element_vehicle_id+"DealerLocationPhone").text(vehicle_dealer_location_phone);
		    		jQuery("#mobileBtn"+element_vehicle_id_ucfirst+"FullDetails").prop("href", page_url_vehicle_details);
		    		jQuery("#mobileBtn"+element_vehicle_id_ucfirst+"Compare").click(function(){

		    			if( jQuery(this).hasClass("active") ){
		    				globalMyComparison.remove({ 
		    					post_id: vehicle_id
		    					, success: function(response){

		    						var data = response.data;
		    						var notify_type = 'danger';
		    						var message = data.detail;
		    						if(data.status == 1){
		    							notify_type = 'success';
		    						}

		    						jQuery.notify({ message: message }, { type: notify_type });
		    					} 
		    				});
		    			}
		    			else{
		    				globalMyComparison.add({ 
		    					post_id: vehicle_id
		    					, success: function(response){

		    						var data = response.data;
		    						var notify_type = 'danger';
		    						var message = data.detail;
		    						if(data.status == 1){
		    							notify_type = 'success';
		    						}

		    						jQuery.notify({ message: message }, { type: notify_type });
		    					} 
		    				});
		    			}

		    		});

		    		// show vehicle
		    		jQuery("#"+element_vehicle_id).show();
				});

	    		// recover from disablility of btns show more and show all
	    		jQuery("#btnShowMore").removeProp("disabled");
	    		jQuery("#btnShowAll").removeProp("disabled");
	    		_ladda_btn_show_more.stop();
	    		_ladda_btn_show_all.stop();

	    		// hide spinner loading
	    		jQuery("#spinnerResultContent").hide();

	    		// finally load global search filter ajax to correct search form UI
	    		globalSearchFilter.ajax();
	    		// refresh global my comparison UI for btn Compare
	    		globalMyComparison.refresh();
	    		// refresh global popover of volvo selekt
	    		globalPopoverVolvoSelekt.refresh();
	   	    }
	   	    , error: function(jqXHR, textStatus, errorThrown) {
	   	        console.log('Error api-vehicles.');
	   	    }
	   	});

	};

	var handleAjaxVehicleSpecificDetail = function( vehicle_id, options ){

		if(vehicle_id === undefined){
			vehicle_id = 0;
		}
		if(options === undefined){
			options = { dataCallBack: '' };
		}

		var post_id = vehicle_id;

		// variable '_local_obj_page_search' is from functions.php
		jQuery.ajax({
			type: 'GET'
			, url: _local_obj_page_search.ajax_url
			, data: { 
				'action': _local_obj_page_search.vsd_action 
				, 'post_id': post_id
			}
			, success: function(response){

				jQuery("#vehicle"+vehicle_id).append(response);

				//retrieve final data using callback func
				if( !jQuery.isEmptyObject(options) ){
					if(options.hasOwnProperty('dataCallback') && typeof options.dataCallback == 'function' ){
						options.dataCallback( response );
					}
				}
			}
		});
	};

	return {
		init: function(){
			handleBasic();
			handleAjax();
		}
	};

}();