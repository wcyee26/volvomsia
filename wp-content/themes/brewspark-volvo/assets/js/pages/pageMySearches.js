var pageMySearches = function () {

	var _root = this;
	_root.options = {};

	_root.prefix_search_list_id = "searchList";
	_root.prefix_search_list_id_ucfirst = "SearchList";
	_root.sample_search_list_id = _root.prefix_search_list_id;
	_root.sample_search_list_id_ucfirst = _root.prefix_search_list_id_ucfirst;

	var handleBasic = function(){

		var _storage_name = 'my_searches';		
		// global units such as km, L, etc.
		var _obj_units = globalSettings.getUnits();
		var _obj_unit_ranges = globalSettings.getUnitRanges();

		var _price_unit = _obj_units.price;
		var _distance_unit = _obj_units.distance;
		var _engine_size_unit = _obj_units.engine_size;
		var _engine_power_unit = _obj_units.engine_power;
		var _co2_emission_unit = _obj_units.co2_emission;

		var _price_range = _obj_unit_ranges.price;
		var _mileage_range = _obj_unit_ranges.mileage;
		var _age_range = _obj_unit_ranges.age;
		var _co2_emission_range = _obj_unit_ranges.co2_emission;

		if(localStorage.getItem(_storage_name) != null){
			var storages = localStorage.getItem(_storage_name);		    					
			storages = JSON.parse(storages);

			if(storages.length > 0){

				jQuery.each(storages, function(index, storage){

					var sample_search_list = jQuery("#"+_root.sample_search_list_id).clone();

					var post_query = storage;
					// serialize array
					var post_queries = post_query.serializeArray();
					var post_query_href = _local_obj_page_my_searches.home_url + "?" + post_query;
					var element_search_list_id = _root.prefix_search_list_id + String(index);
					var element_search_list_id_ucfirst = _root.prefix_search_list_id_ucfirst + String(index);
					var search_list = sample_search_list;
					// array to store details text
					var search_list_details = [];
					
					jQuery(search_list).prop("id", element_search_list_id);

					jQuery(search_list).find("[id^='"+_root.sample_search_list_id+"']").each(function(){
						var sample_id = jQuery(this).prop("id");
						var new_element_search_list_id = sample_id.replace(_root.sample_search_list_id, element_search_list_id);
						jQuery(this).prop("id", new_element_search_list_id);
					});

					jQuery(search_list).find("[id*='"+_root.sample_search_list_id_ucfirst+"']").each(function(){
						var sample_id = jQuery(this).prop("id");
						var new_element_search_list_id_ucfirst = sample_id.replace(_root.sample_search_list_id_ucfirst, element_search_list_id_ucfirst);
						jQuery(this).prop("id", new_element_search_list_id_ucfirst);
					});

					// append search list element
					jQuery("#searchListPlaceholder").append(search_list);
					
					// request from API vehicles 					
					jQuery.ajax({
						type: 'GET'
						, url: _local_obj_page_my_searches.ajax_url
						, data: post_query + "&action=" + _local_obj_page_my_searches.av_action
						, dataType: "json"
						, success: function(response) {
							
							var vehicles = response.data;
							var meta = response.meta;

							var obj_vehicle_models = {};
							jQuery.each(vehicles, function(index_vehicle, vehicle){
								var vehicle_model = vehicle.vehicle_model;
								var vehicle_model_id = vehicle_model.term_id;
								var vehicle_model_name = vehicle_model.name;
								obj_vehicle_models[vehicle_model_id] = vehicle_model;
							});

							var slider_price_counter = 0;
							var slider_mileage_counter = 0;
							var slider_age_counter = 0;
							var slider_co2_emission_counter = 0;
							jQuery.each(post_queries, function(i, param_obj){
								if( param_obj['name'].indexOf('fieldVehicleModels') >= 0 ){

									jQuery.each(obj_vehicle_models, function(key_obj_vehicle_model, obj_vehicle_model){
										var vehicle_model = obj_vehicle_model;
										if( vehicle_model.term_id == parseInt(param_obj['value']) )
											search_list_details.push(vehicle_model.short_description + ' ' + vehicle_model.name);
									});

								}
								if( param_obj['name'].indexOf('fieldSliderPrices') >= 0 ){
									if(slider_price_counter == 0){
										if( parseInt(param_obj['value']) !=  parseInt(_price_range.min) ) 
											search_list_details.push( "from "+_price_unit+" "+parseInt(param_obj['value']) );
									}
									else{
										if( parseInt(param_obj['value']) !=  parseInt(_price_range.max) ) 
											search_list_details.push( "to "+_price_unit+" "+parseInt(param_obj['value']) );
									}
									slider_price_counter += 1;
								}
								if( param_obj['name'].indexOf('fieldSliderMileages') >= 0 ){
									if(slider_mileage_counter == 0){
										if( parseInt(param_obj['value']) !=  parseInt(_mileage_range.min) ) 
											search_list_details.push( "from "+parseInt(param_obj['value'])+_distance_unit );
									}
									else{
										if( parseInt(param_obj['value']) !=  parseInt(_mileage_range.max) ) 
											search_list_details.push( "to "+parseInt(param_obj['value'])+_distance_unit );
									}
									slider_mileage_counter += 1;
								}
								if( param_obj['name'].indexOf('fieldSliderAges') >= 0 ){
									if(slider_age_counter == 0){
										if( parseInt(param_obj['value']) !=  parseInt(_age_range.min) ) 
											search_list_details.push( "from year "+parseInt(param_obj['value']) );
									}
									else{
										if( parseInt(param_obj['value']) !=  parseInt(_age_range.max) ) 
											search_list_details.push( "to year "+parseInt(param_obj['value']) );
									}
									slider_age_counter += 1;
								}
								if( param_obj['name'].indexOf('fieldSliderCo2Emission') >= 0 ){
									if(slider_co2_emission_counter == 0){
										if( parseInt(param_obj['value']) !=  parseInt(_co2_emission_range.min) ) 
											search_list_details.push( "from "+parseInt(param_obj['value'])+_co2_emission_unit );
									}
									else{
										if( parseInt(param_obj['value']) !=  parseInt(_co2_emission_range.max) ) 
											search_list_details.push( "to "+parseInt(param_obj['value'])+_co2_emission_unit );
									}
									slider_co2_emission_counter += 1;
								}
								if( param_obj['name'] == 'fieldFuelType' && param_obj['value'] != '-1' ){
									var vehicle = vehicles[0];
									search_list_details.push(vehicle.fuel_type.name);
								}
								if( param_obj['name'] == 'fieldTransmissionType' && param_obj['value'] != '-1' ){
									var vehicle = vehicles[0];
									search_list_details.push(vehicle.transmission_type.name);
								}
								if( param_obj['name'] == 'fieldExteriorColour' && param_obj['value'] != '-1' ){
									var vehicle = vehicles[0];
									search_list_details.push(vehicle.ext_colour.name);
								}
								if( param_obj['name'].indexOf('fieldVehicleFeatures') >= 0 ){
									var vehicle = vehicles[0];
									var vehicle_features = vehicle.features;
									
									jQuery.each(vehicle_features, function(i_vehicle_feature, vehicle_feature){
										if( vehicle_feature.term_id == parseInt( param_obj['value'] ) )
											search_list_details.push(vehicle_feature.name);
									});
								}
							});					
								
							// insert content into search list details
							if(search_list_details.length > 0)
								jQuery("#"+element_search_list_id+"Details").html( search_list_details.join(", ") );	
							else
								jQuery("#"+element_search_list_id+"Details").html( "All" );
							// set result count
							jQuery("#"+element_search_list_id+"ResultCount").text(meta.count);

							// set 'data-index' for matching the searches order in localStorage
							jQuery("#"+element_search_list_id).attr("data-index", index);							

							jQuery("#btn"+element_search_list_id_ucfirst+"Details").prop("href", post_query_href);
							jQuery("#btn"+element_search_list_id_ucfirst+"Remove").click(function(){

								globalMySearches.remove({ 
									post_query_index: jQuery("#"+element_search_list_id).attr("data-index")
									, success: function(response){

										var data = response.data;
										var notify_type = 'danger';
										var message = data.detail;
										if(data.status == 1){
											notify_type = 'success';
										}

										jQuery.notify({ message: message }, { type: notify_type });

										// remove the element
										jQuery("#"+element_search_list_id).remove();

										var new_storages = localStorage.getItem(_storage_name);
										new_storages = JSON.parse(new_storages);

										if(new_storages.length > 0){
											// resetting my searches row order by setting 'data-index'
											jQuery(".my-searches").each(function(i, my_searches){
												jQuery(this).attr("data-index", i);
											});
										}
										
									} 
								});

							});

							// set different bg for odd row
							if(index % 2 == 1){
								jQuery("#"+element_search_list_id).addClass("result-list-content-row-deep-grey");						
							}
							// show search list
							jQuery("#"+element_search_list_id).show();
						}
						, error: function(jqXHR, textStatus, errorThrown) {
						    console.log('Error api-vehicles.');
						}
					});

					if( index == (storages.length-1) ){
						// remove sample search list to prevent bug when re-ordering 'data-index'
						jQuery("#"+_root.sample_search_list_id).remove();
					}
				});
			}
		}

	};

	return {
		init: function( options ){

			if(options === undefined){
				options = {};
			}

			_root.options = options;

			handleBasic();
		}
	};

}();