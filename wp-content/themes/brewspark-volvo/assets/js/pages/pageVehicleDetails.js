var pageVehicleDetails = function () {

	var _root = this;
	_root.options = {};

	var handleBasic = function(){

		var vehicle_id = _root.options.post_id;
		var locations = _root.options.locations;
		var location = locations[0];

		globalMapLocations.init({ locations: locations });
		
		// set btn compare for My Comparison
		jQuery("#btnVehicle"+vehicle_id+"Compare").click(function(){

			if( jQuery(this).hasClass("active") ){
				globalMyComparison.remove({ 
					post_id: vehicle_id
					, success: function(response){

						var data = response.data;
						var notify_type = 'danger';
						var message = data.detail;
						if(data.status == 1){
							notify_type = 'success';
						}

						jQuery.notify({ message: message }, { type: notify_type });
					} 
				});
			}
			else{
				globalMyComparison.add({ 
					post_id: vehicle_id
					, success: function(response){

						var data = response.data;
						var notify_type = 'danger';
						var message = data.detail;
						if(data.status == 1){
							notify_type = 'success';
						}

						jQuery.notify({ message: message }, { type: notify_type });
					} 
				});
			}

		});

		// set btn contact dealer in Dealer Location
		jQuery("#btnDealerLocation"+location.ID+"ContactDealer").click(function(){
			jQuery("#contactDealer").collapse('show');

			jQuery('html, body').animate({
	            scrollTop: jQuery("#contactDealer").offset().top - 100
	        }, 500);   
		});

		// replace Contact Form 7 recipient with dealer location email address
		jQuery("input[name='your-recipient']").val(location.email);

	}

	return {
		init: function( options ){

			if(options === undefined){
				options = { post_id: '', locations: '' };
			}

			_root.options = options;

			handleBasic();
		}
	}

}();