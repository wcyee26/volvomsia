<?php 

get_header();

$_page_url_vehicle_details = get_page_link( get_page_by_path('vehicle-details')->ID );
$_page_url_volvo_selekt = get_page_link( get_page_by_path('volvo-selekt')->ID );
$_page_url_find_dealer = get_page_link( get_page_by_path('find-dealer')->ID );

?>

<div class="site-content-banner">
	<!-- Load the slider with "sliderHome" alias every time -->
	<?php putRevSlider("sliderHome") ?>
</div>

<div class="site-content-container site-content-container-home">
	<div class="container">

		<!-- APPROVED USED CARS -->
		<section class="section-placeholder section-placeholder-primary">
			<div class="section-heading">
				<h2>Volvo Selekt Approved Used Cars</h2>
			</div>			
			<div class="section-content">
				<div class="row">
					<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1">
						<!-- FORM GLOBAL SEARCH FILTER -->
						<form id="formGlobalSearchFilter" method="GET" action="<?= home_url( '/' ); ?>">	
							<!-- FIELD GLOBAL SEARCH STRING -->
							<input type="hidden" id="fieldSearchString" name="s" />						
							<div class="row">
								<!-- VEHICLE MODEL LIST -->
								<div class="hidden-xs col-sm-12 col-md-8">
									<section class="section-article">
										<div class="section-heading">
											<h3>Select Model</h3>	
										</div>		
										<div class="section-content">
											<?php get_template_part( 'template-parts/search-filter-vehicle-models' ); ?>
										</div>
									</section>
								</div>
								<!-- VEHICLE FILTER LIST -->
								<div class="col-xs-12 col-sm-12 col-md-4">								
									<section class="section-article custom-filter">
										<div class="section-heading">
											<h3>Filter</h3>	
										</div>		
										<div class="section-content">
											<div class="row">
												<div class="col-md-12">
													<!-- VEHICLE PROPERTIES -->
													<?php get_template_part( 'template-parts/search-filter-vehicle-properties' ); ?>
													<!-- VEHICLE FEATURES -->
													<?php get_template_part( 'template-parts/search-filter-vehicle-features' ); ?>
													<!-- ACTION BUTTONS -->
													<div class="custom-filter-placeholder">	
														<button id="btnQuickSearch" type="button" class="btn btn-sharp btn-sharp-primary btn-width-max" style="display: none;">
															Quick Search
														</button>
														<button id="btnAdvancedSearch" type="button" class="btn btn-sharp btn-sharp-primary btn-width-max">
															Advanced Search
														</button>
														<button id="btnResetSearch" type="button" class="btn btn-sharp btn-sharp-primary btn-width-max">
															Reset Search
														</button>
														<button id="btnSearch" type="submit" 
																class="btn btn-sharp btn-sharp-secondary btn-width-max ladda-button"
																data-style="expand-left">
															<span id="btnSearchText">
																View <span id="resultCount">0</span> Results	
															</span>
														</button>
														<a id="btnSaveSearch" href="javascript:void(0)">Save search condition</a>
													</div>
												</div>
											</div>
										</div>
									</section>
								</div>
							</div>
						</form>
					</div>
				</div>
				
			</div>
		</section>
		
		<!-- FEATURED VEHICLES -->
		<section class="section-placeholder section-placeholder-primary">
			<div class="section-heading">
				<!-- BUTTONS FOR MOBILE DEVICE -->
				<a id="btnPrevFeaturedVehiclesExtraSmall" class="visible-xs-block" href="javascript:void(0)">
					<i class="pull-left fa fa-2x fa-chevron-left" aria-hidden="true"></i>	
				</a>
				<a id="btnNextFeaturedVehiclesExtraSmall" class="visible-xs-block" href="javascript:void(0)">
					<i class="pull-right fa fa-2x fa-chevron-right" aria-hidden="true"></i>	
				</a>
				<!-- BUTTONS FOR NORMAL SCREEN -->
				<a id="btnPrevFeaturedVehiclesSmall" class="hidden-xs" href="javascript:void(0)">
					<i class="pull-left fa fa-2x fa-chevron-left" aria-hidden="true"></i>	
				</a>
				<a id="btnNextFeaturedVehiclesSmall" class="hidden-xs" href="javascript:void(0)">
					<i class="pull-right fa fa-2x fa-chevron-right" aria-hidden="true"></i>	
				</a>
				<h2>Featured Vehicles</h2>				
			</div>	
			<div class="section-content">
				<div class="row">
					<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1">
						<div id="featuredVehicleGallerySlideshow" class="row brewspark-gallery-slideshow">
							<?php $featured_vehicles = BWooVehicle::get_featured_vehicles(); ?>
							<?php foreach($featured_vehicles as $key => $featured_vehicle) : ?>
							<div style="display:none;" 
								 data-brewspark-gallery-slideshow-slide-index="<?= $key+1; ?>"								 
								 class="col-xs-12 col-sm-4 brewspark-gallery-slideshow-slide">
								<div class="card-brochure">
									<div class="card-brochure-thumbnail">
										<a href="<?= $_page_url_vehicle_details . '?post_id=' . $featured_vehicle->ID; ?>">
											<img src="<?= $featured_vehicle->featured_image->srcs['large']; ?>">
										</a>
									</div>
									<div class="card-brochure-heading"><?= $featured_vehicle->title; ?></div>
									<div class="card-brochure-content">
										<ul class="ul-custom ul-col-ct-2">
											<li><?= $featured_vehicle->mileage . ' ' . $featured_vehicle->mileage_unit; ?></li>
											<li><?= $featured_vehicle->engine_size . ' ' . $featured_vehicle->engine_size_unit; ?></li>
											<li><?= $featured_vehicle->engine_power . ' ' . $featured_vehicle->engine_power_unit; ?></li>
											<li><?= $featured_vehicle->fuel_type->name; ?></li>
											<li><?= $featured_vehicle->transmission_type->name; ?></li>
											<li><?= $featured_vehicle->co2_emission . ' ' . $featured_vehicle->co2_emission_unit; ?></li>
											<?php $features = $featured_vehicle->features; ?>
											<?php foreach($features as $key => $feature) : ?>
												<?php if($feature->slug == 'volvo-selekt') : ?>
													<li class="<?= $feature->slug; ?>">
														<img width="100%" src="<?= get_template_directory_uri() ?>/assets/images/icon-volvo-selekt.png">
													</li>
												<?php else : ?>
													<li><?= $feature->name; ?><li>											
												<?php endif; ?>
											<?php endforeach; ?>
										</ul>
									</div>
									<div class="card-brochure-total">
										<?= $featured_vehicle->price_unit . ' ' . $featured_vehicle->price; ?>
									</div>
									<div class="card-brochure-footer clearfix">
										<a type="button" class="pull-right btn btn-sharp btn-sharp-secondary"
											href="<?= $_page_url_vehicle_details . '?post_id=' . $featured_vehicle->ID; ?>">
											Full Details
										</a>	
									</div>								
								</div>
							</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<!-- NEXT STEPS -->
		<section class="section-steps">
			<div class="section-heading">
				<h1>Next Steps</h1>
			</div>
			<div class="section-content">
				<div class="row">
					<div class="col-xs-12 col-sm-10 col-sm-offset-1">						
						<div class="row">
							<!-- <div class="col-xs-12 col-sm-4">
								<div class="card-placeholder">
									<a href="#">								
										<h4>FINANCE YOUR CAR</h4>
										<img src="<?= get_template_directory_uri() ?>/assets/images/icon-finance.png">	
										<p>Find out how Volvo Car Credit could help you purchase an approved used car.</p>
									</a>
								</div>						
							</div> -->
							<div class="col-xs-12 col-sm-4 col-sm-offset-2">
								<div class="card-placeholder">
									<a href="<?= $_page_url_volvo_selekt ?>">								
										<h4>VOLVO SELEKT</h4>
										<img src="<?= get_template_directory_uri() ?>/assets/images/icon-myvolvo.png">	
										<p>Find out more about the benefits of Volvo Selekt car.</p>
									</a>
								</div>				
							</div>
							<!-- <div class="col-xs-12 col-sm-4"> -->
							<div class="col-xs-12 col-sm-4">
								<div class="card-placeholder">
									<a href="<?= $_page_url_find_dealer ?>">								
										<h4>DEALER LOCATION</h4>
										<img src="<?= get_template_directory_uri() ?>/assets/images/icon-map-marker.png">	
										<p>Find a Volvo Selekt dealer in your area.</p>
									</a>
								</div>		
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

<script type="text/javascript" defer>													
	jQuery(document).ready(function(){
		pageIndex.init();		
	});
</script>

<?php get_footer() ?>