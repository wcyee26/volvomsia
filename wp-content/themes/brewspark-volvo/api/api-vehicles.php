<?php 

// Default WP Search String Field
$fieldSearchString = isset($_GET['s']) ? $_GET['s'] : null;

$fieldVehicleModels = isset($_GET['fieldVehicleModels']) ? $_GET['fieldVehicleModels'] : null;
$fieldSliderPrices = isset($_GET['fieldSliderPrices']) ? $_GET['fieldSliderPrices'] : null;
$fieldSliderMileages = isset($_GET['fieldSliderMileages']) ? $_GET['fieldSliderMileages'] : null;
$fieldSliderAges = isset($_GET['fieldSliderAges']) ? $_GET['fieldSliderAges'] : null;
$fieldSliderCo2Emissions = isset($_GET['fieldSliderCo2Emissions']) ? $_GET['fieldSliderCo2Emissions'] : null;
$fieldFuelType = isset($_GET['fieldFuelType']) ? $_GET['fieldFuelType'] : null;
$fieldTransmissionType = isset($_GET['fieldTransmissionType']) ? $_GET['fieldTransmissionType'] : null;
$fieldExteriorColour = isset($_GET['fieldExteriorColour']) ? $_GET['fieldExteriorColour'] : null;
$fieldVehicleFeatures = isset($_GET['fieldVehicleFeatures']) ? $_GET['fieldVehicleFeatures'] : null;

// pagination
$pageNumber = isset($_GET['pageNumber']) ? $_GET['pageNumber'] : 0;
$pageSize = isset($_GET['pageSize']) ? $_GET['pageSize'] : -1;
$resultCount = isset($_GET['resultCount']) ? $_GET['resultCount'] : 0;
// ordering
$order = isset($_GET['order']) ? $_GET['order'] : 'DESC';
$orderby = isset($_GET['orderby']) ? $_GET['orderby'] : 'date';

$args = array();
// pages
$args['offset'] = ($pageNumber > 0) ? ( $pageNumber - 1 ) * $pageSize : 0;
$args['posts_per_page'] = $pageSize;
// show rest of posts
if($pageNumber > 1 && $resultCount > 0){
    $args['posts_per_page'] = $resultCount - ( ( $pageNumber - 1 ) * $pageSize );
}

// orders
$args['order'] = $order; 
$args['orderby'] = $orderby;      
if( $orderby == 'price' ){
    $args['meta_key'] = '_price';
    $args['orderby'] = 'meta_value_num';
}
elseif( $orderby == 'mileage' ){
    $args['meta_key'] = '_bwoovehicle_mileage';
    $args['orderby'] = 'meta_value_num';   
}
elseif( $orderby == 'regDate' ){
    $args['meta_key'] = '_bwoovehicle_reg_date';
    $args['meta_type'] = 'date';
    $args['orderby'] = 'meta_value';   
}
elseif( $orderby == 'enginePower' ){
    $args['meta_key'] = '_bwoovehicle_engine_power';
    $args['orderby'] = 'meta_value_num';   
}

// taxonomy
$args['tax_query'] = array();
// post meta
$args['meta_query'] = array();

if(!empty($fieldVehicleModels)){
	$args['tax_query'][] = array(
	            'taxonomy' => 'product_cat'
	            , 'field' => 'term_id'
	            , 'terms' => $fieldVehicleModels
	            , 'operator' => 'IN'
	        );
}
if(!empty($fieldSliderPrices)){
	$args['meta_query'][] = array(
                    'key' => '_price'
                	, 'value' => $fieldSliderPrices
                    , 'compare' => 'BETWEEN'
                    , 'type' => 'numeric'
                );
}
if(!empty($fieldSliderMileages)){
	$args['meta_query'][] = array(
                    'key' => '_bwoovehicle_mileage'
                	, 'value' => $fieldSliderMileages
                    , 'compare' => 'BETWEEN'
                    , 'type' => 'numeric'
                );
}
if(!empty($fieldSliderAges)){
	$fieldSliderAges[0] = intval($fieldSliderAges[0]) . '-01-01';
	$fieldSliderAges[1] = intval($fieldSliderAges[1]) . '-12-31';

	$args['meta_query'][] = array(
                    'key' => '_bwoovehicle_reg_date'
                	, 'value' => $fieldSliderAges
                    , 'compare' => 'BETWEEN'
                    , 'type' => 'date'
                );
}
if(!empty($fieldSliderCo2Emissions)){
	$args['meta_query'][] = array(
                    'key' => '_bwoovehicle_co2_emission'
                	, 'value' => $fieldSliderCo2Emissions
                    , 'compare' => 'BETWEEN'
                    , 'type' => 'numeric'
                );
}
if(!empty($fieldFuelType) && $fieldFuelType != '-1'){
	$args['meta_query'][] = array(
                    'key' => '_bwoovehicle_fuel_type_id'
                	, 'value' => $fieldFuelType
                    , 'type' => 'numeric'
                );
}
if(!empty($fieldTransmissionType) && $fieldTransmissionType != '-1'){
	$args['meta_query'][] = array(
                    'key' => '_bwoovehicle_transmission_type_id'
                	, 'value' => $fieldTransmissionType
                    , 'type' => 'numeric'
                );
}

if(!empty($fieldExteriorColour) && $fieldExteriorColour != '-1'){
	$args['meta_query'][] = array(
                    'key' => '_bwoovehicle_ext_colour_id'
                	, 'value' => $fieldExteriorColour
                    , 'type' => 'numeric'
                );
}
if(!empty($fieldVehicleFeatures)){
	foreach ($fieldVehicleFeatures as $key => $fieldVehicleFeature) {
		$args['tax_query'][] = array(
		            'taxonomy' => 'pa_vehicle-feature'
		            , 'field' => 'term_id'
		            , 'terms' => $fieldVehicleFeature
		            , 'operator' => 'IN'
		        );
	}
	
}

$paginated_vehicles = BWooVehicle::get_vehicles($args, $is_meta_enabled = true);
$vehicles_meta = $paginated_vehicles->meta;
$vehicles = $paginated_vehicles->data;

// // DEBUGGER
// foreach($vehicles as $vehicle){
//     print_r($vehicle->created_date . '&nbsp;&nbspRM' . $vehicle->price . '&nbsp;&nbsp' . $vehicle->mileage . 'km'
//             . '&nbsp;&nbsp' . $vehicle->reg_date . '&nbsp;&nbsp' . $vehicle->vehicle_model->name
//             . '&nbsp;&nbsp&nbsp;&nbsp' . $vehicle->engine_power
//             . '&nbsp;&nbsp' . $vehicle->vehicle_model->count);
//     print_r('<br/>');
//     // print_r($vehicle->vehicle_model->count);
// }
// exit();

$response = array();
$response['meta'] = array( 'count' => $vehicles_meta->found_posts, 'max_num_pages' => $vehicles_meta->max_num_pages );
$response['data'] = $vehicles;

if($pageNumber > 0){
    $response['links'] = array('self' => get_page_link() . "?pageNumber=" . $pageNumber . "&pageSize=" . $pageSize
                            , 'first' => get_page_link() . "?pageNumber=1&pageSize=" . $pageSize
                            , 'prev' => get_page_link() . "?pageNumber=" . ($pageNumber<=1 ? 1 : $pageNumber-1 ) . "&pageSize=" . $pageSize
                            , 'next' => get_page_link() . "?pageNumber=" . ($pageNumber>=$vehicles_meta->max_num_pages ? $vehicles_meta->max_num_pages : $pageNumber+1) . "&pageSize=" . $pageSize
                            , 'last' => get_page_link() . "?pageNumber=" . $vehicles_meta->max_num_pages . "&pageSize=" . $pageSize
                        );
}
else{
    $response['links'] = array( 'self' => get_page_link() );
}


$json_response = json_encode($response);
echo $json_response;

