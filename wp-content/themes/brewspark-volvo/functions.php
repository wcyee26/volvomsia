<?php

// setup default pages for themes
/**
 * Check if the post/page exists
 * @param  String $post_name  The slug of the post/page
 * @return Boolean
 */
function the_slug_exists($post_name) {
	global $wpdb;
	
	if($wpdb->get_row("SELECT post_name FROM " . $wpdb->prefix . "wp_posts"
						. " WHERE post_name = '" . $post_name . "' AND post_status = 'publish'", 'ARRAY_A')) {
		return true;
	} 
	else {
		return false;
	}
}

// create the blog page
if ( isset($_GET['activated']) && is_admin() ){

	$pages = array( 
		'volvo-selekt' => array(
			'title' => 'Volvo Selekt'
			) 
		, 'find-dealer' => array(
			'title' => 'Find Dealer'
			)
		, 'my-comparison' => array(
			'title' => 'My Comparison'
			)
		, 'my-searches' => array(
			'title' => 'My Searches'
			)
		, 'vehicle-details' => array(
			'title' => 'Vehicle Details'
			)
		, 'volvo-selekt-summary' => array(
			'title' => 'Volvo Selekt Summary'
			)
		);

	foreach ($pages as $slug => $page) {
	    $page_title = $page['title'];
	    $page_content = isset($page['content']) ? $page['content'] : '';
	    $page_slug = $slug;
	    $new_page = array(
		    'post_type' => 'page'
		    , 'post_title' => $page_title
		    , 'post_content' => $page_content
		    , 'post_status' => 'publish'
		    , 'post_author' => 1
		    , 'post_slug' => $page_slug
	    );

	    if( !the_slug_exists( $page_slug ) ){
	        $page_id = wp_insert_post($new_page);
	    }
	}

}

add_action( 'wp_enqueue_scripts', 'mat_assets' );
function mat_assets() {	
	// CSS
	wp_register_script( 'bootstrap-js'
						, get_template_directory_uri() . '/assets/plugins/bootstrap/js/bootstrap.min.js'
						, array('jquery')
						, '3.3.7'
						, true 
						);
	wp_register_style( 'bootstrap-css'
						, get_template_directory_uri() . '/assets/plugins/bootstrap/css/bootstrap.min.css'
						, array()
						, '3.3.7'
						, 'all' 
						);
	wp_register_style( 'bootstrap-vertical-tabs-css'
						, get_template_directory_uri() . '/assets/plugins/bootstrap-vertical-tabs/bootstrap.vertical-tabs.min.css'
						, array()
						, '1.2.2'
						, 'all' 
						);

	wp_register_style( 'font-awesome-css'
						, get_template_directory_uri() . '/assets/plugins/font-awesome/css/font-awesome.min.css'
						, array()
						, '4.7.0'
						, 'all' 
						);
	wp_register_style( 'no-ui-slider-css'
						, get_template_directory_uri() . '/assets/plugins/no-ui-slider/nouislider.min.css'
						, array()
						, '9.2.0'
						, 'all' 
						);
	wp_register_style( 'swipebox-css'
						, get_template_directory_uri() . '/assets/plugins/swipebox/src/css/swipebox.min.css'
						, array()
						, '1.0'
						, 'all' 
						);
	wp_register_style( 'daneden-animate-css'
						, get_template_directory_uri() . '/assets/plugins/daneden/animate.css'
						, array()
						, '1.0'
						, 'all' 
						);
	wp_register_style( 'ladda-bootstrap-css'
						, get_template_directory_uri() . '/assets/plugins/ladda-bootstrap/ladda-themeless.min.css'
						, array()
						, '1.0'
						, 'all' 
						);

	// Javascript
	wp_register_script( 'no-ui-slider-js'
						, get_template_directory_uri() . '/assets/plugins/no-ui-slider/nouislider.min.js'
						, array()
						, '9.2.0'
						, true 
						);
	wp_register_script( 'swipebox-js'
						, get_template_directory_uri() . '/assets/plugins/swipebox/src/js/jquery.swipebox.min.js'
						, array()
						, '1.0'
						, true 
						);

	wp_register_script( 'wnumb-js'
						, get_template_directory_uri() . '/assets/plugins/wnumb/wNumb.js'
						, array()
						, '1.0.2'
						, true 
						);

	wp_register_script( 'slider-range-formatter-js'
						, get_template_directory_uri() . '/assets/plugins/slider-range-formatter/sliderRangeFormatter.js'
						, array()
						, '1.0'
						, true 
						);

	wp_register_script( 'brewspark-gallery-slideshow-js'
						, get_template_directory_uri() . '/assets/plugins/brewspark-gallery-slideshow/brewsparkGallerySlideshow.js'
						, array()
						, '1.0'
						, true 
						);

	wp_register_script( 'v3-utility-library-markerclustererplus-js'
						, get_template_directory_uri() . '/assets/plugins/v3-utility-library/markerclustererplus/src/markerclusterer.js'
						, array()
						, '1.0.1'
						, true 
						);

	// notify animate intro and exit animation relies on daneden-animate-css
	wp_register_script( 'bootstrap-notify-js'
						, get_template_directory_uri() . '/assets/plugins/bootstrap-notify-3.1.3/dist/bootstrap-notify.min.js'
						, array()
						, '3.1.3'
						, true 
						);
	wp_register_script( 'ladda-bootstrap-spin-js'
						, get_template_directory_uri() . '/assets/plugins/ladda-bootstrap/spin.min.js'
						, array()
						, '0.9.4'
						, true 
						);
	wp_register_script( 'ladda-bootstrap-js'
						, get_template_directory_uri() . '/assets/plugins/ladda-bootstrap/ladda.min.js'
						, array()
						, '0.9.4'
						, true 
						);
	
	// CSS
	wp_enqueue_style('bootstrap-css');
	wp_enqueue_style('bootstrap-vertical-tabs-css');
	wp_enqueue_style('font-awesome-css');	
	wp_enqueue_style('no-ui-slider-css');	
	wp_enqueue_style('swipebox-css');	
	wp_enqueue_style('daneden-animate-css');	
	wp_enqueue_style('ladda-bootstrap-css');	
	wp_enqueue_style('brewspark-volvo', get_stylesheet_directory_uri() . '/style.css', array(), filemtime( get_stylesheet_directory() . '/style.css' ) );

	// Javascript
	// Plugin
	wp_enqueue_script('bootstrap-js');
	wp_enqueue_script('no-ui-slider-js');
	wp_enqueue_script('swipebox-js');
	wp_enqueue_script('wnumb-js');
	wp_enqueue_script('slider-range-formatter-js');	
	wp_enqueue_script('brewspark-gallery-slideshow-js');	
	wp_enqueue_script('v3-utility-library-markerclustererplus-js');	
	wp_enqueue_script('bootstrap-notify-js');	
	wp_enqueue_script('ladda-bootstrap-spin-js');	
	wp_enqueue_script('ladda-bootstrap-js');	

	// Globals
	wp_enqueue_script( 'global-settings-js'
						, get_template_directory_uri() . '/assets/js/globals/globalSettings.js'
						, array()
						, filemtime( get_template_directory() . '/assets/js/globals/globalSettings.js' )
						, false 
						);
	wp_enqueue_script( 'global-common-js'
						, get_template_directory_uri() . '/assets/js/globals/globalCommon.js'
						, array()
						, filemtime( get_template_directory() . '/assets/js/globals/globalCommon.js' )
						, true 
						);
	wp_enqueue_script( 'global-search-filter-js'
						, get_template_directory_uri() . '/assets/js/globals/globalSearchFilter.js'
						, array()
						, filemtime( get_template_directory() . '/assets/js/globals/globalSearchFilter.js' )
						, true 
						);
	// pass variable '_local_obj_global_search_filter' with array of data into 'global-search-filter-js'
	wp_localize_script( 'global-search-filter-js'
						, '_local_obj_global_search_filter'
						, array( 
							'home_url' => home_url('/')
							, 'ajax_url' => admin_url( 'admin-ajax.php' )
							, 'av_action' => 'api_vehicles_action'
							)
						);
	wp_enqueue_script( 'global-map-locations-js'
						, get_template_directory_uri() . '/assets/js/globals/globalMapLocations.js'
						, array()
						, filemtime( get_template_directory() . '/assets/js/globals/globalMapLocations.js' )
						, true 
						);
	wp_enqueue_script( 'global-my-comparison-js'
						, get_template_directory_uri() . '/assets/js/globals/globalMyComparison.js'
						, array()
						, filemtime( get_template_directory() . '/assets/js/globals/globalMyComparison.js' )
						, true 
						);
	wp_enqueue_script( 'global-my-searches-js'
						, get_template_directory_uri() . '/assets/js/globals/globalMySearches.js'
						, array()
						, filemtime( get_template_directory() . '/assets/js/globals/globalMySearches.js' )
						, true 
						);
	wp_enqueue_script( 'global-popover-volvo-selekt-js'
						, get_template_directory_uri() . '/assets/js/globals/globalPopoverVolvoSelekt.js'
						, array()
						, filemtime( get_template_directory() . '/assets/js/globals/globalPopoverVolvoSelekt.js' )
						, true 
						);

	// Pages
	wp_enqueue_script( 'page-index-js'
						, get_template_directory_uri() . '/assets/js/pages/pageIndex.js'
						, array()
						, filemtime( get_template_directory() . '/assets/js/pages/pageIndex.js' )
						, true 
						);
	wp_enqueue_script( 'page-search-js'
						, get_template_directory_uri() . '/assets/js/pages/pageSearch.js'
						, array()
						, filemtime( get_template_directory() . '/assets/js/pages/pageSearch.js' )
						, true 
						);
	// pass variable '_local_obj_page_search' with array of data into 'page-search.js'
	wp_localize_script( 'page-search-js'
						, '_local_obj_page_search'
						, array( 
							'ajax_url' => admin_url( 'admin-ajax.php' ) 
							, 'vsd_action' => 'vehicle_specific_detail_action'
							, 'mc_action' => 'my_comparison_action'
							, 'av_action' => 'api_vehicles_action'
							) 
						);

	wp_enqueue_script( 'page-vehicle-details-js'
						, get_template_directory_uri() . '/assets/js/pages/pageVehicleDetails.js'
						, array()
						, filemtime( get_template_directory() . '/assets/js/pages/pageVehicleDetails.js' )
						, true 
						);
	// pass variable '_local_obj_page_vehicle_details' with array of data into 'page-vehicle-details.js'
	wp_localize_script( 'page-vehicle-details-js'
						, '_local_obj_page_vehicle_details'
						, array( 
							'page_url_vehicle_details' => get_page_link( get_page_by_path('vehicle-details')->ID )
							)
						);
	wp_enqueue_script( 'page-find-dealer-js'
						, get_template_directory_uri() . '/assets/js/pages/pageFindDealer.js'
						, array()
						, filemtime( get_template_directory() . '/assets/js/pages/pageFindDealer.js' )
						, true 
						);
	wp_localize_script( 'page-find-dealer-js'
						, '_local_obj_page_find_dealer'
						, array( 
							'template_directory_uri' => get_template_directory_uri()
							) 
						);

	wp_enqueue_script( 'page-my-comparison-js'
						, get_template_directory_uri() . '/assets/js/pages/pageMyComparison.js'
						, array()
						, filemtime( get_template_directory() . '/assets/js/pages/pageMyComparison.js' )
						, true 
						);
	// pass variable '_local_obj' with array of data into 'page-my-comparison.js'
	wp_localize_script( 'page-my-comparison-js'
						, '_local_obj_page_my_comparison'
						, array( 
							'home_url' => home_url('/')
							, 'page_url_vehicle_details' => get_page_link( get_page_by_path('vehicle-details')->ID )
							)
						);

	wp_enqueue_script( 'page-my-searches-js'
						, get_template_directory_uri() . '/assets/js/pages/pageMySearches.js'
						, array()
						, filemtime( get_template_directory() . '/assets/js/pages/pageMySearches.js' )
						, true 
						);
	// pass variable '_local_obj' with array of data into 'page-my-searches.js'
	wp_localize_script( 'page-my-searches-js'
						, '_local_obj_page_my_searches'
						, array( 
							'home_url' => home_url('/')
							, 'ajax_url' => admin_url( 'admin-ajax.php' )
							, 'av_action' => 'api_vehicles_action'
							)
						);
}

// Init Menu Registration
add_action( 'init', 'register_my_menus' );
function register_my_menus() {
	register_nav_menus(
		array(
			'header-menu' => __( 'Header Menu' )
			, 'personal-menu' => __( 'Personal Menu' )
			, 'footer-menu' => __( 'Footer Menu' )
		)
	);
}

// Init Ajax Handlers
add_action( 'wp_ajax_vehicle_specific_detail_action', 'vehicle_specific_detail_action' );
add_action( 'wp_ajax_nopriv_vehicle_specific_detail_action', 'vehicle_specific_detail_action' );
add_action( 'wp_ajax_api_vehicles_action', 'api_vehicles_action' );
add_action( 'wp_ajax_nopriv_api_vehicles_action', 'api_vehicles_action' );

function vehicle_specific_detail_action(){

	$post_id = isset( $_GET['post_id'] ) ? $_GET['post_id'] : 0;

	require_once( __DIR__ . '/template-parts/vehicle-specific-detail.php');

	wp_die();
}

function api_vehicles_action(){

	require_once( __DIR__ . '/api/api-vehicles.php');

	wp_die();
}