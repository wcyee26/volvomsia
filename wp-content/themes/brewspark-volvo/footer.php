<?php 

$new_vehicle_models = BWooVehicle::get_new_vehicle_models();
$demo_vehicle_models = BWooVehicle::get_demo_vehicle_models();
$one_year_old_vehicle_models = BWooVehicle::get_one_year_old_vehicle_models();
$normal_used_vehicle_models = BWooVehicle::get_normal_used_vehicle_models();

$_page_url_search = home_url('/') . '?s=';

?>		

		</div> <!-- site content -->

		<footer class="site-footer">
			<div class="site-footer-primary">				
				<div class="container-fluid">
					<div class="social-share">
						<a href="https://www.facebook.com/Volvo">
							<i class="fa fa-2x fa-facebook-square" aria-hidden="true"></i>
						</a>
						<a href="https://twitter.com/volvocarsglobal">
							<i class="fa fa-2x fa-twitter" aria-hidden="true"></i>
						</a>
						<a href="https://www.youtube.com/user/VolvoCarsNews">
							<i class="fa fa-2x fa-youtube" aria-hidden="true"></i>
						</a>
					</div>
					<div class="country">
						<img src="<?php echo get_template_directory_uri() ?>/assets/images/icon-globe.png" width="24px">
						<a href="http://www.volvocars.com/my" target="_blank">Visit volvocars.com/my</a>
					</div>
					<div class="legal">
						<p>
							Copyright © 2017 Volvo Cars. All Rights Reserved
							</br>
							<span>
								<a href="http://www.volvocars.com/my/footer/cookies">Cookies</a>
								&nbsp;&nbsp;|&nbsp;&nbsp;
								<a href="http://www.volvocars.com/my/footer/legal">Legal</a>
								&nbsp;&nbsp;|&nbsp;&nbsp;
								<a href="http://www.volvocars.com/my/footer/pdpa">PDPA</a>
								&nbsp;&nbsp;|&nbsp;&nbsp;
								<a href="http://www.volvocars.com/my/footer/privacy">Privacy</a>
							</span>							
						</p>						
					</div>
					<div class="logo-text">
						<a href="http://www.volvocars.com/my">
							<img src="<?php echo get_template_directory_uri() ?>/assets/images/volvo.png">
						</a>
					</div>
				</div>
			</div>
			<div class="site-footer-secondary collapse navbar-collapse">
				<div class="container">
					<div class="back-to-top">
						<a href="#">
							<i class="fa fa-2x fa-angle-up" aria-hidden="true"></i>
						</a>
					</div>				
					<section>
						<h6 class="section-title">Volvo</h6>
						<div class="section-content">							
							<div class="row">
								<div class="col-xs-1 col-md-1"></div>
								<div class="col-xs-2 col-md-2">
									<?php $url_new_cars = $_page_url_search; ?>
									<?php foreach ($new_vehicle_models as $index => $new_vehicle_model) : ?>
									<?php $url_new_cars .= "&fieldVehicleModels[]=" . (String) $new_vehicle_model->term_id; ?>
									<?php endforeach; ?>
									<a href="<?= $url_new_cars; ?>">Volvo New Cars</a>
								</div>
								<div class="col-xs-2 col-md-2">
									<?php $url_normal_used_cars = $_page_url_search; ?>
									<?php foreach ($normal_used_vehicle_models as $index => $normal_used_vehicle_model) : ?>
									<?php $url_normal_used_cars .= "&fieldVehicleModels[]=" . (String) $normal_used_vehicle_model->term_id; ?>
									<?php endforeach; ?>
									<a href="<?= $url_normal_used_cars; ?>">Volvo Used Cars</a>
								</div>
								<div class="col-xs-2 col-md-2">
									<?php $url_one_year_old_cars = $_page_url_search; ?>
									<?php foreach ($one_year_old_vehicle_models as $index => $one_year_old_vehicle_model) : ?>
									<?php $url_one_year_old_cars .= "&fieldVehicleModels[]=" . (String) $one_year_old_vehicle_model->term_id; ?>
									<?php endforeach; ?>
									<a href="<?= $url_one_year_old_cars; ?>">Volvo One Year Old Cars</a>
								</div>
								<div class="col-xs-2 col-md-2">
									<?php $url_demo_cars = $_page_url_search; ?>
									<?php foreach ($demo_vehicle_models as $index => $demo_vehicle_model) : ?>
									<?php $url_demo_cars .= "&fieldVehicleModels[]=" . (String) $demo_vehicle_model->term_id; ?>
									<?php endforeach; ?>
									<a href="<?= $url_demo_cars; ?>">Volvo Demo Cars</a>
								</div>
								<div class="col-xs-2 col-md-2"></div>
								<div class="col-xs-1 col-md-1"></div>
							</div>
						</div>
					</section>				
					<section>
						<h6 class="section-title">Models</h6>						
						<div class="section-content">							
							<div class="row">
								<div class="col-xs-1 col-md-1">
								</div>
								<!-- NEW VEHICLE MODELS -->
								<div class="col-xs-2 col-md-2">			
									<?php foreach ($new_vehicle_models as $index => $new_vehicle_model) : ?>
									<div>
										<a href="<?= $_page_url_search . '&fieldVehicleModels[]=' . $new_vehicle_model->term_id; ?>">
											Volvo <?=  $new_vehicle_model->short_description . ' ' . $new_vehicle_model->name; ?>
										</a>
									</div>
									<?php endforeach; ?>
								</div>
								<!-- NORMAL USED VEHICLE MODELS -->
								<div class="col-xs-2 col-md-2">
									<?php foreach ($normal_used_vehicle_models as $index => $normal_used_vehicle_model) : ?>
									<div>
										<a href="<?= $_page_url_search . '&fieldVehicleModels[]=' . $normal_used_vehicle_model->term_id; ?>">
											Volvo <?=  $normal_used_vehicle_model->short_description . ' ' . $normal_used_vehicle_model->name; ?>
										</a>
									</div>
									<?php endforeach; ?>
								</div>
								<!-- ONE YEAR OLD VEHICLE MODELS -->
								<div class="col-xs-2 col-md-2">
									<?php foreach ($one_year_old_vehicle_models as $index => $one_year_old_vehicle_model) : ?>
									<div>
										<a href="<?= $_page_url_search . '&fieldVehicleModels[]=' . $one_year_old_vehicle_model->term_id; ?>">
											Volvo <?=  $one_year_old_vehicle_model->short_description . ' ' . $one_year_old_vehicle_model->name; ?>
										</a>
									</div>
									<?php endforeach; ?>
								</div>
								<!-- DEMO VEHICLE MODELS (FIRST HALF) -->
								<div class="col-xs-2 col-md-2">
									<?php foreach ($demo_vehicle_models as $index => $demo_vehicle_model) : ?>
									<?php if( $index < count( $demo_vehicle_models )/2 ) : ?>
									<div>
										<a href="<?= $_page_url_search . '&fieldVehicleModels[]=' . $demo_vehicle_model->term_id; ?>">
											Volvo <?=  $demo_vehicle_model->short_description . ' ' . $demo_vehicle_model->name; ?>
										</a>
									</div>
									<?php endif; ?>
									<?php endforeach; ?>
								</div>
								<!-- DEMO VEHICLE MODELS (SECOND HALF) -->
								<div class="col-xs-2 col-md-2">
									<?php foreach ($demo_vehicle_models as $index => $demo_vehicle_model) : ?>
									<?php if( $index >= count( $demo_vehicle_models )/2 ) : ?>
									<div>
										<a href="<?= $_page_url_search . '&fieldVehicleModels[]=' . $demo_vehicle_model->term_id; ?>">
											Volvo <?=  $demo_vehicle_model->short_description . ' ' . $demo_vehicle_model->name; ?>
										</a>
									</div>
									<?php endif; ?>
									<?php endforeach; ?>
								</div>
								<div class="col-xs-1 col-md-1">									
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</footer>
	</div>

	<?php wp_footer(); ?>

	<?php 
		// get page content from 'volvo-selekt-summary'
		$volvo_selekt_summary_slug = 'volvo-selekt-summary';
		$volvo_selekt_summary_args = array(
			'name'           => $volvo_selekt_summary_slug,
			'post_type'      => 'page',
			'post_status'    => 'publish',
			'posts_per_page' => 1
		);
		$volvo_selekt_summary_page = get_posts( $volvo_selekt_summary_args )[0];
	?>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYX0k9hh35UhmjM6ScY_YV8yIC4IgF63Q"></script>
	<script type="text/javascript">
		jQuery("document").ready(function(){	

			// enable collapse link from url
			var url = document.location.toString();
			if (url.match('#')) {
				var hash = url.split('#')[1];
				if(hash != '' && hash != null){					
				    jQuery('.collapse[id="' + hash + '"]').collapse('show');
				    
		    		jQuery('html, body').animate({
		                scrollTop: jQuery("#"+hash).offset().top - 100
		            }, 500);
				}
			}

			var _page_url_my_comparison = "<?= get_page_link( get_page_by_path('my-comparison')->ID ); ?>";
			var _page_url_my_searches = "<?= get_page_link( get_page_by_path('my-searches')->ID ); ?>";
				
			globalMyComparison.init({ page_url_my_comparison: _page_url_my_comparison });
			globalMySearches.init({ page_url_my_searches: _page_url_my_searches });

			// init swipebox
			jQuery(".swipebox").swipebox();

			// init popover of volvo selekt
			globalPopoverVolvoSelekt.init({
				content: <?= json_encode( $volvo_selekt_summary_page->post_content ); ?>
			});
		});
	</script>
</body>
</html>