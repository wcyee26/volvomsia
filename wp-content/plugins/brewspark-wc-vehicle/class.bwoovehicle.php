<?php

class BWooVehicle {
	const PLUGIN_SLUG = 'bwoovehicle';
	const STORE_LOCATOR_PLUGIN_SLUG = 'wpsl';
	const GOOGLE_MAP_URL = 'https://www.google.com/maps/';

	private static $initiated = false;

	public static function init() {
		if ( ! self::$initiated ) {
			self::init_hooks();
		}
	}

	/**
	 * Initializes WordPress hooks
	 */
	private static function init_hooks() {
		self::$initiated = true;
	}

	/**
	 * Get Fuel Types
	 * @return  array  An Array of WP_Term Object
	 */
	public static function get_fuel_types(){
		$fuel_types = get_terms( array(
		    'taxonomy' => 'pa_fuel-type'
		    , 'hide_empty' => false
		) );

		return $fuel_types;
	}

	/**
	 * Convert slug each word to first letter to uppercase
	 * @return  string $new_words
	 */
	public static function ucslug( $words, $separator = '' ){
		$new_words = implode($separator, array_map('ucfirst', explode('-', $words)));
		return $new_words;
	}

	/**
	 * Get Transmission Types
	 * @return  array  An Array of WP_Term Object
	 */
	public static function get_transmission_types(){
		$transmission_types = get_terms( array(
		    'taxonomy' => 'pa_transmission-type'
		    , 'hide_empty' => false
		) );

		return $transmission_types;
	}

	/**
	 * Get Exterior Colours
	 * @return  array  An Array of WP_Term Object
	 */
	public static function get_exterior_colours(){
		$exterior_colours = get_terms( array(
		    'taxonomy' => 'pa_exterior-colour'
		    , 'hide_empty' => false
		) );

		return $exterior_colours;
	}

	/**
	 * Get Interior Colours
	 * @return  array  An Array of WP_Term Object
	 */
	public static function get_interior_colours(){
		$interior_colours = get_terms( array(
		    'taxonomy' => 'pa_interior-colour'
		    , 'hide_empty' => false
		) );

		return $interior_colours;
	}

	/**
	 * Get Vehicle Features
	 * @return  array  An Array of WP_Term Object
	 */
	public static function get_vehicle_features(){
		$vehicle_features = get_terms( array(
		    'taxonomy' => 'pa_vehicle-feature'
		    , 'hide_empty' => false
		) );

		return $vehicle_features;
	}


	/**
	 * Get Vehicle Serieses (Product Categories)
	 * @param  array  $args  Search series by name, slug, include => (ids), etc.
	 * @return Object $vehicle_serieses
	 */
	public static function get_vehicle_serieses( $args = array( 'name', 'slug', 'include' => array() ) ){

		$term_args = array( 'hide_empty' => false 
							, 'taxonomy' => 'product_cat'
							, 'meta_key' => self::PLUGIN_SLUG . '_'.'cat_type'
							, 'meta_value' => 'series'
							);

		foreach ($args as $key => $arg) {
			if(!empty($arg))
				$term_args[$key] = $arg;
		}

		$vehicle_serieses = get_terms( $term_args );

		foreach ($vehicle_serieses as $key => $vehicle_series) {
			$vehicle_series = self::get_vehicle_series( array('include' => $vehicle_series->term_id) );
			$vehicle_serieses[$key] = $vehicle_series;
		}

		return $vehicle_serieses;
	}

	/**
	 * Get Vehicle Series (Product Category)
	 * @param  array  $args  Search series by name, slug, include => (ids), etc.
	 * @return Object $vehicle_series
	 */
	public static function get_vehicle_series( $args = array( 'name', 'slug', 'include' => array() ) ){

		$term_args = array( 'hide_empty' => false 
											, 'taxonomy' => 'product_cat'
											, 'meta_key' => self::PLUGIN_SLUG . '_'.'cat_type'
											, 'meta_value' => 'series'
											);

		foreach ($args as $key => $arg) {
			if(!empty($arg))
				$term_args[$key] = $arg;
		}

		$vehicle_series = get_terms( $term_args );
		$vehicle_series = $vehicle_series[0];

		$vehicle_series->short_description = get_term_meta($vehicle_series->term_id, self::PLUGIN_SLUG . '_'.'short_description', true);
		$vehicle_series->cat_type = get_term_meta($vehicle_series->term_id, self::PLUGIN_SLUG . '_'.'cat_type', true);
		$vehicle_series->letter_head = strtoupper( substr( $vehicle_series->name, 0, strpos($vehicle_series->name, ' ') ) );		

		$vehicle_series->children = get_term_children( $vehicle_series->term_id, "product_cat" );

		return $vehicle_series;
	}

	/**
	 * Get Normal Used Vehicle Models 
	 * @return Object $vehicle_models
	 */
	public static function get_normal_used_vehicle_models(){

		$current_year = date("Y");

		$args = array( 
			'meta_query' => array( 
				array(
					'key' => self::PLUGIN_SLUG . '_'.'created_year'
					, 'value' => $current_year-1
				    , 'compare' => '<'
				    , 'type' => 'numeric'
					) 
				, array(
					'key' => self::PLUGIN_SLUG . '_'.'is_new'
					, 'value' => 'no'
					) 
				, array(
					'key' => self::PLUGIN_SLUG . '_'.'is_demo'
					, 'value' => 'no'
					) 
				) 
			);

		$normal_used_vehicle_models = self::get_vehicle_models( $args );

		return $normal_used_vehicle_models;
	}

	/**
	 * Get One Year Old Vehicle Models 
	 * @return Object $vehicle_models
	 */
	public static function get_one_year_old_vehicle_models(){

		$current_year = date("Y");

		$args = array( 
			'meta_query' => array( 
				array(
					'key' => self::PLUGIN_SLUG . '_'.'created_year'
					, 'value' => $current_year-1
				    , 'compare' => '>='
				    , 'type' => 'numeric'
					) 
				, array(
					'key' => self::PLUGIN_SLUG . '_'.'is_new'
					, 'value' => 'no'
					) 
				, array(
					'key' => self::PLUGIN_SLUG . '_'.'is_demo'
					, 'value' => 'no'
					) 
				) 
			);

		$one_year_old_vehicle_models = self::get_vehicle_models( $args );

		return $one_year_old_vehicle_models;
	}

	/**
	 * Get Demo Vehicle Models 
	 * @return Object $vehicle_models
	 */
	public static function get_demo_vehicle_models(){

		$args = array( 
			'meta_query' => array( 
				array(
					'key' => self::PLUGIN_SLUG . '_'.'is_demo'
					, 'value' => 'yes'
					) 
				) 
			);

		$demo_vehicle_models = self::get_vehicle_models( $args );

		return $demo_vehicle_models;
	}

	/**
	 * Get New Vehicle Models 
	 * @return Object $vehicle_models
	 */
	public static function get_new_vehicle_models(){

		$args = array( 
			'meta_query' => array( 
				array(
					'key' => self::PLUGIN_SLUG . '_'.'is_new'
					, 'value' => 'yes'
					) 
				) 
			);

		$new_vehicle_models = self::get_vehicle_models( $args );

		return $new_vehicle_models;
	}

	/**
	 * Get Vehicle Models (Product Categories)
	 * @param  array  $args  Search model by name, slug, include => (ids), etc.
	 * @return Object $vehicle_models
	 */
	public static function get_vehicle_models( $args = array( 'name', 'slug', 'include' => array() ) ){

		$term_args = array( 'hide_empty' => false 
							, 'taxonomy' => 'product_cat'
							, 'meta_query' => array( 
								array(
									'key' => self::PLUGIN_SLUG . '_'.'cat_type'
									, 'value' => 'model'
									) 
								)
							);

		foreach ($args as $key => $arg) {
			if(!empty($arg)){
				if( $key == 'meta_query' ){
					$term_args[$key] = array_merge( $term_args[$key], $arg );
				}
				else{
					$term_args[$key] = $arg;					
				}
			}
		}

		$vehicle_models = get_terms( $term_args );		

		foreach ($vehicle_models as $key => $vehicle_model) {
			$vehicle_model = self::get_vehicle_model( array('include' => $vehicle_model->term_id) );
			$vehicle_models[$key] = $vehicle_model;
		}

		return $vehicle_models;
	}

	/**
	 * Get Vehicle Model (Product Category)
	 * @param  array  $args  Search model by name, slug, include => (ids), etc.
	 * @return Object $vehicle_model
	 */
	public static function get_vehicle_model( $args = array( 'name', 'slug', 'include' => array() ) ){

		$term_args = array( 'hide_empty' => false 
											, 'taxonomy' => 'product_cat'
											, 'meta_key' => self::PLUGIN_SLUG . '_'.'cat_type'
											, 'meta_value' => 'model'
											);

		foreach ($args as $key => $arg) {
			if(!empty($arg))
				$term_args[$key] = $arg;
		}

		$vehicle_model = get_terms( $term_args );
		$vehicle_model = $vehicle_model[0];

		//get featured image
		$featured_image_id = get_woocommerce_term_meta( $vehicle_model->term_id, 'thumbnail_id', true );
		$featured_image = self::get_image( $featured_image_id );

		$vehicle_model->short_description = get_term_meta($vehicle_model->term_id, self::PLUGIN_SLUG . '_'.'short_description', true);
		$vehicle_model->cat_type = get_term_meta($vehicle_model->term_id, self::PLUGIN_SLUG . '_'.'cat_type', true);
		$vehicle_model->created_year = get_term_meta($vehicle_model->term_id, self::PLUGIN_SLUG . '_'.'created_year', true);
		$vehicle_model->is_new = get_term_meta($vehicle_model->term_id, self::PLUGIN_SLUG . '_'.'is_new', true);
		$vehicle_model->is_demo = get_term_meta($vehicle_model->term_id, self::PLUGIN_SLUG . '_'.'is_demo', true);
		$vehicle_model->vehicle_series = self::get_vehicle_series( array('include' => $vehicle_model->parent) );	
		$vehicle_model->featured_image = $featured_image;	

		return $vehicle_model;
	}

	/**
	 * Get Featured Vehicles
	 * @param  array  $args Refer to wp get_posts().
	 * @return Objects       $featured_vehicles
	 */
	public static function get_featured_vehicles( $args = array() ){

		$default_args = array( 'meta_key' => '_featured', 'meta_value' => 'yes' );
		$args = array_merge($default_args, $args);

		$featured_vehicles = self::get_vehicles( $args );

		return $featured_vehicles;
	}

	/**
	 * Get Vehicles
	 * @param  array  $args Arguments such as 'post_type', 'post_status', 'numberposts', etc. Refer to wp get_posts().
	 * @param  boolean  $is_meta_enabled  Whether to include meta data (result count, max page number)
	 * @param  array  $options  Options to include specific data
	 * @return Object  $response
	 */
	public static function get_vehicles( $args = array(), $is_meta_enabled = false, $options = array('has_specific_detail' => false) ){

		$default_args = array( 'post_type' => 'product'
						, 'post_status' => 'publish'
						, 'posts_per_page' => -1
						, 'fields' => 'all'
						);

		foreach($args as $key => $arg){
			if(!empty($arg))
				$default_args[$key] = $arg;
		}

		$wp_query = new WP_Query( $default_args );
		$posts = $wp_query->posts;
		$vehicles = array();

		foreach($posts as $key => $post){
			if( $options['has_specific_detail'] )
				$vehicle = self::get_vehicle($post->ID);
			else
				$vehicle = self::get_vehicle($post->ID);

			$vehicles[] = $vehicle;
		}

		if($is_meta_enabled){
			$response = (object) array('meta' => '', 'data' => '');
			$response->meta = (object) array('found_posts' => $wp_query->found_posts, 'max_num_pages' => $wp_query->max_num_pages);
			$response->data = $vehicles;
		}
		else
			$response = $vehicles;

		return $response;
	}

	/**
	 * Get Vehicle Object
	 * @param  Integer $post_id
	 * @param  Array $options Options such as whether to include specific detail, adjacent post, etc.
	 * @return Object $vehicle
	 */
	public static function get_vehicle( $post_id, $options = array('has_specific_detail' => false, 'has_adjacent_post' => false) ){

		// in this case, product id = post id
		$product = wc_get_product( $post_id );		

		$post = $product->post;
		$post_meta = get_post_meta( $post_id, '', true );

		//get option list
		$fuel_type = get_term($post_meta['_'.self::PLUGIN_SLUG.'_'.'fuel_type_id'][0]);
		$transmission_type = get_term($post_meta['_'.self::PLUGIN_SLUG.'_'.'transmission_type_id'][0]);
		$int_colour = get_term($post_meta['_'.self::PLUGIN_SLUG.'_'.'int_colour_id'][0]);
		$ext_colour = get_term($post_meta['_'.self::PLUGIN_SLUG.'_'.'ext_colour_id'][0]);

		//get featured image
		$featured_image_id = get_post_thumbnail_id( $post_id );
		$featured_image = self::get_image( $featured_image_id );

		//get vehicle model
		$product_term_ids = wc_get_product_terms( $product->id, 'product_cat', array( 'fields' => 'ids' ) );
		$vehicle_model = self::get_vehicle_model( array('include' => $product_term_ids) );

		//get vehicle series category from model category
		$vehicle_series = get_term($vehicle_model->parent);

		//get features of vehicle
		$features = wc_get_product_terms( $product->id, 'pa_vehicle-feature', array( 'fields' => 'all' ) );

		$vehicle = (object) array('ID' => $post_id);
		$vehicle->created_date = $post->post_date;
		$vehicle->title = $post->post_title;
		$vehicle->price = number_format( $post_meta['_regular_price'][0] );
		$vehicle->price_value = $post_meta['_regular_price'][0];
		$vehicle->price_unit = get_woocommerce_currency_symbol(get_woocommerce_currency());
		$vehicle->mileage = number_format( $post_meta['_'.self::PLUGIN_SLUG.'_'.'mileage'][0] );
		$vehicle->mileage_value = $post_meta['_'.self::PLUGIN_SLUG.'_'.'mileage'][0];
		$vehicle->mileage_unit = get_option(self::PLUGIN_SLUG.'_distance_unit');
		$vehicle->engine_size = $post_meta['_'.self::PLUGIN_SLUG.'_'.'engine_size'][0];
		$vehicle->engine_size_unit = get_option(self::PLUGIN_SLUG.'_engine_size_unit');
		$vehicle->engine_power = $post_meta['_'.self::PLUGIN_SLUG.'_'.'engine_power'][0];
		$vehicle->engine_power_unit = get_option(self::PLUGIN_SLUG.'_engine_power_unit');
		$vehicle->fuel_type = $fuel_type;
		$vehicle->transmission_type = $transmission_type;
		$vehicle->int_colour = $int_colour;
		$vehicle->ext_colour = $ext_colour;
		$vehicle->reg_no = $post_meta['_'.self::PLUGIN_SLUG.'_'.'reg_no'][0];
		$vehicle->reg_date = $post_meta['_'.self::PLUGIN_SLUG.'_'.'reg_date'][0];
		$vehicle->co2_emission = $post_meta['_'.self::PLUGIN_SLUG.'_'.'co2_emission'][0];
		$vehicle->co2_emission_unit = get_option(self::PLUGIN_SLUG.'_co2_emission_unit');
		$vehicle->is_featured = $post_meta['_featured'][0];
		$vehicle->featured_image = $featured_image;
		// Features
		$vehicle->features = $features;
		// Dealer Location
		$vehicle->dealer_location = self::get_dealer_location($post_meta['_'.self::PLUGIN_SLUG.'_'.'dealer_location_id'][0]);
		// Product Category
		$vehicle->vehicle_model = $vehicle_model;

		// Specific Detail (include only when needed)
		if($options['has_specific_detail']){
			$specific_detail = self::get_vehicle_specific_detail( $product->id );

			$vehicle->specific_detail = $specific_detail;
		}
		if($options['has_adjacent_post']){
			global $post;
			$post = get_post( $post_id );

			$next_post = get_next_post( false, '', 'product_cat' );
			$previous_post = get_previous_post( false, '', 'product_cat' );

			$vehicle->next_post = $next_post;
			$vehicle->previous_post = $previous_post;
		}

		return $vehicle;
	}

	public static function get_vehicle_specific_detail( $post_id ){
		// in this case, product id = post id
		$product = wc_get_product( $post_id );	

		$post = $product->post;
		$post_meta = get_post_meta( $post_id, '', true );

		//get tech specification
		$std_eqps = wc_get_product_terms( $product->id, 'pa_standard-equipment', array( 'fields' => 'all' ) );
		$opt_eqps = wc_get_product_terms( $product->id, 'pa_optional-equipment', array( 'fields' => 'all' ) );
		//get gallery images
		$gallery_image_ids = $product->get_gallery_attachment_ids();
		$gallery_images = array();
		foreach ($gallery_image_ids as $gallery_image_id) {
			$gallery_image = self::get_image( $gallery_image_id );
			$gallery_images[] = $gallery_image;
		}
		//get dealer location
		$dealer_location = self::get_dealer_location($post_meta['_'.self::PLUGIN_SLUG.'_'.'dealer_location_id'][0]);

		// Tech Specs
		$specific_detail = (object) array('vehicle_ID' => $post_id);
		$specific_detail->std_eqps = $std_eqps;
		$specific_detail->opt_eqps = $opt_eqps;
		// Images
		$specific_detail->gallery_images = $gallery_images;
		// Dealer Location
		$specific_detail->dealer_location = $dealer_location;

		return $specific_detail;
	}

	/**
	 * Get Dealer Locations
	 * @param  array  $args Arguments such as 'post_type', 'post_status', 'numberposts', etc. Refer to wp get_posts().
	 * @param  boolean  $is_meta_enabled  Whether to include meta data (result count, max page number)
	 * @return Object  $response
	 */
	public static function get_dealer_locations( $args = array(), $is_meta_enabled = false ){

		$default_args = array( 'post_type' => 'wpsl_stores'
						, 'post_status' => 'publish'
						, 'posts_per_page' => -1
						, 'fields' => 'all'
						);

		foreach($args as $key => $arg){
			if(!empty($arg))
				$default_args[$key] = $arg;
		}

		$wp_query = new WP_Query( $default_args );
		$posts = $wp_query->posts;

		// print_r($wp_query);
		// exit();

		$dealer_locations = array();

		foreach ($posts as $key => $post) {
			$dealer_location = self::get_dealer_location($post->ID);
			$dealer_locations[$key] = $dealer_location;
		}

		$response = $dealer_locations;

		return $response;
	}

	/**
	 * Get Dealer Location
	 * @param  Integer $post_id
	 * @return Object $dealer_location
	 */
	public static function get_dealer_location($post_id){

		$post = get_post($post_id);
		$post_meta = get_post_meta($post_id);

		$urls = explode( ";", $post_meta[self::STORE_LOCATOR_PLUGIN_SLUG.'_'.'url'][0] );

		$dealer_location = (object) array('ID' => $post_id);
		$dealer_location->title = $post->post_title;
		$dealer_location->address = $post_meta[self::STORE_LOCATOR_PLUGIN_SLUG.'_'.'address'][0];
		$dealer_location->address2 = $post_meta[self::STORE_LOCATOR_PLUGIN_SLUG.'_'.'address2'][0];
		$dealer_location->city = $post_meta[self::STORE_LOCATOR_PLUGIN_SLUG.'_'.'city'][0];
		$dealer_location->state = $post_meta[self::STORE_LOCATOR_PLUGIN_SLUG.'_'.'state'][0];
		$dealer_location->zip = $post_meta[self::STORE_LOCATOR_PLUGIN_SLUG.'_'.'zip'][0];
		$dealer_location->country = $post_meta[self::STORE_LOCATOR_PLUGIN_SLUG.'_'.'country'][0];
		$dealer_location->phone = $post_meta[self::STORE_LOCATOR_PLUGIN_SLUG.'_'.'phone'][0];
		$dealer_location->fax = $post_meta[self::STORE_LOCATOR_PLUGIN_SLUG.'_'.'fax'][0];
		$dealer_location->email = $post_meta[self::STORE_LOCATOR_PLUGIN_SLUG.'_'.'email'][0];
		$dealer_location->url = trim($urls[0]);
		$dealer_location->url2 = isset($urls[1]) ? trim($urls[1]) : '';
		$dealer_location->url3 = isset($urls[2]) ? trim($urls[2]) : '';
		$dealer_location->lat = $post_meta[self::STORE_LOCATOR_PLUGIN_SLUG.'_'.'lat'][0];
		$dealer_location->lng = $post_meta[self::STORE_LOCATOR_PLUGIN_SLUG.'_'.'lng'][0];
		$dealer_location->full_address = trim($dealer_location->address) . ' ' . trim($dealer_location->address2)
										. ' ' . $dealer_location->zip . ' ' . $dealer_location->city . ' ' . $dealer_location->state . '.' ;
		$dealer_location->full_lat_lng = $dealer_location->lat . ',' . $dealer_location->lng;

		$maps = array('google');

		$google_map = (object) array();
		$google_map->saddr = self::GOOGLE_MAP_URL . 'dir/?saddr=' . $dealer_location->full_lat_lng;
		$google_map->daddr = self::GOOGLE_MAP_URL . 'dir/?daddr=' . $dealer_location->full_lat_lng;
		$google_map->place = self::GOOGLE_MAP_URL . 'place/@' . $dealer_location->full_lat_lng . ',18z';
		$maps['google'] = $google_map;

		$dealer_location->maps = $maps;

		return $dealer_location;
	}

	/**
	 * Get standard image
	 * @param  Integer $image_attachment_id
	 * @return Object $image
	 */
	public static function get_image( $image_attachment_id ){
		$title = get_the_title( $image_attachment_id );
		$srcs = array('thumbnail' => '', 'medium' => '', 'large' => '', 'full' => '');
		foreach($srcs as $key => $src){
			$srcs[$key] = wp_get_attachment_image_src($image_attachment_id, $key)[0];
		}

		$image = (object) array('ID' => $image_attachment_id);
		$image->title = $title;
		$image->srcs = $srcs;

		return $image;
	}

	/**
	 * Attached to activate_{ plugin_basename( __FILES__ ) } by register_activation_hook()
	 * @static
	 */
	public static function plugin_activation() {
		//Run first installtion
		require_once(BWooVehicle__PLUGIN_DIR . 'class.bwoovehicle-install.php');
		BWooVehicle_Install::init();
	}

	/**
	 * Removes all connection options
	 * @static
	 */
	public static function plugin_deactivation( ) {
		
	}

	/**
	 * Removes all data
	 * @static
	 */
	public static function plugin_uninstalltion( ) {
		require_once(BWooVehicle__PLUGIN_DIR . 'class.bwoovehicle-uninstall.php');
		BWooVehicle_Uninstall::init();

		return;
	}

}