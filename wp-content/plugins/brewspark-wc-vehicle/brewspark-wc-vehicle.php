<?php
/**
 * @package Brewspark Woocommerce Vehicle
 */
/*
Plugin Name: Brewspark Woocommerce Vehicle
Plugin URI: http://brewspark.co
Description: A vehicle manager based on Woocommerce platform. (Reactivate if product attributes initial data is not shown)
Version: 1.0
Author: Brewspark
Author URI: http://brewspark.co
License: GPLv2 or later
*/

// Make sure we don't expose any info if called directly
if(!function_exists('add_action')){
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

define('BWooVehicle_VERSION', '1.0');
define('BWooVehicle__PLUGIN_DIR', plugin_dir_path(__FILE__ ));

register_activation_hook( __FILE__, array('BWooVehicle', 'plugin_activation'));
register_deactivation_hook( __FILE__, array('BWooVehicle', 'plugin_deactivation'));
register_uninstall_hook( __FILE__, array('BWooVehicle', 'plugin_uninstallation'));

require_once(BWooVehicle__PLUGIN_DIR . 'class.bwoovehicle.php');

add_action('init', array('BWooVehicle', 'init'));

if(is_admin()){
	require_once(BWooVehicle__PLUGIN_DIR . 'class.bwoovehicle-admin.php');
	add_action('init', array('BWooVehicle_Admin', 'init'));
}