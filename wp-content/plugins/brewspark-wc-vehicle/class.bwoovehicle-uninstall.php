<?php

class BWooVehicle_Uninstall{
	const PLUGIN_SLUG = 'bwoovehicle';

	private static $initiated = false;

	public static function init() {
		if ( ! self::$initiated ) {
			self::remove_options();
		}
	}

	private static function remove_options(){
		self::$initiated = true;

		delete_option(self::PLUGIN_SLUG.'_distance_unit');
		delete_option(self::PLUGIN_SLUG.'_engine_size_unit');
		delete_option(self::PLUGIN_SLUG.'_engine_power_unit');
		delete_option(self::PLUGIN_SLUG.'_co2_emission_rate_unit');
	}
}