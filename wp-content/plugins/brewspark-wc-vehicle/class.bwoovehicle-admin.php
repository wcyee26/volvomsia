<?php

class BWooVehicle_Admin {
	const PLUGIN_SLUG = 'bwoovehicle';

	private static $initiated = false;

	public static function init() {
		if ( ! self::$initiated ) {
			self::init_hooks();
		}
	}

	/**
	 * Initializes WordPress hooks
	 */
	private static function init_hooks() {
		self::$initiated = true;

		add_action( 'admin_init', array( 'BWooVehicle_Admin', 'admin_init' ) );
		// Add Vehicle Details Fields
		add_action( 'woocommerce_product_options_general_product_data', array('BWooVehicle_Admin', 'woo_product_vehicle_details'));
		// Save Vehicle Details Fields
		add_action( 'woocommerce_process_product_meta', array('BWooVehicle_Admin', 'woo_product_vehicle_details_save'));
		// Add Vehicle Settings Section in Woo > Settings > Product tab
		add_filter( 'woocommerce_get_sections_products', array('BWooVehicle_Admin', 'woo_settings_vehicle_settings_section') );
		// Add Vehicle Settings Content
		add_filter( 'woocommerce_get_settings_products', array('BWooVehicle_Admin', 'woo_settings_vehicle_settings_content'), 10, 2 );
		// Product Category Add Fields
		add_action('product_cat_add_form_fields', array('BWooVehicle_Admin', 'woo_product_cat_vehicle_meta_add'), 10, 1);
		// Product Category Edit Fields
		add_action('product_cat_edit_form_fields', array('BWooVehicle_Admin', 'woo_product_cat_vehicle_meta_edit'), 10, 1);
		// Save Product Category Add and Edit Fields
		add_action('create_product_cat', array('BWooVehicle_Admin', 'woo_product_cat_vehicle_meta_save'), 10, 1);
		add_action('edited_product_cat', array('BWooVehicle_Admin', 'woo_product_cat_vehicle_meta_save'), 10, 1);
	}

	public static function admin_init() {
		
	}

	public static function woo_product_vehicle_details() {
		global $woocommerce, $post;
		$product = wc_get_product( $post->ID );
		
		echo '<div class="options_group">';
		
		// Custom fields will be created here...

		// Mileage
		woocommerce_wp_text_input( 
			array( 
				'id'          => '_'.self::PLUGIN_SLUG.'_mileage'
				, 'label'       => __( 'Mileage (' . get_option(self::PLUGIN_SLUG.'_distance_unit', 'km') . ')', self::PLUGIN_SLUG )
			)
		);

		// Engine Size
		woocommerce_wp_text_input( 
			array( 
				'id'          => '_'.self::PLUGIN_SLUG.'_engine_size'
				, 'label'       => __( 'Engine size (' . get_option(self::PLUGIN_SLUG.'_engine_size_unit', 'L') . ')', self::PLUGIN_SLUG )
			)
		);

		// Engine Power
		woocommerce_wp_text_input( 
			array( 
				'id'          => '_'.self::PLUGIN_SLUG.'_engine_power'
				, 'label'       => __( 'Engine power (' . get_option(self::PLUGIN_SLUG.'_engine_power_unit', 'bhp') . ')', self::PLUGIN_SLUG )
			)
		);

		// Get Attributes of Fuel Type
		$fuel_type_terms = BWooVehicle::get_fuel_types();
		foreach ($fuel_type_terms as $fuel_type_term) {
			$fuel_type_options[$fuel_type_term->term_id] = __( $fuel_type_term->name, self::PLUGIN_SLUG );
		}
		// Fuel Type Id
		woocommerce_wp_select( 
		array( 
			'id'      => '_'.self::PLUGIN_SLUG.'_fuel_type_id'
			, 'label'   => __( 'Fuel type', self::PLUGIN_SLUG )
			, 'options' => $fuel_type_options
			, 'custom_attributes' => array('data-term-taxonomy' => 'pa_fuel-type')
			, 'desc_tip'    => 'true'
			, 'description' => __( 'The list is from global product attributes.', self::PLUGIN_SLUG ) 
			)
		);

		// Get Attributes of Transmission Type
		$transmission_type_terms = BWooVehicle::get_transmission_types();
		foreach ($transmission_type_terms as $transmission_type_term) {
			$tranmission_type_options[$transmission_type_term->term_id] = __( $transmission_type_term->name, self::PLUGIN_SLUG );
		}
		// Transmission Type Id
		woocommerce_wp_select( 
		array( 
			'id'      => '_'.self::PLUGIN_SLUG.'_transmission_type_id'
			, 'label'   => __( 'Transmission type', self::PLUGIN_SLUG )
			, 'options' => $tranmission_type_options
			, 'custom_attributes' => array('data-term-taxonomy' => 'pa_transmission-type')
			, 'desc_tip'    => 'true'
			, 'description' => __( 'The list is from global product attributes.', self::PLUGIN_SLUG ) 
			)
		);

		// Get Attributes of Exterior Colour
		$exterior_colour_terms = BWooVehicle::get_exterior_colours();
		foreach ($exterior_colour_terms as $exterior_colour_term) {
			$exterior_colour_options[$exterior_colour_term->term_id] = __( $exterior_colour_term->name, self::PLUGIN_SLUG );
		}
		// Exterior Colour Id
		woocommerce_wp_select( 
		array( 
			'id'      => '_'.self::PLUGIN_SLUG.'_ext_colour_id'
			, 'label'   => __( 'Exterior colour', self::PLUGIN_SLUG )
			, 'options' => $exterior_colour_options
			, 'custom_attributes' => array('data-term-taxonomy' => 'pa_exterior-colour')
			, 'desc_tip'    => 'true'
			, 'description' => __( 'The list is from global product attributes.', self::PLUGIN_SLUG ) 
			)
		);

		// Get Attributes of Interior Colour
		$interior_colour_terms = BWooVehicle::get_interior_colours();
		foreach ($interior_colour_terms as $interior_colour_term) {
			$interior_colour_options[$interior_colour_term->term_id] = __( $interior_colour_term->name, self::PLUGIN_SLUG );
		}
		// Interior Colour Id
		woocommerce_wp_select( 
		array( 
			'id'      => '_'.self::PLUGIN_SLUG.'_int_colour_id'
			, 'label'   => __( 'Interior colour', self::PLUGIN_SLUG )
			, 'options' => $interior_colour_options
			, 'custom_attributes' => array('data-term-taxonomy' => 'pa_interior-colour')
			, 'desc_tip'    => 'true'
			, 'description' => __( 'The list is from global product attributes.', self::PLUGIN_SLUG ) 
			)
		);		

		// Registration No
		woocommerce_wp_text_input( 
			array( 
				'id'          => '_'.self::PLUGIN_SLUG.'_reg_no'
				, 'label'       => __( 'Registration number', self::PLUGIN_SLUG )
			)
		);		

		// Registration Date
		woocommerce_wp_text_input( 
			array( 
				'id'          => '_'.self::PLUGIN_SLUG.'_reg_date'
				, 'type'		=> 'date'
				, 'label'       => __( 'Registration date', self::PLUGIN_SLUG )
				, 'class'		=> 'wpcm-date-field hasDatepicker'
				, 'placeholder'	=> 'yyyy-mm-dd'
			)
		);

		// CO2 Emission
		woocommerce_wp_text_input( 
			array( 
				'id'          => '_'.self::PLUGIN_SLUG.'_co2_emission'
				, 'label'       => __( 'CO2 emission (' . get_option(self::PLUGIN_SLUG.'_co2_emission_unit', 'g/km') . ')', self::PLUGIN_SLUG )
			)
		);

		// Get dealer locations (posts)
		$dealer_locations = BWooVehicle::get_dealer_locations();
		foreach ($dealer_locations as $dealer_location) {
			$dealer_location_options[$dealer_location->ID] = __( $dealer_location->title, self::PLUGIN_SLUG );
		}
		// Dealer Location Id
		woocommerce_wp_select( 
		array( 
			'id'      => '_'.self::PLUGIN_SLUG.'_dealer_location_id'
			, 'label'   => __( 'Dealer location', self::PLUGIN_SLUG )
			, 'options' => $dealer_location_options
			, 'custom_attributes' => array('data-post-type' => 'wpsl_stores')
			, 'desc_tip'    => 'true'
			, 'description' => __( 'The store list is from Store Locator Plugin', self::PLUGIN_SLUG ) 
			)
		);	

		// // Is Volvo Selekt
		// woocommerce_wp_checkbox( 
		// array( 
		// 	'id'            => '_'.self::PLUGIN_SLUG.'_is_volvo_selekt'
		// 	, 'label'         => __('Volvo Selekt', self::PLUGIN_SLUG )
		// 	)
		// );
		
		echo '</div>';
	}

	public static function woo_product_vehicle_details_save($post_id){

		$mileage = filter_input(INPUT_POST, '_' . self::PLUGIN_SLUG . '_' . 'mileage');
		if( !empty($mileage) )
			update_post_meta( $post_id, '_' . self::PLUGIN_SLUG . '_' . 'mileage', esc_attr( $mileage ) );

		$engine_size = filter_input(INPUT_POST, '_' . self::PLUGIN_SLUG . '_' . 'engine_size');
		if( !empty($engine_size) )
			update_post_meta( $post_id, '_' . self::PLUGIN_SLUG . '_' . 'engine_size', esc_attr( $engine_size ) );

		$engine_power = filter_input(INPUT_POST, '_' . self::PLUGIN_SLUG . '_' . 'engine_power');
		if( !empty($engine_power) )
			update_post_meta( $post_id, '_' . self::PLUGIN_SLUG . '_' . 'engine_power', esc_attr( $engine_power ) );

		$fuel_type_id = filter_input(INPUT_POST, '_' . self::PLUGIN_SLUG . '_' . 'fuel_type_id');
		if( !empty($fuel_type_id) )
			update_post_meta( $post_id, '_' . self::PLUGIN_SLUG . '_' . 'fuel_type_id', esc_attr( $fuel_type_id ) );

		$transmission_type_id = filter_input(INPUT_POST, '_' . self::PLUGIN_SLUG . '_' . 'transmission_type_id');
		if( !empty($transmission_type_id) )
			update_post_meta( $post_id, '_' . self::PLUGIN_SLUG . '_' . 'transmission_type_id', esc_attr( $transmission_type_id ) );

		$ext_colour_id = filter_input(INPUT_POST, '_' . self::PLUGIN_SLUG . '_' . 'ext_colour_id');
		if( !empty($ext_colour_id) )
			update_post_meta( $post_id, '_' . self::PLUGIN_SLUG . '_' . 'ext_colour_id', esc_attr( $ext_colour_id ) );

		$int_colour_id = filter_input(INPUT_POST, '_' . self::PLUGIN_SLUG . '_' . 'int_colour_id');
		if( !empty($int_colour_id) )
			update_post_meta( $post_id, '_' . self::PLUGIN_SLUG . '_' . 'int_colour_id', esc_attr( $int_colour_id ) );

		$reg_no = filter_input(INPUT_POST, '_' . self::PLUGIN_SLUG . '_' . 'reg_no');
		if( !empty($reg_no) )
			update_post_meta( $post_id, '_' . self::PLUGIN_SLUG . '_' . 'reg_no', esc_attr( $reg_no ) );

		$reg_date = filter_input(INPUT_POST, '_' . self::PLUGIN_SLUG . '_' . 'reg_date');
		if( !empty($reg_date) )
			update_post_meta( $post_id, '_' . self::PLUGIN_SLUG . '_' . 'reg_date', esc_attr( $reg_date ) );

		$co2_emission = filter_input(INPUT_POST, '_' . self::PLUGIN_SLUG . '_' . 'co2_emission');
		if( !empty($co2_emission) )
			update_post_meta( $post_id, '_' . self::PLUGIN_SLUG . '_' . 'co2_emission', esc_attr( $co2_emission ) );

		$dealer_location_id = filter_input(INPUT_POST, '_' . self::PLUGIN_SLUG . '_' . 'dealer_location_id');
		if( !empty($dealer_location_id) )
			update_post_meta( $post_id, '_' . self::PLUGIN_SLUG . '_' . 'dealer_location_id', esc_attr( $dealer_location_id ) );

		// $is_volvo_selekt = filter_input(INPUT_POST, '_' . self::PLUGIN_SLUG . '_' . 'is_volvo_selekt');
		// $is_volvo_selekt = $is_volvo_selekt ? $is_volvo_selekt : 'no';
		// update_post_meta( $post_id, '_' . self::PLUGIN_SLUG . '_' . 'is_volvo_selekt', esc_attr( $is_volvo_selekt ) );
	}

	public static function woo_settings_vehicle_settings_section($sections){
		$sections[self::PLUGIN_SLUG.'_vehicle_settings'] = __( 'BWooVehicle', self::PLUGIN_SLUG );
		return $sections;
	}

	public static function woo_settings_vehicle_settings_content( $settings, $current_section ) {
		if ( $current_section == self::PLUGIN_SLUG.'_vehicle_settings' ) {

			$vehicle_settings = array();
			// Add Title to the Settings
			$vehicle_settings[] = array( 
				'name' => __( 'Vehicle Settings', self::PLUGIN_SLUG )
				, 'type' => 'title'
				, 'id' => self::PLUGIN_SLUG.'_vehicle_settings' 
				);
			//Distance Unit
			$vehicle_settings[] = array(
						'id'       => self::PLUGIN_SLUG.'_distance_unit'
						, 'name'     => __( 'Distance Unit', self::PLUGIN_SLUG )
						, 'type'     => 'text'
						, 'desc_tip' => __( 'This will normally use for mileage', self::PLUGIN_SLUG )
						, 'default' => get_option(self::PLUGIN_SLUG.'_distance_unit')
					);
			//Engine Size Unit
			$vehicle_settings[] = array(
						'id'       => self::PLUGIN_SLUG.'_engine_size_unit'
						, 'name'     => __( 'Engine Size Unit', self::PLUGIN_SLUG )
						, 'type'     => 'text'
						, 'default' => get_option(self::PLUGIN_SLUG.'_engine_size_unit')
					);
			//Engine Power Unit
			$vehicle_settings[] = array(
						'id'       => self::PLUGIN_SLUG.'_engine_power_unit'
						, 'name'     => __( 'Engine Power Unit', self::PLUGIN_SLUG )
						, 'type'     => 'text'
						, 'default' => get_option(self::PLUGIN_SLUG.'_engine_power_unit')
					);
			//CO2 Emission Unit
			$vehicle_settings[] = array(
						'id'       => self::PLUGIN_SLUG.'_co2_emission_unit'
						, 'name'     => __( 'CO2 Emission Unit', self::PLUGIN_SLUG )
						, 'type'     => 'text'
						, 'default' => get_option(self::PLUGIN_SLUG.'_co2_emission_unit')
					);

			$vehicle_settings[] = array( 'type' => 'sectionend', 'id' => self::PLUGIN_SLUG.'_vehicle_settings' );

			return $vehicle_settings;
		}
		else{
			return $settings;
		}
	}

	public static function woo_product_cat_vehicle_meta_add(){
		?>   
		    <div class="form-field">
		        <label for="<?php echo self::PLUGIN_SLUG . '_short_description' ?>">
		        	<?php _e('Short Description', self::PLUGIN_SLUG); ?>
		        </label>
		        <input type="text" name="<?php echo self::PLUGIN_SLUG . '_short_description' ?>" id="<?php echo self::PLUGIN_SLUG . '_short_description' ?>">
		        <p class="description"><?php _e('Short description for the product category.', self::PLUGIN_SLUG); ?></p>
		    </div>
		    <div class="form-field">
		        <label for="<?php echo self::PLUGIN_SLUG . '_cat_type' ?>"><?php _e('Category Type', self::PLUGIN_SLUG ); ?></label>
		        <select name="<?php echo self::PLUGIN_SLUG . '_cat_type' ?>" id="<?php echo self::PLUGIN_SLUG . '_cat_type' ?>">
		        	<option value="-1">Default</option>
		        	<option value="model">Model</option>
		        	<option value="series">Series</option>
		        </select>
		        <p class="description"><?php _e('The type of product category.', self::PLUGIN_SLUG); ?></p>
		    </div>
		    <div class="form-field">
		        <label for="<?php echo self::PLUGIN_SLUG . '_created_year' ?>"><?php _e('Created Year', self::PLUGIN_SLUG ); ?></label>
		        <select name="<?php echo self::PLUGIN_SLUG . '_created_year' ?>" id="<?php echo self::PLUGIN_SLUG . '_created_year' ?>">
		        	<option value="-1">None</option>
		        	<?php for($created_year = date("Y"); $created_year >= 2010; $created_year--): ?>
		        	<option value="<?php echo $created_year; ?>"><?php echo $created_year; ?></option>
			        <?php endfor; ?>
		        </select>
		        <p class="description"><?php _e('The year that product category has been created.', self::PLUGIN_SLUG); ?></p>
		    </div>
		    <div class="form-field">
		        <label for="<?php echo self::PLUGIN_SLUG . '_is_new' ?>"><?php _e('Is New?', self::PLUGIN_SLUG ); ?></label>
		        <input type="checkbox" value="yes"
		        	   name="<?php echo self::PLUGIN_SLUG . '_is_new' ?>" 
		        	   id="<?php echo self::PLUGIN_SLUG . '_is_new' ?>">
		        <p class="description"><?php _e('Whether this product category is new.', self::PLUGIN_SLUG); ?></p>
		    </div>
		    <div class="form-field">
		        <label for="<?php echo self::PLUGIN_SLUG . '_is_demo' ?>"><?php _e('Is Demo?', self::PLUGIN_SLUG ); ?></label>
		        <input type="checkbox" value="yes"
		        	   name="<?php echo self::PLUGIN_SLUG . '_is_demo' ?>" 
		        	   id="<?php echo self::PLUGIN_SLUG . '_is_demo' ?>">
		        <p class="description"><?php _e('Whether this product category is a demo.', self::PLUGIN_SLUG); ?></p>
		    </div>
	    <?php
	}

	public static function woo_product_cat_vehicle_meta_edit($term){

		//getting term id
		$term_id = $term->term_id;

		// retrieve the existing value(s) for this meta field.
	    $short_description = get_term_meta($term_id, self::PLUGIN_SLUG . '_short_description', true);
	    $cat_type = get_term_meta($term_id, self::PLUGIN_SLUG . '_cat_type', true);
	    $created_year = get_term_meta($term_id, self::PLUGIN_SLUG . '_created_year', true);
	    $is_new = get_term_meta($term_id, self::PLUGIN_SLUG . '_is_new', true);
	    $is_demo = get_term_meta($term_id, self::PLUGIN_SLUG . '_is_demo', true);
		?>   

		<tr class="form-field">
	        <th scope="row" valign="top">
	        	<label for="<?php echo self::PLUGIN_SLUG . '_short_description' ?>">
	        		<?php _e('Short Description', self::PLUGIN_SLUG); ?>
	        	</label>
	        </th>
	        <td>
	            <input type="text" name="<?php echo self::PLUGIN_SLUG . '_short_description' ?>" 
	            	   id="<?php echo self::PLUGIN_SLUG . '_short_description' ?>" 
	            	   value="<?php echo esc_attr($short_description) ? esc_attr($short_description) : ''; ?>">
	            <p class="description"><?php _e('Short description for the product category.', self::PLUGIN_SLUG . '_short_description'); ?></p>
	        </td>
	    </tr>
		<tr class="form-field">
	        <th scope="row" valign="top">
	        	<label for="<?php echo self::PLUGIN_SLUG . '_cat_type' ?>">
	        		<?php _e('Category Type', self::PLUGIN_SLUG); ?>
	        	</label>
	        </th>
	        <td>
		        <select name="<?php echo self::PLUGIN_SLUG . '_cat_type' ?>" id="<?php echo self::PLUGIN_SLUG . '_cat_type' ?>">
		        	<option value="-1">Default</option>
		        	<option value="model" <?php if($cat_type == 'model') echo "selected" ?> >Model</option>
		        	<option value="series" <?php if($cat_type == 'series') echo "selected" ?> >Series</option>
		        </select>
		        <p class="description"><?php _e('The type of product category.', self::PLUGIN_SLUG); ?></p>
	        </td>
	    </tr>
		<tr class="form-field">
	        <th scope="row" valign="top">
	        	<label for="<?php echo self::PLUGIN_SLUG . '_created_year' ?>">
	        		<?php _e('Created Year', self::PLUGIN_SLUG); ?>
	        	</label>
	        </th>
	        <td>
		        <select name="<?php echo self::PLUGIN_SLUG . '_created_year' ?>" id="<?php echo self::PLUGIN_SLUG . '_created_year' ?>">
		        	<option value="-1">None</option>
		        	<?php for($created_year_loop = date("Y"); $created_year_loop >= 2000; $created_year_loop--): ?>
		        	<option <?php if($created_year_loop == $created_year) echo "selected"; ?> 
		        			value="<?php echo $created_year_loop; ?>">
		        			<?php echo $created_year_loop; ?>
		        	</option>
			        <?php endfor; ?>
		        </select>
		        <p class="description"><?php _e('The year that product category has been created.', self::PLUGIN_SLUG); ?></p>
	        </td>
	    </tr>
		<tr class="form-field">
	        <th scope="row" valign="top">
	        	<label for="<?php echo self::PLUGIN_SLUG . '_is_new' ?>">
	        		<?php _e('Is New?', self::PLUGIN_SLUG); ?>
	        	</label>
	        </th>
	        <td>
		        <input type="checkbox" value="yes" 
		        	   <?php if($is_new == 'yes') echo 'checked'; ?>
		        	   name="<?php echo self::PLUGIN_SLUG . '_is_new' ?>" 
		        	   id="<?php echo self::PLUGIN_SLUG . '_is_new' ?>">
		        <p class="description"><?php _e('Whether this product category is new.', self::PLUGIN_SLUG); ?></p>
	        </td>
	    </tr>
		<tr class="form-field">
	        <th scope="row" valign="top">
	        	<label for="<?php echo self::PLUGIN_SLUG . '_is_demo' ?>">
	        		<?php _e('Is Demo?', self::PLUGIN_SLUG); ?>
	        	</label>
	        </th>
	        <td>
		        <input type="checkbox" value="yes" 
		        	   <?php if($is_demo == 'yes') echo 'checked'; ?>
		        	   name="<?php echo self::PLUGIN_SLUG . '_is_demo' ?>" 
		        	   id="<?php echo self::PLUGIN_SLUG . '_is_demo' ?>">
		        <p class="description"><?php _e('Whether this product category is a demo.', self::PLUGIN_SLUG); ?></p>
	        </td>
	    </tr>
		    
	    <?php
	}

	public static function woo_product_cat_vehicle_meta_save($term_id){

		$short_description = filter_input(INPUT_POST, self::PLUGIN_SLUG . '_short_description');
		$cat_type = filter_input(INPUT_POST, self::PLUGIN_SLUG . '_cat_type');
		$created_year = filter_input(INPUT_POST, self::PLUGIN_SLUG . '_created_year');
		$is_new = $_POST[self::PLUGIN_SLUG . '_is_new'] ? $_POST[self::PLUGIN_SLUG . '_is_new'] : 'no';
		$is_demo = $_POST[self::PLUGIN_SLUG . '_is_demo'] ? $_POST[self::PLUGIN_SLUG . '_is_demo'] : 'no';

		update_term_meta($term_id, self::PLUGIN_SLUG . '_short_description', $short_description);
		update_term_meta($term_id, self::PLUGIN_SLUG . '_created_year', $created_year);
		update_term_meta($term_id, self::PLUGIN_SLUG . '_cat_type', $cat_type);
		update_term_meta($term_id, self::PLUGIN_SLUG . '_is_new', $is_new);
		update_term_meta($term_id, self::PLUGIN_SLUG . '_is_demo', $is_demo);
	}
}