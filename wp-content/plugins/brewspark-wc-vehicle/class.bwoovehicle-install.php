<?php

class BWooVehicle_Install{
	const PLUGIN_SLUG = 'bwoovehicle';

	private static $initiated = false;

	public static function init() {
		if ( ! self::$initiated ) {
			self::setup_options();
			self::setup_woocommerce_vehicle_attributes();
			self::setup_woocommerce_vehicle_attribute_terms();

		}
	}

	private static function setup_options(){
		self::$initiated = true;

		add_option(self::PLUGIN_SLUG.'_distance_unit', 'km');
		add_option(self::PLUGIN_SLUG.'_engine_size_unit', 'L');
		add_option(self::PLUGIN_SLUG.'_engine_power_unit', 'bhp');
		add_option(self::PLUGIN_SLUG.'_co2_emission_unit', 'g/km');
	}


	/**
	 * Setup the default vehicles attributes needed by woocommerce
	 */
	private static function setup_woocommerce_vehicle_attributes(){
		global $wpdb;

		// default product attributes needed for vehicle
		$attributes = array(
			'fuel-type' => array('label' => 'Fuel Type')
			, 'transmission-type' => array('label' => 'Transmission Type')
			, 'exterior-colour' => array('label' => 'Exterior Colour')
			, 'interior-colour' => array('label' => 'Interior Colour')
			, 'vehicle-feature' => array('label' => 'Vehicle Feature')
			, 'standard-equipment' => array('label' => 'Standard Equipment')
			, 'optional-equipment' => array('label' => 'Optional Equipment')
			);

		foreach( $attributes as $attribute_name => $attribute ){
			// if attribute is not already existed
			if( !self::get_woocommerce_attribute_taxonomies($attribute_name) ) {

				$new_attribute = array();
				$new_attribute['attribute_name'] = $attribute_name;
				$new_attribute['attribute_label'] = $attribute['label'];
				$new_attribute['attribute_type'] = 'select';
				$new_attribute['attribute_orderby'] = 'menu_order';
				$new_attribute['attribute_public'] = 0;

				$wpdb->insert( $wpdb->prefix . 'woocommerce_attribute_taxonomies', $new_attribute );

				do_action( 'woocommerce_attribute_added', $wpdb->insert_id, $new_attribute );

				// clear old wc attribute taxonomies (new attributes only will show by clearing this)
				flush_rewrite_rules();
				delete_transient( 'wc_attribute_taxonomies' );
			}
		}
	}

	public static function setup_woocommerce_vehicle_attribute_terms(){
		// default product attribute terms needed for vehicle
		$attribute_terms = array(
			'fuel-type' => array(
				'Petrol', 'Diesel', 'Hybrid'
				)
			, 'transmission-type' => array(
				'Automatic', 'Manual', 'Direct'
				)
			, 'exterior-colour' => array(
				'Beige', 'Blue', 'Brown', 'Gold', 'Grey', 'Green'
				, 'Orange', 'Red', 'Black', 'Silver', 'White'
				)
			, 'interior-colour' => array(
				'Black', 'Grey', 'Light Brown'
				)
			, 'vehicle-feature' => array(
				'Volvo Selekt', 'All Wheel Drive', 'Leather Upholdstery'
				, 'Satellite Navigation', '7 Seats', 'Keyless Start or Go'
				, 'Rear Park Assist', 'Sunroof', 'Heated Seats', 'Cruise Control'
				, 'Polestar Performance', 'Xenon'
				)
			, 'standard-equipment' => array(
				'WHIPS' => array('description' => 'Whiplash Protection System')
				, 'IC' => array('description' => 'Inflatable Curtain')
				, 'Home Safe and Approach Lighting'
				, 'Height and Reach Adjustable Steering Column'
				, 'SIPS' => array('description' => 'Side Impact Protection Airbags')
				)
			, 'optional-equipment' => array(
				'WHIPS' => array('description' => 'Whiplash Protection System')
				, 'IC' => array('description' => 'Inflatable Curtain')
				, 'Home Safe and Approach Lighting'
				, 'Height and reach Adjustable Steering Column'
				, 'SIPS' => array('description' => 'Side Impact Protection Airbags')
				, 'Power Adjustable and Heated Door Mirrors'
				, 'Storage in Front Doors'
				, 'Front Centre Armrest with Cupholders and Storage'
				)
			);

		foreach ($attribute_terms as $attribute_slug => $attribute_term) {
			$taxonomy = 'pa_'.$attribute_slug;

			$attribute_term_counter = 0;
			foreach($attribute_term as $index => $term){				
				$attribute_term_counter += 1;

				// insert term 		
				if( is_int($index) ){
					$inserted_term = wp_insert_term( $term, $taxonomy );
				}
				else{
					$term_name = $index;
					$term_args = $term;
					$inserted_term = wp_insert_term( $term_name, $taxonomy, $term_args );
				}
				$order_meta_key = 'order_' . $taxonomy;

				// if inserted term is not error object
				if( !$inserted_term instanceof WP_Error ){
					// update menu order
					update_woocommerce_term_meta( $inserted_term['term_id'], $order_meta_key, $attribute_term_counter );
				}
			}
		}

	}

	private static function get_woocommerce_attribute_taxonomies($attribute_name){
		global $wpdb;

		return $wpdb->get_row("SELECT attribute_name FROM " . $wpdb->prefix ."woocommerce_attribute_taxonomies"
								. " WHERE attribute_name = '" . $attribute_name . "'", 'ARRAY_A');
	}
}